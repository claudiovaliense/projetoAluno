-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 24/10/2017 às 22:12
-- Versão do servidor: 10.1.21-MariaDB
-- Versão do PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `mydb`
--


-- --------------------------------------------------------

--
-- Estrutura para tabela `curso`
--

CREATE TABLE `curso` (
  `idcurso` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `disciplina`
--

CREATE TABLE `disciplina` (
  `iddisciplina` int(11) NOT NULL,
  `codigoDisciplina` varchar(45) DEFAULT NULL,
  `nomeDisciplina` varchar(45) DEFAULT NULL,
  `cargaHoraria` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- --------------------------------------------------------

--
-- Estrutura para tabela `informacoesAluno`
--

CREATE TABLE `informacoesAluno` (
  `idinformacoesAluno` int(11) NOT NULL,
  `dataColeta` datetime DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `aplicativoAberto` varchar(45) DEFAULT NULL,
  `estaParado` tinyint(4) DEFAULT NULL,
  `aluno_idaluno` int(11) DEFAULT NULL,
  `macId` varchar(30) DEFAULT NULL,
  `luminosidade` double DEFAULT NULL,
  `barulho` double DEFAULT NULL,
  `cargaBateria` int(11) DEFAULT NULL,
  `precisaoGPS` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `professor`
--

CREATE TABLE `professor` (
  `idprofessor` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `matricula` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `sala`
--

CREATE TABLE `sala` (
  `idsala` int(11) NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area` double DEFAULT NULL,
  `localizacaoTextual` varchar(45) DEFAULT NULL,
  `numJanelas` int(11) DEFAULT NULL,
  `numProjetor` int(11) DEFAULT NULL,
  `numQuadro` int(11) DEFAULT NULL,
  `numArCondicionado` int(11) DEFAULT NULL,
  `numVentilador` int(11) DEFAULT NULL,
  `numPortas` int(11) DEFAULT NULL,
  `numLampadas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `semestre`
--

CREATE TABLE `semestre` (
  `idSemestre` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `dataInicio` date DEFAULT NULL,
  `dataFim` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `turma`
--

CREATE TABLE `turma` (
  `idturma` int(11) NOT NULL,
  `codigoTurma` varchar(10) DEFAULT NULL,
  `horaInicio` int(11) DEFAULT NULL,
  `minutoInicio` int(11) DEFAULT NULL,
  `horaFim` int(11) DEFAULT NULL,
  `minutoFim` int(11) DEFAULT NULL,
  `disciplina_iddisciplina` int(11) NOT NULL,
  `professor_idprofessor` int(11) NOT NULL,
  `semestre_idtable1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Estrutura para tabela `aluno`
--

CREATE TABLE `aluno` (
  `idaluno` int(11) NOT NULL,
  `macId` varchar(30) DEFAULT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `dataNascimento` date DEFAULT NULL,
  `matricula` varchar(45) DEFAULT NULL,
  `curso_idcurso` int(11) NOT NULL,
  `semestre_idtable1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Estrutura para tabela `entradaSaida`
--

CREATE TABLE `entradaSaida` (
  `idEntradaSaida` int(11) NOT NULL,
  `dataEntrada` date DEFAULT NULL,
  `dataSaida` date DEFAULT NULL,
  `aula_has_aluno_idAula_has_aluno` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `aluno_has_turma`
--

CREATE TABLE `aluno_has_turma` (
  `id` int(11) NOT NULL,
  `aluno_idaluno` int(11) NOT NULL,
  `turma_idturma` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `aula`
--

CREATE TABLE `aula` (
  `idaula` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `chegadaProfessor` datetime DEFAULT NULL,
  `saidaProfessor` datetime DEFAULT NULL,
  `inicioAula` datetime DEFAULT NULL,
  `fimAula` datetime DEFAULT NULL,
  `turma_idturma` int(11) NOT NULL,
  `professor_idprofessor` int(11) NOT NULL,
  `numJanelasAbertas` int(11) DEFAULT NULL,
  `numVentiladorLigados` int(11) DEFAULT NULL,
  `numArCondicionadoLigado` int(11) DEFAULT NULL,
  `temperatura` int(11) DEFAULT NULL,
  `barulho` int(11) DEFAULT NULL,
  `luminosidade` int(11) DEFAULT NULL,
  `numLampadasLigadas` int(11) DEFAULT NULL,
  `usouProjetor` tinyint(4) DEFAULT NULL,
  `usouQuadro` tinyint(4) DEFAULT NULL,
  `sala_idsala` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `aula_has_aluno`
--

CREATE TABLE `aula_has_aluno` (
  `idAula_has_aluno` int(11) NOT NULL,
  `localAlunoSentou` varchar(45) DEFAULT NULL,
  `numParticipacoes` int(11) DEFAULT NULL,
  `aluno_idaluno` int(11) NOT NULL,
  `aula_idaula` int(11) NOT NULL,
  `dormiu` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- --------------------------------------------------------

--
-- Estrutura para tabela `turma_has_sala`
--

CREATE TABLE `turma_has_sala` (
  `id` int(11) NOT NULL,
  `turma_idturma` int(11) NOT NULL,
  `sala_idsala` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `aluno`
--
ALTER TABLE `aluno`
  ADD PRIMARY KEY (`idaluno`),
  ADD KEY `fk_aluno_curso1_idx` (`curso_idcurso`),
  ADD KEY `fk_aluno_semestre1_idx` (`semestre_idtable1`);

--
-- Índices de tabela `aluno_has_turma`
--
ALTER TABLE `aluno_has_turma`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_aluno_has_turma_turma1_idx` (`turma_idturma`),
  ADD KEY `fk_aluno_has_turma_aluno1_idx` (`aluno_idaluno`);

--
-- Índices de tabela `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`idaula`),
  ADD KEY `fk_aula_turma1_idx` (`turma_idturma`),
  ADD KEY `fk_aula_professor1_idx` (`professor_idprofessor`),
  ADD KEY `fk_aula_sala1_idx` (`sala_idsala`);

--
-- Índices de tabela `aula_has_aluno`
--
ALTER TABLE `aula_has_aluno`
  ADD PRIMARY KEY (`idAula_has_aluno`),
  ADD KEY `fk_aula_has_aluno_aluno1_idx` (`aluno_idaluno`),
  ADD KEY `fk_aula_has_aluno_aula1_idx` (`aula_idaula`);

--
-- Índices de tabela `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`idcurso`);

--
-- Índices de tabela `disciplina`
--
ALTER TABLE `disciplina`
  ADD PRIMARY KEY (`iddisciplina`);

--
-- Índices de tabela `entradaSaida`
--
ALTER TABLE `entradaSaida`
  ADD PRIMARY KEY (`idEntradaSaida`),
  ADD KEY `fk_entradaSaida_aula_has_aluno1_idx` (`aula_has_aluno_idAula_has_aluno`);

--
-- Índices de tabela `informacoesAluno`
--
ALTER TABLE `informacoesAluno`
  ADD PRIMARY KEY (`idinformacoesAluno`),
  ADD KEY `fk_informacoesAluno_aluno1_idx` (`aluno_idaluno`);

--
-- Índices de tabela `professor`
--
ALTER TABLE `professor`
  ADD PRIMARY KEY (`idprofessor`);

--
-- Índices de tabela `sala`
--
ALTER TABLE `sala`
  ADD PRIMARY KEY (`idsala`);

--
-- Índices de tabela `semestre`
--
ALTER TABLE `semestre`
  ADD PRIMARY KEY (`idSemestre`);

--
-- Índices de tabela `turma`
--
ALTER TABLE `turma`
  ADD PRIMARY KEY (`idturma`),
  ADD KEY `fk_turma_disciplina1_idx` (`disciplina_iddisciplina`),
  ADD KEY `fk_turma_professor1_idx` (`professor_idprofessor`),
  ADD KEY `fk_turma_semestre1_idx` (`semestre_idtable1`);

--
-- Índices de tabela `turma_has_sala`
--
ALTER TABLE `turma_has_sala`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_turma_has_sala_sala1_idx` (`sala_idsala`),
  ADD KEY `fk_turma_has_sala_turma1_idx` (`turma_idturma`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `aluno`
--
ALTER TABLE `aluno`
  MODIFY `idaluno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de tabela `aluno_has_turma`
--
ALTER TABLE `aluno_has_turma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `aula`
--
ALTER TABLE `aula`
  MODIFY `idaula` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `aula_has_aluno`
--
ALTER TABLE `aula_has_aluno`
  MODIFY `idAula_has_aluno` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `curso`
--
ALTER TABLE `curso`
  MODIFY `idcurso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `disciplina`
--
ALTER TABLE `disciplina`
  MODIFY `iddisciplina` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `entradaSaida`
--
ALTER TABLE `entradaSaida`
  MODIFY `idEntradaSaida` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `informacoesAluno`
--
ALTER TABLE `informacoesAluno`
  MODIFY `idinformacoesAluno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `professor`
--
ALTER TABLE `professor`
  MODIFY `idprofessor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `sala`
--
ALTER TABLE `sala`
  MODIFY `idsala` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `semestre`
--
ALTER TABLE `semestre`
  MODIFY `idSemestre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `turma`
--
ALTER TABLE `turma`
  MODIFY `idturma` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `turma_has_sala`
--
ALTER TABLE `turma_has_sala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `aluno`
--
ALTER TABLE `aluno`
  ADD CONSTRAINT `fk_aluno_curso1` FOREIGN KEY (`curso_idcurso`) REFERENCES `curso` (`idcurso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_aluno_semestre1` FOREIGN KEY (`semestre_idtable1`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `aluno_has_turma`
--
ALTER TABLE `aluno_has_turma`
  ADD CONSTRAINT `fk_aluno_has_turma_aluno1` FOREIGN KEY (`aluno_idaluno`) REFERENCES `aluno` (`idaluno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_aluno_has_turma_turma1` FOREIGN KEY (`turma_idturma`) REFERENCES `turma` (`idturma`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `aula`
--
ALTER TABLE `aula`
  ADD CONSTRAINT `fk_aula_professor1` FOREIGN KEY (`professor_idprofessor`) REFERENCES `professor` (`idprofessor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_aula_sala1` FOREIGN KEY (`sala_idsala`) REFERENCES `sala` (`idsala`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_aula_turma1` FOREIGN KEY (`turma_idturma`) REFERENCES `turma` (`idturma`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `aula_has_aluno`
--
ALTER TABLE `aula_has_aluno`
  ADD CONSTRAINT `fk_aula_has_aluno_aluno1` FOREIGN KEY (`aluno_idaluno`) REFERENCES `aluno` (`idaluno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_aula_has_aluno_aula1` FOREIGN KEY (`aula_idaula`) REFERENCES `aula` (`idaula`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `entradaSaida`
--
ALTER TABLE `entradaSaida`
  ADD CONSTRAINT `fk_entradaSaida_aula_has_aluno1` FOREIGN KEY (`aula_has_aluno_idAula_has_aluno`) REFERENCES `aula_has_aluno` (`idAula_has_aluno`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `informacoesAluno`
--
ALTER TABLE `informacoesAluno`
  ADD CONSTRAINT `fk_informacoesAluno_aluno1` FOREIGN KEY (`aluno_idaluno`) REFERENCES `aluno` (`idaluno`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `turma`
--
ALTER TABLE `turma`
  ADD CONSTRAINT `fk_turma_disciplina1` FOREIGN KEY (`disciplina_iddisciplina`) REFERENCES `disciplina` (`iddisciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_turma_professor1` FOREIGN KEY (`professor_idprofessor`) REFERENCES `professor` (`idprofessor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_turma_semestre1` FOREIGN KEY (`semestre_idtable1`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `turma_has_sala`
--
ALTER TABLE `turma_has_sala`
  ADD CONSTRAINT `fk_turma_has_sala_sala1` FOREIGN KEY (`sala_idsala`) REFERENCES `sala` (`idsala`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_turma_has_sala_turma1` FOREIGN KEY (`turma_idturma`) REFERENCES `turma` (`idturma`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
