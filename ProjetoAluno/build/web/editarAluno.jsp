<%-- 
    Document   : editarAluno
    Created on : 18/10/2017, 14:00:58
    Author     : claudiomoises
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="database.Aluno"%>
<%@page import="database.Semestre"%>
<%@page import="java.util.List"%>
<%@page import="database.Curso"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%   
    //Verifica se está logado
    if (!(session.getAttribute("login") != null 
            && session.getAttribute("senha")  != null )) {
        out.println("<script>alert('Não está logado!'); document.location=('/ProjetoAluno'); </script>");
    }
    
    Aluno aluno = (Aluno) request.getAttribute("listInformacoesAluno");      
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar Aluno</title>
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>



        <div id="campos">
            <form action="CadastrarAluno" method="POST" >
                <h1>Editar Aluno</h1>
                <label>Mac:</label>
                <input type="text" name="macid" value="<%= aluno.getMacId()%>" />
                <label>Nome:</label>
                <input type="text" name="nome" value="<%= aluno.getNome()%>" />
                <label>Endereço:</label>
                <input type="text" name="endereco" value="<%= aluno.getEndereco()%>" />
                <label>Data de Nascimento:</label>
                <input type="date" name="dataNascimento" value="<%= (aluno.getDataNascimento())%>" />
                <label>Matrícula:</label>
                <input type="text" name="matricula" value="<%= aluno.getMatricula()%>" />
                <label>Curso:</label>
                <select name="cursos">
                    <option value="<%= aluno.getCurso().getIdcurso() %>"><%= aluno.getCurso().getNome() %></option>
                    <%  
                        List<Curso> list = (List) request.getAttribute("curso");
                        for (Curso teste : list) {
                            out.println("<option value='" + teste.getIdcurso() + "'>" + teste.getNome() + "</option>");
                        }
                    %>                      
                </select>
                <label>Semestre:</label>
                <select name="semestres">
                     <option value="<%= aluno.getSemestre().getIdSemestre()%>"><%= aluno.getSemestre().getNome() %></option>
                    <%
                        List<Semestre> listSemestre = (List) request.getAttribute("semestre");
                        for (Semestre teste : listSemestre) {
                            out.println("<option value='" + teste.getIdSemestre() + "'>" + teste.getNome() + "</option>");
                        }
                    %>                      
                </select>
                <br/>
                <label>IMEI:</label>
                <input type="text" name="imei" value="<%= aluno.getImei()%>" />
                 <label>IMEI2:</label>
                <input type="text" name="imei2" value="<%= aluno.getImei2()%>" />
                 <label>IMEI3:</label>
                <input type="text" name="imei3" value="<%= aluno.getImei3()%>" />
                <br/><br/>
                <input type="hidden" name="idAluno" value="<%= aluno.getIdaluno()%>" />
                <input type="submit" value="Cadastrar" />
            </form>
        </div>
        <br/><br/><br/>
        <a href="/ProjetoAluno/TelaAlunos"> <input type="button" value="Voltar" />  </a> 
    </body>
</html>

