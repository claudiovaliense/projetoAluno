<%-- 
    Document   : informacoesAluno
    Created on : 05/10/2017, 10:58:36
    Author     : claudiomoises
--%>

<%@page import="database.InformacoesAluno"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    //Verifica se está logado
    if (!(session.getAttribute("login") != null
            && session.getAttribute("senha") != null)) {
        out.println("<script>alert('Não está logado!'); document.location=('/ProjetoAluno'); </script>");
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Dados aplicativo aluno:</h1>
        <table>
            <th>ID</th>
            <th>MACID</th>
            <th>Data coleta</th>
            <th>Latitude</th>
            <th>Longitude</th>
            <th>Aplicativo aberto</th>
            <th>Está parado</th>
            <th>Luminosidade</th>         
            <th>Carga bateria</th>
            <th>Precisão GPS</th>
            <th>IMEI</th>

            <%
                List<InformacoesAluno> listInformacoesAluno = (List) request.getAttribute("listInformacoesAluno");
                for (InformacoesAluno informacoesAluno : listInformacoesAluno) {
                    out.print("<tr>");
                    out.println("<td>" + informacoesAluno.getIdinformacoesAluno() + "</td>");
                    out.println("<td>" + informacoesAluno.getMacId() + "</td>");
                    out.println("<td>" + informacoesAluno.getDataColeta() + "</td>");
                    out.println("<td>" + informacoesAluno.getLatitude() + "</td>");
                    out.println("<td>" + informacoesAluno.getLongitude() + "</td>");
                    out.println("<td>" + informacoesAluno.getAplicativoAberto() + "</td>");
                    out.println("<td>" + informacoesAluno.getEstaParado() + "</td>");
                    out.println("<td>" + informacoesAluno.getLuminosidade() + "</td>");
                    //out.println("<td>" + informacoesAluno.getBarulho() + "</td>");
                    out.println("<td>" + informacoesAluno.getCargaBateria() + "</td>");
                    out.println("<td>" + informacoesAluno.getPrecisaoGps() + "</td>");
                    out.println("<td>" + informacoesAluno.getImei() + "</td>");
                    out.print("</tr>");
                    ///out.println("<option value='" +teste.getIddisciplina()+"'>" + teste.getNomeDisciplina()+ "</option>");
                }
            %>  

        </table>
        <a href="/ProjetoAluno/menu.jsp"> <input type="button" value="Voltar" />  </a> 
    </body>
</html>
