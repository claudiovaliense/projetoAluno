<%-- 
    Document   : cadastrarAluno
    Created on : 05/09/2017, 14:00:47
    Author     : claudiomoises
--%>

<%@page import="database.Semestre"%>
<%@page import="java.util.List"%>
<%@page import="database.Curso"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%   
    //Verifica se está logado
    if (!(session.getAttribute("login") != null 
            && session.getAttribute("senha")  != null )) {
        out.println("<script>alert('Não está logado!'); document.location=('/ProjetoAluno'); </script>");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastrar Aluno</title>
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>



        <div id="campos">
            <form action="CadastrarAluno" method="POST" >
                <h1>Cadastro Aluno</h1>
                <label>Mac:</label>
                <input type="text" name="macid" />
                <label>Nome:</label>
                <input type="text" name="nome" />
                <label>Endereço:</label>
                <input type="text" name="endereco" />
                <label>Data de Nascimento:</label>
                <input type="date" name="dataNascimento"/>
                <label>Matrícula:</label>
                <input type="text" name="matricula"/>
                <label>Curso:</label>
                <select name="cursos">
                    <%                        List<Curso> list = (List) request.getAttribute("curso");
                        for (Curso teste : list) {
                            out.println("<option value='" + teste.getIdcurso() + "'>" + teste.getNome() + "</option>");
                        }
                    %>                      
                </select>
                <label>Semestre:</label>
                <select name="semestres">
                    <%
                        List<Semestre> listSemestre = (List) request.getAttribute("semestre");
                        for (Semestre teste : listSemestre) {
                            out.println("<option value='" + teste.getIdSemestre() + "'>" + teste.getNome() + "</option>");
                        }
                    %>                      
                </select>
                <br/>
                <label>IMEI</label>
                <input type="text" name="imei"/>
                <label>IMEI2</label>
                <input type="text" name="imei2"/>
                <label>IMEI3</label>
                <input type="text" name="imei3"/>
                <br/><br/>
                <input type="submit" value="Cadastrar" />
            </form>
        </div>
        <br/><br/><br/>
        <a href="/ProjetoAluno/menu.jsp"> <input type="button" value="Voltar" />  </a> 
    </body>
</html>
