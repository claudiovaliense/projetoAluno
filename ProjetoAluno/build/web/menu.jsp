<%-- 
    Document   : menu
    Created on : 21/09/2017, 13:19:31
    Author     : claudiomoises
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    //Verifica se está logado
    if (!(session.getAttribute("login") != null
            && session.getAttribute("senha") != null)) {
        out.println("<script>alert('Não está logado!'); document.location=('/ProjetoAluno'); </script>");
    }
    /*
    if (request.getParameter("login") != null) {
        if (request.getParameter("login").equals("adam")
                && request.getParameter("senha").equals("adam123")) {
            session.setAttribute("login", "adam");
            session.setAttribute("senha", "adam123");
        } else {
            out.println("<script>alert('Senha incorreta!'); document.location=('/ProjetoAluno'); </script>");
        }
    }else{
        out.println("<script>alert('Não está logado!'); document.location=('/ProjetoAluno'); </script>");
    }*/

%>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
       <div id="botoes">
            <img src="img/logo.jpg" style="width: 500px; height: 250px; margin-left: 250px; " />
            <h1>Cadastrar:</h1><br/>
            <a href="TelaCadastroAluno"><input type="button" value="Aluno" /></a>
            <a href="cadastrarCurso.jsp"><input type="button" value="Curso" /></a>
            <a href="cadastrarDisciplina.jsp"> <input type="button" value="Disciplina" /></a>
            <a href="cadastrarProfessor.jsp"><input type="button" value="Professor" /></a>
            <a href="cadastrarSala.jsp"><input type="button" value="Sala"/></a>
            <a href="cadastrarSemestre.jsp"><input type="button" value="Semestre" /></a>
            <a href="TelaCadastroTurma"><input type="button" value="Turma" /></a> 
            <a href="TelaCadastroAlunoTurma"><input type="button" value="Aluno/Turma" /></a> 
            <a href="TelaCadastroTurmaSala"><input type="button" value="Turma/Sala" /></a> 
            <h1>Consultar/Editar:</h1><br/>
            <a href="TelaInformacoesAluno"><input type="button" value="Coleta Aluno" /></a> 
            <!--<a href="TelaInformacoesAluno2"><input type="button" value="Coleta Aluno Distinc IMEI" /></a> -->
             <a href="TelaAlunos"><input type="button" value="Dados do Aluno" /></a> 
             <a href="TelaAulas"><input type="button" value="Dados da Aula" /></a> 
        </div>
    </body>
</html>
