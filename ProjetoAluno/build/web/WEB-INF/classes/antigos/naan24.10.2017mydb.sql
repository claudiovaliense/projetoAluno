-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 24-Out-2017 às 18:12
-- Versão do servidor: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydb`
--



-- --------------------------------------------------------

--
-- Estrutura da tabela `curso`
--

CREATE TABLE `curso` (
  `idcurso` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `curso`
--

INSERT INTO `curso` (`idcurso`, `nome`) VALUES
(1, 'Engenharia da Computacao');

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina`
--

CREATE TABLE `disciplina` (
  `iddisciplina` int(11) NOT NULL,
  `codigoDisciplina` varchar(45) DEFAULT NULL,
  `nomeDisciplina` varchar(45) DEFAULT NULL,
  `cargaHoraria` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `disciplina`
--

INSERT INTO `disciplina` (`iddisciplina`, `codigoDisciplina`, `nomeDisciplina`, `cargaHoraria`) VALUES
(1, 'EXA805', 'ALGORITMOS E PROGRAMACAO II', 30),
(2, 'EXA806', 'ESTRUTURA DE DADOS', 30),
(3, 'EXA801', 'ALGORITMOS E PROGRAMACAO I', 60);

-- --------------------------------------------------------

--
-- Estrutura da tabela `entradasaida`
--

CREATE TABLE `entradasaida` (
  `idEntradaSaida` int(11) NOT NULL,
  `dataEntrada` date DEFAULT NULL,
  `dataSaida` date DEFAULT NULL,
  `aula_has_aluno_idAula_has_aluno` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Estrutura da tabela `professor`
--

CREATE TABLE `professor` (
  `idprofessor` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `matricula` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `professor`
--

INSERT INTO `professor` (`idprofessor`, `nome`, `matricula`) VALUES
(1, 'JOAO B. ROCHA JR', '714189148'),
(2, 'CLAUDIA PINTO PEREIRA', '714442392');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sala`
--

CREATE TABLE `sala` (
  `idsala` int(11) NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `area` double DEFAULT NULL,
  `localizacaoTextual` varchar(45) DEFAULT NULL,
  `numJanelas` int(11) DEFAULT NULL,
  `numProjetor` int(11) DEFAULT NULL,
  `numQuadro` int(11) DEFAULT NULL,
  `numArCondicionado` int(11) DEFAULT NULL,
  `numVentilador` int(11) DEFAULT NULL,
  `numPortas` int(11) DEFAULT NULL,
  `numLampadas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `sala`
--

INSERT INTO `sala` (`idsala`, `latitude`, `longitude`, `area`, `localizacaoTextual`, `numJanelas`, `numProjetor`, `numQuadro`, `numArCondicionado`, `numVentilador`, `numPortas`, `numLampadas`) VALUES
(1, 121160, 385823, 0, 'PAT 51', 10, 1, 1, 0, 1, 1, 6),
(2, 121207, 385808, 0, 'PAV 02', 2, 1, 1, 0, 1, 1, 6),
(3, 121208, 385808, 0, 'PAV 11', 2, 1, 1, 0, 1, 1, 6),
(4, 121158, 385817, 0, 'PAT 32', 10, 1, 1, 0, 1, 1, 6),
(5, 121158, 385817, 0, 'PAT 33', 10, 1, 1, 0, 1, 1, 6),
(6, 121200, 385824, 0, 'PAT 52', 14, 1, 1, 0, 1, 1, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `semestre`
--

CREATE TABLE `semestre` (
  `idSemestre` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `dataInicio` date DEFAULT NULL,
  `dataFim` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `semestre`
--

INSERT INTO `semestre` (`idSemestre`, `nome`, `dataInicio`, `dataFim`) VALUES
(1, '2017.2', '2017-09-12', '2018-02-09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `turma`
--

CREATE TABLE `turma` (
  `idturma` int(11) NOT NULL,
  `codigoTurma` varchar(10) DEFAULT NULL,
  `horaInicio` int(11) DEFAULT NULL,
  `minutoInicio` int(11) DEFAULT NULL,
  `horaFim` int(11) DEFAULT NULL,
  `minutoFim` int(11) DEFAULT NULL,
  `disciplina_iddisciplina` int(11) NOT NULL,
  `professor_idprofessor` int(11) NOT NULL,
  `semestre_idtable1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `turma`
--

INSERT INTO `turma` (`idturma`, `codigoTurma`, `horaInicio`, `minutoInicio`, `horaFim`, `minutoFim`, `disciplina_iddisciplina`, `professor_idprofessor`, `semestre_idtable1`) VALUES
(1, 'EXA805-T01', 15, 30, 17, 30, 1, 1, 1),
(2, 'EXA806-T01', 13, 30, 15, 30, 2, 1, 1),
(3, 'EXA806-T02', 15, 30, 17, 30, 2, 1, 1),
(4, 'EXA801-T02', 13, 30, 15, 30, 3, 2, 1);


-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno`
--

CREATE TABLE `aluno` (
  `idaluno` int(11) NOT NULL,
  `macId` varchar(30) DEFAULT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `dataNascimento` date DEFAULT NULL,
  `matricula` varchar(45) DEFAULT NULL,
  `curso_idcurso` int(11) NOT NULL,
  `semestre_idtable1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `aluno`
--

INSERT INTO `aluno` (`idaluno`, `macId`, `nome`, `endereco`, `dataNascimento`, `matricula`, `curso_idcurso`, `semestre_idtable1`) VALUES
(1, 'cc:07:ab:0e:4f:15', 'ADRIEL SOARES DE BRITO', 'Rua São João de Meriti, 432, Campo Limpo', '1999-11-29', '17111188', 1, 1),
(2, '94:01:c2:1d:d2:92', 'ALESSANDRO COSTA DA SILVA CARNEIRO', 'Rua A, n 166, Feira IX', '1992-03-20', '15111152', 1, 1),
(3, 'b8:1d:aa:a3:c2:40', 'ALMIR MOREIRA DA SILVA NETO', 'Rua A, Caminho 8 casa 2, Conjunto feira IV, t', '1999-05-30', '17111207', 1, 1),
(4, '0', 'ALYSON ALVARES BRITO MORAIS', '0', '2017-09-01', '14211146', 1, 1),
(5, '0', 'ANGELO RAFAEL MOTA GOMES', '0', '2017-09-01', '11211149', 1, 1),
(6, '40:88:05:a0:d6:a1', 'ARTHUR HENRIQUE MOTA BRITO SANTOS', 'Rua G, n. 110 Bairro Muchila I', '2000-01-13', '17111183', 1, 1),
(7, '0', 'ARTUR CAVALCANTE OLIVEIRA', '0', '2017-09-01', '17111196', 1, 1),
(8, '88:79:7e:69:52:dc', 'AURELIO ROCHA BARRETO', 'Rua A, n 17, Conjunto Feira 6', '1996-12-14', '16111164', 1, 1),
(9, 'e0:98:61:59:f1:99', 'CARLOS ANDRE PEREIRA TININ', 'Rua André Vidal de Negreiros, 610, Tomba.', '1999-07-31', '17111195', 1, 1),
(10, '0', 'HILDEBRANDO SIMOES DE ARAUJO NETO', '0', '2017-09-01', '17111187', 1, 1),
(11, '08:62:66:41:e3:4b', 'IAGO MACHADO DA CONCEICAO SILVA', 'Conjunto Feira VI, Rua I, Edificio Koppenhagu', '1998-08-02', '17111174', 1, 1),
(12, '0', 'JADER JOSE CERQUEIRA DO NASCIMENTO', 'Avenida Cazumbá, n 34, São Gonçalo dos Campos', '1997-04-16', '15211163', 1, 1),
(13, '80:58:f8:ae:10:57', 'JOSE WILKER PEREIRA DOS SANTOS', 'Rua Olivenca, n 09, Mangabeira', '1989-01-17', '15211186', 1, 1),
(14, 'c4:9a:02:05:1d:1b', 'LAERCIO DE SOUZA RIOS', '1ª Travessa H, n 9 apto 4, Conjunto Habitacio', '1999-08-18', '17111175', 1, 1),
(15, '0', 'LARISSA SILVA SAMPAIO', '0', '2017-09-01', '14111161', 1, 1),
(16, '0', 'LEONARDO MASCARENHAS GUSMAO', '0', '2017-09-01', '15211173', 1, 1),
(17, '0', 'LUCIANO ARAUJO DOURADO FILHO', 'Rua Piracicaba, n 18, Quadra B', '1998-10-03', '17111203', 1, 1),
(18, 'e8:91:20:74:85:bf', 'MATEUS GUIMARAES DOS SANTOS LIMA', 'Rua Visconde de Cairu, Ponto Central ', '1998-07-22', '17111186', 1, 1),
(19, 'a8:9f:ba:15:51:d8', 'MATHEUS TELES DE OLIVEIRA', 'Rua Jurema, Prédio Platinum 100, apto 103, Fe', '1999-04-04', '17111201', 1, 1),
(20, '1c:56:fe:39:12:10', 'NATALIA SILVA ROSA', 'Rua Targino, 41, Loteamento Monte Pascoal, Ba', '2017-09-01', '17111191', 1, 1),
(21, '30:cb:f8:ab:e1:f8', 'RAFAEL DOS REIS AZEVEDO', 'Rua Jurema, predio Platinum, Apto 304, Feira ', '1998-09-26', '17111202', 1, 1),
(22, '0', 'THIAGO DA CUNHA MENEZES SOUZA', '0', '2017-09-01', '15211182', 1, 1),
(23, '00:0a:f5:6b:21:8c', 'VICTOR LOPES MATOS SILVA', 'Rua Mazagao', '1993-09-01', '17111213', 1, 1),
(24, '0', 'VYCTOR ASSUNCAO ALVES', '0', '2017-09-01', '15211187', 1, 1),
(25, '0', 'ALEXANDRE RIBEIRO CARNEIRO', '0', '2017-09-01', '16111199', 1, 1),
(26, '0', 'ANANIAS CORREIA DO NASCIMENTO', '0', '2017-09-01', '16111187', 1, 1),
(27, '0', 'ARTHUR LESSA LEITE', '0', '2017-09-01', '15111171', 1, 1),
(28, '0', 'BRENDO NASCIMENTO DOS SANTOS SOUSA', '0', '2017-09-01', '16111181', 1, 1),
(29, '0', 'BRUNO CLAUDINO MATIAS', '0', '2017-09-01', '16111195', 1, 1),
(30, '0', 'GABRIEL DA CRUZ SILVA', 'o', '2017-09-01', '16111177', 1, 1),
(31, '80:58:f8:42:47:70', 'GABRIEL SILVA DE AZEVEDO', 'Rua C, N 580, Conjunto ACM, Mangabeira', '1998-12-20', '16111167', 1, 1),
(32, '40:88:05:e9:d9:96', 'ITAMAR MORGADO DA SILVA', 'Rua A, n 45, Conjunto Feira 6', '1994-09-06', '13211175', 1, 1),
(33, 'cc:fa:00:fb:cc:c4', 'JAEVILLEN FERREIRA DE OLIVEIRA', 'Travessa Bartolomeu de Gusmao, 40, Sobradinho', '2000-06-27', '17111176', 1, 1),
(34, '0', 'LUAN VICTOR TEIXEIRA DE BRITO', '0', '2017-09-01', '15111173', 1, 1),
(35, '0', 'LUCAS DOS SANTOS ROCHA', '0', '2017-09-01', '16111202', 1, 1),
(36, '0', 'LUCAS MACHADO LEVI', '0', '2017-09-01', '17111199', 1, 1),
(37, '0', 'MATHEUS ALMEIDA LIMA', '0', '2017-09-01', '13111178', 1, 1),
(38, '0', 'ROBSON JONES DE LIMA', '0', '2017-09-01', '13211189', 1, 1),
(39, '0', 'ROGERIO GABRIEL RODRIGUES SOUZA', '0', '2017-09-01', '15111182', 1, 1),
(40, '2c:59:8a:66:74:f2', 'VAGNER JOSE DOS SANTOS', 'Caminho 27, Conjunto Feira VI, n 6 A', '1994-03-17', '14211180', 1, 1),
(41, '0', 'VANESSA NOGUEIRA LOPES MAIA', '0', '2017-09-01', '16111201', 1, 1),
(42, '0', 'VINICIUS DIAS DE JESUS MACIEL', '0', '2017-09-01', '16111163', 1, 1),
(43, 'f0:25:b7:de:c9:c3', 'WERISSON NASCIMENTO MOTA', '0', '1996-04-18', '14211184', 1, 1),
(44, '18:83:31:09:f5:e4', 'ABEL RAMALHO GALVAO', 'Caminho 16, Conjunto Feira V, Mangabeira', '1996-07-19', '16111169', 1, 1),
(45, 'cc:61:e5:38:8f:52', 'JOHNNY DA SILVA SANTANA', 'Rua A, n. 103, Feira 6', '1996-06-06', '15211168', 1, 1),
(46, '0', 'LUCAS SILVA LIMA', '0', '2017-09-01', '16111165', 1, 1),
(47, '0', 'MARCELO PINTO NETO', '0', '2017-09-01', '14211182', 1, 1),
(48, '00:16:98:0c:89:21', 'PEDRO HENRIQUE BRANDAO OLIVEIRA BASTOS', 'Rua 4, Papagaio', '1995-01-11', '13211179', 1, 1),
(49, '0', 'ANDERSON DA LUZ CORREIA', '0', '2017-09-01', '17211155', 1, 1),
(50, '0', 'ANESIO SOUSA DOS SANTOS NETO', '0', '2017-09-01', '17111205', 1, 1),
(51, '0', 'ARIANA DE JESUS SANTOS PEREIRA', '0', '2017-09-01', '17211160', 1, 1),
(52, '0', 'CLERISTON OLIVEIRA BESSA', '0', '2017-09-01', '17211151', 1, 1),
(53, 'd4:63:c6:20:6f:b1', 'ED CARLOS MENDES SOARES', 'Rua Aeronave, Campo Limpo', '1999-04-26', '17111182', 1, 1),
(54, '0', 'ESTHER DE SANTANA ARAUJO', '0', '2017-09-01', '17211159', 1, 1),
(55, '0', 'FABIO COSME FREITAS ALMEIDA', '0', '2017-09-01', '17211156', 1, 1),
(56, '0', 'JOAO LUCAS DE MAGALHAES LEAL MOREIRA', '0', '2017-09-01', '17111185', 1, 1),
(57, '0', 'JOAO PEDRO BORGES GOMES', '0', '2017-09-01', '17211158', 1, 1),
(58, '0', 'JOAO PEDRO COSTA FERREIRA', '0', '2017-09-01', '17111178', 1, 1),
(59, '0', 'JORGE MOREIRA DA COSTA JUNIOR', '0', '2017-09-01', '17211152', 1, 1),
(60, '68:c4:4d:83:10:60', 'JULIANA ARAGAO PINTO', 'Rua José Nunes, 171, Centro, Amélia Rodrigues', '1998-02-20', '17211162', 1, 1),
(61, '0', 'MARIANA DA SILVA LIMA SANTOS', '0', '2017-09-01', '17211157', 1, 1),
(62, '68:c4:4d:60:0f:72', 'MATEUS DE ALMEIDA MIRANDA', 'Avenida Maria Quiteria, Brasilia, 597.', '1998-10-05', '17211154', 1, 1),
(63, '0', 'RAMON DE CERQUEIRA SILVA', '0', '2017-09-01', '17111210', 1, 1),
(64, '0', 'ROGERIO DOS SANTOS CERQUEIRA', '0', '2017-09-01', '16111188', 1, 1),
(65, '0', 'ROMARIO DOS ANJOS NASCIMENTO', '0', '2017-09-01', '17211153', 1, 1),
(66, '0', 'SERGIO PUGLIESI DE MATOS', '0', '2017-09-01', '17211161', 1, 1),
(67, '0', 'TIAGO DE FIGUEIREDO MOURA', '0', '2017-09-01', '17111197', 1, 1),
(68, '0', 'VICTOR CESAR DA ROCHA BASTOS', '0', '2017-09-01', '17111211', 1, 1),
(69, '8:50:8b:27:59:08', 'DANIEL ALVES COSTA', 'Rua Luz e Fraternidade, 14, Jardim Acácia', '1999-03-11', '17211166', 1, 1),
(70, '0', 'IGOR LIMA GONÇALVES', '0', '2017-09-01', '17211167', 1, 1),
(71, '0', 'DAVI JERONIMO DOS SANTOS SAMPAIO', '0', '2017-09-01', '17211165', 1, 1),
(72, '0', 'ERICK MOREIRA DE JESUS CAMPOS', '0', '2017-09-01', '17211163', 1, 1),
(73, '0', 'VITOR LIMA ROMA DOS SANTOS', '0', '2017-09-01', '17211164', 1, 1),
(74, '0', 'YAN LIMA ROMA DOS SANTOS', '0', '2017-09-01', '16111170', 1, 1);



-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno_has_turma`
--

CREATE TABLE `aluno_has_turma` (
  `id` int(11) NOT NULL,
  `aluno_idaluno` int(11) NOT NULL,
  `turma_idturma` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `aluno_has_turma`
--

INSERT INTO `aluno_has_turma` (`id`, `aluno_idaluno`, `turma_idturma`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 1),
(14, 14, 1),
(15, 15, 1),
(16, 16, 1),
(17, 17, 1),
(18, 18, 1),
(19, 19, 1),
(20, 20, 1),
(21, 21, 1),
(22, 22, 1),
(23, 23, 1),
(24, 24, 1),
(25, 2, 2),
(28, 25, 2),
(29, 3, 2),
(30, 26, 2),
(31, 27, 2),
(32, 7, 2),
(33, 28, 2),
(34, 29, 2),
(35, 30, 2),
(36, 31, 2),
(37, 32, 2),
(38, 12, 2),
(39, 33, 2),
(40, 13, 2),
(41, 34, 2),
(42, 35, 2),
(43, 36, 2),
(44, 37, 2),
(45, 38, 2),
(46, 39, 2),
(47, 40, 2),
(48, 41, 2),
(49, 42, 2),
(50, 43, 2),
(51, 44, 3),
(52, 1, 3),
(53, 4, 3),
(54, 5, 3),
(55, 6, 3),
(56, 8, 3),
(57, 9, 3),
(58, 10, 3),
(59, 11, 3),
(60, 45, 3),
(61, 14, 3),
(62, 15, 3),
(63, 16, 3),
(64, 46, 3),
(65, 17, 3),
(66, 47, 3),
(67, 18, 3),
(68, 19, 3),
(69, 20, 3),
(70, 48, 3),
(71, 21, 3),
(72, 22, 3),
(73, 23, 3),
(74, 24, 3),
(75, 49, 4),
(76, 50, 4),
(77, 51, 4),
(78, 52, 4),
(79, 53, 4),
(80, 54, 4),
(81, 55, 4),
(82, 56, 4),
(83, 57, 4),
(84, 58, 4),
(85, 59, 4),
(86, 60, 4),
(87, 61, 4),
(88, 62, 4),
(89, 63, 4),
(90, 64, 4),
(91, 65, 4),
(92, 66, 4),
(93, 67, 4),
(94, 68, 4),
(95, 69, 4),
(96, 70, 4),
(97, 71, 4),
(98, 72, 4),
(99, 73, 4),
(100, 74, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula`
--

CREATE TABLE `aula` (
  `idaula` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `chegadaProfessor` datetime DEFAULT NULL,
  `saidaProfessor` datetime DEFAULT NULL,
  `inicioAula` datetime DEFAULT NULL,
  `fimAula` datetime DEFAULT NULL,
  `turma_idturma` int(11) NOT NULL,
  `professor_idprofessor` int(11) NOT NULL,
  `numJanelasAbertas` int(11) DEFAULT NULL,
  `numVentiladorLigados` int(11) DEFAULT NULL,
  `numArCondicionadoLigado` int(11) DEFAULT NULL,
  `temperatura` int(11) DEFAULT NULL,
  `barulho` int(11) DEFAULT NULL,
  `luminosidade` int(11) DEFAULT NULL,
  `numLampadasLigadas` int(11) DEFAULT NULL,
  `usouProjetor` tinyint(4) DEFAULT NULL,
  `usouQuadro` tinyint(4) DEFAULT NULL,
  `sala_idsala` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `aula`
--

INSERT INTO `aula` (`idaula`, `nome`, `chegadaProfessor`, `saidaProfessor`, `inicioAula`, `fimAula`, `turma_idturma`, `professor_idprofessor`, `numJanelasAbertas`, `numVentiladorLigados`, `numArCondicionadoLigado`, `temperatura`, `barulho`, `luminosidade`, `numLampadasLigadas`, `usouProjetor`, `usouQuadro`, `sala_idsala`) VALUES
(0, 'Introdução a Java. Ambiente de compilação. In', '2017-09-12 15:30:00', '2017-09-12 17:27:00', '2017-09-12 15:40:00', '2017-09-12 17:22:00', 1, 1, 6, 0, 0, 0, 70, 63, 4, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula_has_aluno`
--

CREATE TABLE `aula_has_aluno` (
  `idAula_has_aluno` int(11) NOT NULL,
  `localAlunoSentou` varchar(45) DEFAULT NULL,
  `numParticipacoes` int(11) DEFAULT NULL,
  `aluno_idaluno` int(11) NOT NULL,
  `aula_idaula` int(11) NOT NULL,
  `dormiu` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Estrutura da tabela `informacoesaluno`
--

CREATE TABLE `informacoesaluno` (
  `idinformacoesAluno` int(11) NOT NULL,
  `dataColeta` datetime DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `aplicativoAberto` varchar(45) DEFAULT NULL,
  `estaParado` tinyint(4) DEFAULT NULL,
  `aluno_idaluno` int(11) DEFAULT NULL,
  `macId` varchar(30) DEFAULT NULL,
  `luminosidade` double DEFAULT NULL,
  `barulho` double DEFAULT NULL,
  `cargaBateria` int(11) DEFAULT NULL,
  `precisaoGPS` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- --------------------------------------------------------

--
-- Estrutura da tabela `turma_has_sala`
--

CREATE TABLE `turma_has_sala` (
  `id` int(11) NOT NULL,
  `turma_idturma` int(11) NOT NULL,
  `sala_idsala` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `turma_has_sala`
--

INSERT INTO `turma_has_sala` (`id`, `turma_idturma`, `sala_idsala`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 4, 5),
(6, 4, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aluno`
--
ALTER TABLE `aluno`
  ADD PRIMARY KEY (`idaluno`),
  ADD KEY `fk_aluno_curso1_idx` (`curso_idcurso`),
  ADD KEY `fk_aluno_semestre1_idx` (`semestre_idtable1`);

--
-- Indexes for table `aluno_has_turma`
--
ALTER TABLE `aluno_has_turma`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_aluno_has_turma_turma1_idx` (`turma_idturma`),
  ADD KEY `fk_aluno_has_turma_aluno1_idx` (`aluno_idaluno`);

--
-- Indexes for table `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`idaula`),
  ADD KEY `fk_aula_turma1_idx` (`turma_idturma`),
  ADD KEY `fk_aula_professor1_idx` (`professor_idprofessor`),
  ADD KEY `fk_aula_sala1_idx` (`sala_idsala`);

--
-- Indexes for table `aula_has_aluno`
--
ALTER TABLE `aula_has_aluno`
  ADD PRIMARY KEY (`idAula_has_aluno`),
  ADD KEY `fk_aula_has_aluno_aluno1_idx` (`aluno_idaluno`),
  ADD KEY `fk_aula_has_aluno_aula1_idx` (`aula_idaula`);

--
-- Indexes for table `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`idcurso`);

--
-- Indexes for table `disciplina`
--
ALTER TABLE `disciplina`
  ADD PRIMARY KEY (`iddisciplina`);

--
-- Indexes for table `entradasaida`
--
ALTER TABLE `entradasaida`
  ADD PRIMARY KEY (`idEntradaSaida`),
  ADD KEY `fk_entradaSaida_aula_has_aluno1_idx` (`aula_has_aluno_idAula_has_aluno`);

--
-- Indexes for table `informacoesaluno`
--
ALTER TABLE `informacoesaluno`
  ADD PRIMARY KEY (`idinformacoesAluno`),
  ADD KEY `fk_informacoesAluno_aluno1_idx` (`aluno_idaluno`);

--
-- Indexes for table `professor`
--
ALTER TABLE `professor`
  ADD PRIMARY KEY (`idprofessor`);

--
-- Indexes for table `sala`
--
ALTER TABLE `sala`
  ADD PRIMARY KEY (`idsala`);

--
-- Indexes for table `semestre`
--
ALTER TABLE `semestre`
  ADD PRIMARY KEY (`idSemestre`);

--
-- Indexes for table `turma`
--
ALTER TABLE `turma`
  ADD PRIMARY KEY (`idturma`),
  ADD KEY `fk_turma_disciplina1_idx` (`disciplina_iddisciplina`),
  ADD KEY `fk_turma_professor1_idx` (`professor_idprofessor`),
  ADD KEY `fk_turma_semestre1_idx` (`semestre_idtable1`);

--
-- Indexes for table `turma_has_sala`
--
ALTER TABLE `turma_has_sala`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_turma_has_sala_sala1_idx` (`sala_idsala`),
  ADD KEY `fk_turma_has_sala_turma1_idx` (`turma_idturma`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aluno`
--
ALTER TABLE `aluno`
  MODIFY `idaluno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `aluno_has_turma`
--
ALTER TABLE `aluno_has_turma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `aula`
--
ALTER TABLE `aula`
  MODIFY `idaula` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aula_has_aluno`
--
ALTER TABLE `aula_has_aluno`
  MODIFY `idAula_has_aluno` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `curso`
--
ALTER TABLE `curso`
  MODIFY `idcurso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `disciplina`
--
ALTER TABLE `disciplina`
  MODIFY `iddisciplina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `entradasaida`
--
ALTER TABLE `entradasaida`
  MODIFY `idEntradaSaida` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `informacoesaluno`
--
ALTER TABLE `informacoesaluno`
  MODIFY `idinformacoesAluno` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `professor`
--
ALTER TABLE `professor`
  MODIFY `idprofessor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sala`
--
ALTER TABLE `sala`
  MODIFY `idsala` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `semestre`
--
ALTER TABLE `semestre`
  MODIFY `idSemestre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `turma`
--
ALTER TABLE `turma`
  MODIFY `idturma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `turma_has_sala`
--
ALTER TABLE `turma_has_sala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `aluno`
--
ALTER TABLE `aluno`
  ADD CONSTRAINT `fk_aluno_curso1` FOREIGN KEY (`curso_idcurso`) REFERENCES `curso` (`idcurso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_aluno_semestre1` FOREIGN KEY (`semestre_idtable1`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `aluno_has_turma`
--
ALTER TABLE `aluno_has_turma`
  ADD CONSTRAINT `fk_aluno_has_turma_aluno1` FOREIGN KEY (`aluno_idaluno`) REFERENCES `aluno` (`idaluno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_aluno_has_turma_turma1` FOREIGN KEY (`turma_idturma`) REFERENCES `turma` (`idturma`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `aula`
--
ALTER TABLE `aula`
  ADD CONSTRAINT `fk_aula_professor1` FOREIGN KEY (`professor_idprofessor`) REFERENCES `professor` (`idprofessor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_aula_sala1` FOREIGN KEY (`sala_idsala`) REFERENCES `sala` (`idsala`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_aula_turma1` FOREIGN KEY (`turma_idturma`) REFERENCES `turma` (`idturma`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `aula_has_aluno`
--
ALTER TABLE `aula_has_aluno`
  ADD CONSTRAINT `fk_aula_has_aluno_aluno1` FOREIGN KEY (`aluno_idaluno`) REFERENCES `aluno` (`idaluno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_aula_has_aluno_aula1` FOREIGN KEY (`aula_idaula`) REFERENCES `aula` (`idaula`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `entradasaida`
--
ALTER TABLE `entradasaida`
  ADD CONSTRAINT `fk_entradaSaida_aula_has_aluno1` FOREIGN KEY (`aula_has_aluno_idAula_has_aluno`) REFERENCES `aula_has_aluno` (`idAula_has_aluno`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `informacoesaluno`
--
ALTER TABLE `informacoesaluno`
  ADD CONSTRAINT `fk_informacoesAluno_aluno1` FOREIGN KEY (`aluno_idaluno`) REFERENCES `aluno` (`idaluno`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `turma`
--
ALTER TABLE `turma`
  ADD CONSTRAINT `fk_turma_disciplina1` FOREIGN KEY (`disciplina_iddisciplina`) REFERENCES `disciplina` (`iddisciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_turma_professor1` FOREIGN KEY (`professor_idprofessor`) REFERENCES `professor` (`idprofessor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_turma_semestre1` FOREIGN KEY (`semestre_idtable1`) REFERENCES `semestre` (`idSemestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `turma_has_sala`
--
ALTER TABLE `turma_has_sala`
  ADD CONSTRAINT `fk_turma_has_sala_sala1` FOREIGN KEY (`sala_idsala`) REFERENCES `sala` (`idsala`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_turma_has_sala_turma1` FOREIGN KEY (`turma_idturma`) REFERENCES `turma` (`idturma`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
