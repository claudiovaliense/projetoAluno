-- File name: C:\Users\Usuario\meuScript.sql
-- Created by DBConvert http://www.dbconvert.com


--
-- Table structure for table `curso`
--

CREATE TABLE "curso" (  "id_curso" SERIAL NOT NULL ,
  "nome" VARCHAR NULL ,
  PRIMARY KEY ("id_curso")
); 


--
-- Table structure for table `semestre`
--

CREATE TABLE "semestre" (  "id_semestre" SERIAL NOT NULL ,
  "nome" VARCHAR NULL ,
  "data_inicio" DATE NULL ,
  "data_fim" DATE NULL ,
  PRIMARY KEY ("id_semestre")
); 


--
-- Table structure for table `aluno`
--

CREATE TABLE "aluno" (  "id_aluno" SERIAL NOT NULL ,
  "mac_id" VARCHAR NULL ,
  "nome" VARCHAR NULL ,
  "endereco" VARCHAR NULL ,
  "data_nascimento" DATE NULL ,
  "matricula" VARCHAR NULL ,
  "curso_idcurso" INTEGER NOT NULL ,
  "semestre_idtable1" INTEGER NOT NULL ,
  PRIMARY KEY ("id_aluno"),FOREIGN KEY ("curso_idcurso") REFERENCES "curso" ( "id_curso" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("semestre_idtable1") REFERENCES "semestre" ( "id_semestre" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
CREATE INDEX "aluno_fk_aluno_curso1_idx" ON "aluno" ("curso_idcurso");
CREATE INDEX "aluno_fk_aluno_semestre1_idx" ON "aluno" ("semestre_idtable1");


--
-- Table structure for table `informacoes_aluno`
--

CREATE TABLE "informacoes_aluno" (  "id_informacoes_aluno" SERIAL NOT NULL ,
  "data_coleta" TIMESTAMP NULL ,
  "latitude" DOUBLE PRECISION NULL ,
  "longitude" DOUBLE PRECISION NULL ,
  "aplicativo_aberto" VARCHAR NULL ,
  "esta_parado" SMALLINT NULL ,
  "aluno_idaluno" INTEGER NOT NULL ,
  PRIMARY KEY ("id_informacoes_aluno"),FOREIGN KEY ("aluno_idaluno") REFERENCES "aluno" ( "id_aluno" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
CREATE INDEX "informacoes_aluno_fk_informacoesAluno_aluno1_idx" ON "informacoes_aluno" ("aluno_idaluno");


--
-- Table structure for table `disciplina`
--

CREATE TABLE "disciplina" (  "id_disciplina" SERIAL NOT NULL ,
  "codigo_disciplina" VARCHAR NULL ,
  "nome_disciplina" VARCHAR NULL ,
  "carga_horaria" INTEGER NULL ,
  PRIMARY KEY ("id_disciplina")
); 


--
-- Table structure for table `sala`
--

CREATE TABLE "sala" (  "id_sala" SERIAL NOT NULL ,
  "latitude" DOUBLE PRECISION NULL ,
  "longitude" DOUBLE PRECISION NULL ,
  "area" DOUBLE PRECISION NULL ,
  "localizacao_textual" VARCHAR NULL ,
  "num_janelas" INTEGER NULL ,
  "num_projetor" INTEGER NULL ,
  "num_quadro" INTEGER NULL ,
  "num_ar_condicionado" INTEGER NULL ,
  "num_ventilador" INTEGER NULL ,
  "num_portas" INTEGER NULL ,
  "num_lampadas" INTEGER NULL ,
  PRIMARY KEY ("id_sala")
); 


--
-- Table structure for table `professor`
--

CREATE TABLE "professor" (  "id_professor" SERIAL NOT NULL ,
  "nome" VARCHAR NULL ,
  "matricula" VARCHAR NULL ,
  PRIMARY KEY ("id_professor")
); 


--
-- Table structure for table `turma`
--

CREATE TABLE "turma" (  "id_turma" SERIAL NOT NULL ,
  "codigo_turma" VARCHAR NULL ,
  "hora_inicio" INTEGER NULL ,
  "minuto_inicio" INTEGER NULL ,
  "hora_fim" INTEGER NULL ,
  "minuto_fim" INTEGER NULL ,
  "disciplina_iddisciplina" INTEGER NOT NULL ,
  "professor_idprofessor" INTEGER NOT NULL ,
  "semestre_idtable1" INTEGER NOT NULL ,
  PRIMARY KEY ("id_turma"),FOREIGN KEY ("disciplina_iddisciplina") REFERENCES "disciplina" ( "id_disciplina" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("professor_idprofessor") REFERENCES "professor" ( "id_professor" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("semestre_idtable1") REFERENCES "semestre" ( "id_semestre" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
CREATE INDEX "turma_fk_turma_disciplina1_idx" ON "turma" ("disciplina_iddisciplina");
CREATE INDEX "turma_fk_turma_professor1_idx" ON "turma" ("professor_idprofessor");
CREATE INDEX "turma_fk_turma_semestre1_idx" ON "turma" ("semestre_idtable1");


--
-- Table structure for table `aula`
--

CREATE TABLE "aula" (  "id_aula" SERIAL NOT NULL ,
  "nome" VARCHAR NULL ,
  "chegada_professor" TIMESTAMP NULL ,
  "saida_professor" TIMESTAMP NULL ,
  "inicio_aula" TIMESTAMP NULL ,
  "fim_aula" TIMESTAMP NULL ,
  "turma_idturma" INTEGER NOT NULL ,
  "professor_idprofessor" INTEGER NOT NULL ,
  "num_janelas_abertas" INTEGER NULL ,
  "num_ventilador_ligados" INTEGER NULL ,
  "num_ar_condicionado_ligado" INTEGER NULL ,
  "temperatura" INTEGER NULL ,
  "barulho" INTEGER NULL ,
  "luminosidade" INTEGER NULL ,
  "num_lampadas_ligadas" INTEGER NULL ,
  "usou_projetor" SMALLINT NULL ,
  "usou_quadro" SMALLINT NULL ,
  "sala_idsala" INTEGER NOT NULL ,
  PRIMARY KEY ("id_aula"),FOREIGN KEY ("professor_idprofessor") REFERENCES "professor" ( "id_professor" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("sala_idsala") REFERENCES "sala" ( "id_sala" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("turma_idturma") REFERENCES "turma" ( "id_turma" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
CREATE INDEX "aula_fk_aula_turma1_idx" ON "aula" ("turma_idturma");
CREATE INDEX "aula_fk_aula_professor1_idx" ON "aula" ("professor_idprofessor");
CREATE INDEX "aula_fk_aula_sala1_idx" ON "aula" ("sala_idsala");


--
-- Table structure for table `aula_has_aluno`
--

CREATE TABLE "aula_has_aluno" (  "id_aula_has_aluno" SERIAL NOT NULL ,
  "local_aluno_sentou" VARCHAR NULL ,
  "num_participacoes" INTEGER NULL ,
  "aluno_idaluno" INTEGER NOT NULL ,
  "aula_idaula" INTEGER NOT NULL ,
  PRIMARY KEY ("id_aula_has_aluno"),FOREIGN KEY ("aluno_idaluno") REFERENCES "aluno" ( "id_aluno" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("aula_idaula") REFERENCES "aula" ( "id_aula" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
CREATE INDEX "aula_has_aluno_fk_aula_has_aluno_aluno1_idx" ON "aula_has_aluno" ("aluno_idaluno");
CREATE INDEX "aula_has_aluno_fk_aula_has_aluno_aula1_idx" ON "aula_has_aluno" ("aula_idaula");


--
-- Table structure for table `aluno_has_turma`
--

CREATE TABLE "aluno_has_turma" (  "id_aluno_has_turma" SERIAL NOT NULL ,
  "aluno_idaluno" INTEGER NOT NULL ,
  "turma_idturma" INTEGER NOT NULL ,
  PRIMARY KEY ("id_aluno_has_turma"),FOREIGN KEY ("aluno_idaluno") REFERENCES "aluno" ( "id_aluno" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("turma_idturma") REFERENCES "turma" ( "id_turma" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
CREATE INDEX "aluno_has_turma_fk_aluno_has_turma_turma1_idx" ON "aluno_has_turma" ("turma_idturma");
CREATE INDEX "aluno_has_turma_fk_aluno_has_turma_aluno1_idx" ON "aluno_has_turma" ("aluno_idaluno");


--
-- Table structure for table `turma_has_sala`
--

CREATE TABLE "turma_has_sala" (  "id_turma_has_sala" SERIAL NOT NULL ,
  "turma_idturma" INTEGER NOT NULL ,
  "sala_idsala" INTEGER NOT NULL ,
  PRIMARY KEY ("id_turma_has_sala"),FOREIGN KEY ("sala_idsala") REFERENCES "sala" ( "id_sala" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("turma_idturma") REFERENCES "turma" ( "id_turma" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
CREATE INDEX "turma_has_sala_fk_turma_has_sala_sala1_idx" ON "turma_has_sala" ("sala_idsala");
CREATE INDEX "turma_has_sala_fk_turma_has_sala_turma1_idx" ON "turma_has_sala" ("turma_idturma");


--
-- Table structure for table `entrada_saida`
--

CREATE TABLE "entrada_saida" (  "id_entrada_saida" SERIAL NOT NULL ,
  "data_entrada" DATE NULL ,
  "data_saida" DATE NULL ,
  "aula_has_aluno_idAula_has_aluno" INTEGER NOT NULL ,
  PRIMARY KEY ("id_entrada_saida"),FOREIGN KEY ("aula_has_aluno_idAula_has_aluno") REFERENCES "aula_has_aluno" ( "id_aula_has_aluno" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
CREATE INDEX "entrada_saida_fk_entradaSaida_aula_has_aluno1_idx" ON "entrada_saida" ("aula_has_aluno_idAula_has_aluno");

