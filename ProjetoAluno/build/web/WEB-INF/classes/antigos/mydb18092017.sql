-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 18-Set-2017 às 15:52
-- Versão do servidor: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydb`
--

--
-- Extraindo dados da tabela `curso`
--

INSERT INTO `curso` (`idcurso`, `nome`) VALUES
(1, 'Engenharia da Computacao');

--
-- Extraindo dados da tabela `disciplina`
--

INSERT INTO `disciplina` (`iddisciplina`, `codigoDisciplina`, `nomeDisciplina`, `cargaHoraria`) VALUES
(1, 'EXA805', 'ALGORITMOS E PROGRAMACAO II', 30),
(2, 'EXA806', 'ESTRUTURA DE DADOS', 30),
(3, 'EXA801', 'ALGORITMOS E PROGRAMACAO I', 60);

--
-- Extraindo dados da tabela `professor`
--

INSERT INTO `professor` (`idprofessor`, `nome`, `matricula`) VALUES
(1, 'JOAO B. ROCHA JR', '714189148'),
(2, 'CLAUDIA PINTO PEREIRA', '714442392');

--
-- Extraindo dados da tabela `sala`
--

INSERT INTO `sala` (`idsala`, `latitude`, `longitude`, `area`, `localizacaoTextual`, `numJanelas`, `numProjetor`, `numQuadro`, `numArCondicionado`, `numVentilador`, `numPortas`, `numLampadas`) VALUES
(1, 121160, 385823, 0, 'PAT 51', 10, 1, 1, 0, 1, 1, 6),
(2, 121207, 385808, 0, 'PAV 02', 2, 1, 1, 0, 1, 1, 6),
(3, 0, 0, 0, 'PAV 11', 0, 1, 1, 0, 1, 1, 6),
(4, 0, 0, 0, 'PAT 32', 0, 1, 1, 0, 1, 1, 6),
(5, 0, 0, 0, 'PAT 33', 0, 1, 1, 0, 1, 1, 6);

--
-- Extraindo dados da tabela `semestre`
--

INSERT INTO `semestre` (`idSemestre`, `nome`, `dataInicio`, `dataFim`) VALUES
(1, '2017.2', '2017-09-12', '2018-02-09');

--
-- Extraindo dados da tabela `aluno`
--

INSERT INTO `aluno` (`idaluno`, `macId`, `nome`, `endereco`, `dataNascimento`, `matricula`, `curso_idcurso`, `semestre_idtable1`) VALUES
(1, '0', 'ADRIEL SOARES DE BRITO', '0', '2017-09-01', '17111188', 1, 1),
(2, '0', 'ALESSANDRO COSTA DA SILVA CARNEIRO', '0', '2017-09-01', '15111152', 1, 1),
(3, '0', 'ALMIR MOREIRA DA SILVA NETO', '0', '2017-09-01', '17111207', 1, 1),
(4, '0', 'ALYSON ALVARES BRITO MORAIS', '0', '2017-09-01', '14211146', 1, 1),
(5, '0', 'ANGELO RAFAEL MOTA GOMES', '0', '2017-09-01', '11211149', 1, 1),
(6, '0', 'ARTHUR HENRIQUE MOTA BRITO SANTOS', '0', '2017-09-01', '17111183', 1, 1),
(7, '0', 'ARTUR CAVALCANTE OLIVEIRA', '0', '2017-09-01', '17111196', 1, 1),
(8, '0', 'AURELIO ROCHA BARRETO', '0', '2017-09-01', '16111164', 1, 1),
(9, '0', 'CARLOS ANDRE PEREIRA TININ', '0', '2017-09-01', '17111195', 1, 1),
(10, '0', 'HILDEBRANDO SIMOES DE ARAUJO NETO', '0', '2017-09-01', '17111187', 1, 1),
(11, '0', 'IAGO MACHADO DA CONCEICAO SILVA', '0', '2017-09-01', '17111174', 1, 1),
(12, '0', 'JADER JOSE CERQUEIRA DO NASCIMENTO', '0', '2017-09-01', '15211163', 1, 1),
(13, '0', 'JOSE WILKER PEREIRA DOS SANTOS', '0', '2017-09-01', '15211186', 1, 1),
(14, 'c4:9a:02:05:1d:1b', 'LAERCIO DE SOUZA RIOS', '1 Trv H, n 9 ap 4, Conj Feira VI, Campo Limpo', '1999-08-18', '17111175', 1, 1),
(15, '0', 'LARISSA SILVA SAMPAIO', '0', '2017-09-01', '14111161', 1, 1),
(16, '0', 'LEONARDO MASCARENHAS GUSMAO', '0', '2017-09-01', '15211173', 1, 1),
(17, '0', 'LUCIANO ARAUJO DOURADO FILHO', 'Rua Piracicaba, n 18, Quadra B', '1998-10-03', '17111203', 1, 1),
(18, '0', 'MATEUS GUIMARAES DOS SANTOS LIMA', '0', '2017-09-01', '17111186', 1, 1),
(19, '0', 'MATHEUS TELES DE OLIVEIRA', '0', '2017-09-01', '17111201', 1, 1),
(20, '1c:56:fe:39:12:10', 'NATALIA SILVA ROSA', 'Rua Targino, n 41, Lot Monte Pascoal, Calumbi', '2017-09-01', '17111191', 1, 1),
(21, '0', 'RAFAEL DOS REIS AZEVEDO', '0', '2017-09-01', '17111202', 1, 1),
(22, '0', 'THIAGO DA CUNHA MENEZES SOUZA', '0', '2017-09-01', '15211182', 1, 1),
(23, '0', 'VICTOR LOPES MATOS SILVA', '0', '2017-09-01', '17111213', 1, 1),
(24, '0', 'VYCTOR ASSUNCAO ALVES', '0', '2017-09-01', '15211187', 1, 1),
(25, '0', 'ALEXANDRE RIBEIRO CARNEIRO', '0', '2017-09-01', '16111199', 1, 1),
(26, '0', 'ANANIAS CORREIA DO NASCIMENTO', '0', '2017-09-01', '16111187', 1, 1),
(27, '0', 'ARTHUR LESSA LEITE', '0', '2017-09-01', '15111171', 1, 1),
(28, '0', 'BRENDO NASCIMENTO DOS SANTOS SOUSA', '0', '2017-09-01', '16111181', 1, 1),
(29, '0', 'BRUNO CLAUDINO MATIAS', '0', '2017-09-01', '16111195', 1, 1),
(30, '0', 'GABRIEL DA CRUZ SILVA', 'o', '2017-09-01', '16111177', 1, 1),
(31, '0', 'GABRIEL SILVA DE AZEVEDO', '0', '2017-09-01', '16111167', 1, 1),
(32, '40:88:05:e9:d9:96', 'ITAMAR MORGADO DA SILVA', 'Rua A, n 45, Conjunto Feira 6', '1994-09-06', '13211175', 1, 1),
(33, '0', 'JAEVILLEN FERREIRA DE OLIVEIRA', '0', '2017-09-01', '17111176', 1, 1),
(34, '0', 'LUAN VICTOR TEIXEIRA DE BRITO', '0', '2017-09-01', '15111173', 1, 1),
(35, '0', 'LUCAS DOS SANTOS ROCHA', '0', '2017-09-01', '16111202', 1, 1),
(36, '0', 'LUCAS MACHADO LEVI', '0', '2017-09-01', '17111199', 1, 1),
(37, '0', 'MATHEUS ALMEIDA LIMA', '0', '2017-09-01', '13111178', 1, 1),
(38, '0', 'ROBSON JONES DE LIMA', '0', '2017-09-01', '13211189', 1, 1),
(39, '0', 'ROGERIO GABRIEL RODRIGUES SOUZA', '0', '2017-09-01', '15111182', 1, 1),
(40, '0', 'VAGNER JOSE DOS SANTOS', '0', '2017-09-01', '14211180', 1, 1),
(41, '0', 'VANESSA NOGUEIRA LOPES MAIA', '0', '2017-09-01', '16111201', 1, 1),
(42, '0', 'VINICIUS DIAS DE JESUS MACIEL', '0', '2017-09-01', '16111163', 1, 1),
(43, '0', 'WERISSON NASCIMENTO MOTA', '0', '2017-09-01', '14211184', 1, 1),
(44, '0', 'ABEL RAMALHO GALVAO', '0', '2017-09-01', '16111169', 1, 1),
(45, '0', 'JOHNNY DA SILVA SANTANA', '0', '2017-09-01', '15211168', 1, 1),
(46, '0', 'LUCAS SILVA LIMA', '0', '2017-09-01', '16111165', 1, 1),
(47, '0', 'MARCELO PINTO NETO', '0', '2017-09-01', '14211182', 1, 1),
(48, '0', 'PEDRO HENRIQUE BRANDAO OLIVEIRA BASTOS', '0', '2017-09-01', '13211179', 1, 1),
(49, '0', 'ANDERSON DA LUZ CORREIA', '0', '2017-09-01', '17211155', 1, 1),
(50, '0', 'ANESIO SOUSA DOS SANTOS NETO', '0', '2017-09-01', '17111205', 1, 1),
(51, '0', 'ARIANA DE JESUS SANTOS PEREIRA', '0', '2017-09-01', '17211160', 1, 1),
(52, '0', 'CLERISTON OLIVEIRA BESSA', '0', '2017-09-01', '17211151', 1, 1),
(53, '0', 'ED CARLOS MENDES SOARES', '0', '2017-09-01', '17111182', 1, 1),
(54, '0', 'ESTHER DE SANTANA ARAUJO', '0', '2017-09-01', '17211159', 1, 1),
(55, '0', 'FABIO COSME FREITAS ALMEIDA', '0', '2017-09-01', '17211156', 1, 1),
(56, '0', 'JOAO LUCAS DE MAGALHAES LEAL MOREIRA', '0', '2017-09-01', '17111185', 1, 1),
(57, '0', 'JOAO PEDRO BORGES GOMES', '0', '2017-09-01', '17211158', 1, 1),
(58, '0', 'JOAO PEDRO COSTA FERREIRA', '0', '2017-09-01', '17111178', 1, 1),
(59, '0', 'JORGE MOREIRA DA COSTA JUNIOR', '0', '2017-09-01', '17211152', 1, 1),
(60, '0', 'JULIANA ARAGAO PINTO', '0', '2017-09-01', '17211162', 1, 1),
(61, '0', 'MARIANA DA SILVA LIMA SANTOS', '0', '2017-09-01', '17211157', 1, 1),
(62, '0', 'MATEUS DE ALMEIDA MIRANDA', '0', '2017-09-01', '17211154', 1, 1),
(63, '0', 'RAMON DE CERQUEIRA SILVA', '0', '2017-09-01', '17111210', 1, 1),
(64, '0', 'ROGERIO DOS SANTOS CERQUEIRA', '0', '2017-09-01', '16111188', 1, 1),
(65, '0', 'ROMARIO DOS ANJOS NASCIMENTO', '0', '2017-09-01', '17211153', 1, 1),
(66, '0', 'SERGIO PUGLIESI DE MATOS', '0', '2017-09-01', '17211161', 1, 1),
(67, '0', 'TIAGO DE FIGUEIREDO MOURA', '0', '2017-09-01', '17111197', 1, 1),
(68, '0', 'VICTOR CESAR DA ROCHA BASTOS', '0', '2017-09-01', '17111211', 1, 1);


--
-- Extraindo dados da tabela `turma`
--

INSERT INTO `turma` (`idturma`, `codigoTurma`, `horaInicio`, `minutoInicio`, `horaFim`, `minutoFim`, `disciplina_iddisciplina`, `professor_idprofessor`, `semestre_idtable1`) VALUES
(1, 'EXA805-T01', 15, 30, 17, 30, 1, 1, 1),
(2, 'EXA806-T01', 13, 30, 15, 30, 2, 1, 1),
(3, 'EXA806-T02', 15, 30, 17, 30, 2, 1, 1),
(4, 'EXA801-T02', 13, 30, 15, 30, 3, 2, 1);

--
-- Extraindo dados da tabela `aluno_has_turma`
--

INSERT INTO `aluno_has_turma` (`id`, `aluno_idaluno`, `turma_idturma`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 1),
(14, 14, 1),
(15, 15, 1),
(16, 16, 1),
(17, 17, 1),
(18, 18, 1),
(19, 19, 1),
(20, 20, 1),
(21, 21, 1),
(22, 22, 1),
(23, 23, 1),
(24, 24, 1),
(25, 2, 2),
(28, 25, 2),
(29, 3, 2),
(30, 26, 2),
(31, 27, 2),
(32, 7, 2),
(33, 28, 2),
(34, 29, 2),
(35, 30, 2),
(36, 31, 2),
(37, 32, 2),
(38, 12, 2),
(39, 33, 2),
(40, 13, 2),
(41, 34, 2),
(42, 35, 2),
(43, 36, 2),
(44, 37, 2),
(45, 38, 2),
(46, 39, 2),
(47, 40, 2),
(48, 41, 2),
(49, 42, 2),
(50, 43, 2),
(51, 44, 3),
(52, 1, 3),
(53, 4, 3),
(54, 5, 3),
(55, 6, 3),
(56, 8, 3),
(57, 9, 3),
(58, 10, 3),
(59, 11, 3),
(60, 45, 3),
(61, 14, 3),
(62, 15, 3),
(63, 16, 3),
(64, 46, 3),
(65, 17, 3),
(66, 47, 3),
(67, 18, 3),
(68, 19, 3),
(69, 20, 3),
(70, 48, 3),
(71, 21, 3),
(72, 22, 3),
(73, 23, 3),
(74, 24, 3);





--
-- Extraindo dados da tabela `turma_has_sala`
--

INSERT INTO `turma_has_sala` (`id`, `turma_idturma`, `sala_idsala`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 4, 5);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
