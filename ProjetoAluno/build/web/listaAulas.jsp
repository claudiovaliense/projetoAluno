<%-- 
    Document   : listaAulas
    Created on : 07/11/2017, 09:31:56
    Author     : claudiomoises
--%>

<%@page import="database.Aula"%>
<%@page import="database.Aluno"%>
<%@page import="database.InformacoesAluno"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    //Verifica se está logado
    if (!(session.getAttribute("login") != null
            && session.getAttribute("senha") != null)) {
        out.println("<script>alert('Não está logado!'); document.location=('/ProjetoAluno'); </script>");
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
        <title>Lista aulas</title>
    </head>
    <body>
        <h1>Aula</h1>

        <table>
            <th>ID</th>            
            <th>Nome aula</th>
            <th>Chegada professor</th>
            <th>Saída professor</th>
            <th>Início aula</th>
            <th>Fim aula</th>
            <th>Código Turma</th>
            <th>Nome professor</th>
            <th>Número janelas abertas</th>
            <th>Número ventiladores Ligados</th>
            <th>Número ar-condicionado ligado</th>
            <th>Temperatura</th>
            <th>Barulho</th>
            <th>Luminosidade</th>
            <th>Número lampadas ligadas</th>
            <th>Usou projetor</th>
            <th>Usou quadro</th>
            <th>Sala</th>
            <!-- <th></th> !-->       

            <%
                List<Aula> listInformacoesAula = (List) request.getAttribute("listInformacoesAula");
                for (Aula informacoesAula : listInformacoesAula) {
                    out.print("<tr>");
                    out.println("<td>" + informacoesAula.getIdaula() + "</td>");
                    out.println("<td>" + informacoesAula.getNome() + "</td>");                    
                    out.println("<td>" + informacoesAula.getChegadaProfessor() + "</td>");
                    out.println("<td>" + informacoesAula.getSaidaProfessor() + "</td>");
                    out.println("<td>" + informacoesAula.getInicioAula() + "</td>");
                    out.println("<td>" + informacoesAula.getFimAula() + "</td>");
                    out.println("<td>" + informacoesAula.getTurma().getCodigoTurma() + "</td>");
                    out.println("<td>" + informacoesAula.getProfessor().getNome() + "</td>");
                    out.println("<td>" + informacoesAula.getNumJanelasAbertas() + "</td>");
                    out.println("<td>" + informacoesAula.getNumVentiladorLigados() + "</td>");
                    out.println("<td>" + informacoesAula.getNumArCondicionadoLigado() + "</td>");
                    out.println("<td>" + informacoesAula.getTemperatura() + "</td>");
                    out.println("<td>" + informacoesAula.getBarulho() + "</td>");
                    out.println("<td>" + informacoesAula.getLuminosidade() + "</td>");
                    out.println("<td>" + informacoesAula.getNumLampadasLigadas() + "</td>");
                    out.println("<td>" + informacoesAula.getUsouProjetor() + "</td>");
                    out.println("<td>" + informacoesAula.getUsouQuadro() + "</td>");
                    out.println("<td>" + informacoesAula.getSala().getLocalizacaoTextual() + "</td>");
                    out.println("</tr>");
                }
            %>  

        </table>
        <br/>
        <a href="/ProjetoAluno/menu.jsp"> <input type="button" value="Voltar" />  </a> 
    </body>
</html>