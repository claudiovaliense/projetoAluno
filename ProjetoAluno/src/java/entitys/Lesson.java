package entitys;

import java.io.Serializable;
import java.util.Date;

public class Lesson implements Serializable {

    private Integer id;
    private Integer idClass;
    private Integer idProfessor;
    private Integer idRoom;
    private String name;
    private Date initTime;
    private Date endTime;
    private Date professorInTime;
    private Date professorOutTime;

    public Lesson() {
    }

    public Lesson(Integer id, Integer idClass, Integer idProfessor, Integer idRoom, String name, Date initTime, Date endTime, Date professorInTime, Date professorOutTime) {
        this.id = id;
        this.idClass = idClass;
        this.idProfessor = idProfessor;
        this.idRoom = idRoom;
        this.name = name;
        this.initTime = initTime;
        this.endTime = endTime;
        this.professorInTime = professorInTime;
        this.professorOutTime = professorOutTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdClass() {
        return idClass;
    }

    public void setIdClass(Integer idClass) {
        this.idClass = idClass;
    }

    public Integer getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(Integer idProfessor) {
        this.idProfessor = idProfessor;
    }

    public Integer getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(Integer idRoom) {
        this.idRoom = idRoom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getInitTime() {
        return initTime;
    }

    public void setInitTime(Date initTime) {
        this.initTime = initTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getProfessorInTime() {
        return professorInTime;
    }

    public void setProfessorInTime(Date professorInTime) {
        this.professorInTime = professorInTime;
    }

    public Date getProfessorOutTime() {
        return professorOutTime;
    }

    public void setProfessorOutTime(Date professorOutTime) {
        this.professorOutTime = professorOutTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lesson course = (Lesson) o;

        return id != null && id.equals(course.id);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return name;
    }
}
