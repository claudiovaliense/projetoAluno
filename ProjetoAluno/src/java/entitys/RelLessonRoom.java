package entitys;

import java.io.Serializable;

public class RelLessonRoom implements Serializable {

    private Integer idLesson;
    private Integer idRoom;
    private Integer windows;
    private Integer doors;
    private Integer fans;
    private Integer lamps;
    private Integer luminosity;
    private Integer temperature;
    private Integer noise;
    private Boolean board;
    private Boolean projector;
    private Boolean airconditioner;

    public RelLessonRoom() {
    }

    public RelLessonRoom(Integer idLesson, Integer idRoom, Integer windows, Integer doors, Integer fans, Integer lamps, Integer luminosity, Integer temperature, Integer noise, Boolean board, Boolean projector, Boolean airconditioner) {
        this.idLesson = idLesson;
        this.idRoom = idRoom;
        this.windows = windows;
        this.doors = doors;
        this.fans = fans;
        this.lamps = lamps;
        this.luminosity = luminosity;
        this.temperature = temperature;
        this.noise = noise;
        this.board = board;
        this.projector = projector;
        this.airconditioner = airconditioner;
    }

    public Integer getIdLesson() {
        return idLesson;
    }

    public void setIdLesson(Integer idLesson) {
        this.idLesson = idLesson;
    }

    public Integer getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(Integer idRoom) {
        this.idRoom = idRoom;
    }

    public Integer getWindows() {
        return windows;
    }

    public void setWindows(Integer windows) {
        this.windows = windows;
    }

    public Integer getDoors() {
        return doors;
    }

    public void setDoors(Integer doors) {
        this.doors = doors;
    }

    public Integer getFans() {
        return fans;
    }

    public void setFans(Integer fans) {
        this.fans = fans;
    }

    public Integer getLamps() {
        return lamps;
    }

    public void setLamps(Integer lamps) {
        this.lamps = lamps;
    }

    public Integer getLuminosity() {
        return luminosity;
    }

    public void setLuminosity(Integer luminosity) {
        this.luminosity = luminosity;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public Integer getNoise() {
        return noise;
    }

    public void setNoise(Integer noise) {
        this.noise = noise;
    }

    public boolean isBoard() {
        return board;
    }

    public void setBoard(boolean board) {
        this.board = board;
    }

    public boolean isProjector() {
        return projector;
    }

    public void setProjector(boolean projector) {
        this.projector = projector;
    }

    public boolean isAirconditioner() {
        return airconditioner;
    }

    public void setAirConditioner(boolean airconditioner) {
        this.airconditioner = airconditioner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RelLessonRoom that = (RelLessonRoom) o;

        return idLesson != null && idRoom != null && idLesson.equals(that.idLesson) && idRoom.equals(that.idRoom);
    }
}
