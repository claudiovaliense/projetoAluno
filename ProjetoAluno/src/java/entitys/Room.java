package entitys;

import java.io.Serializable;

public class Room implements Serializable {

    private Integer id;
    private String name;
    private Integer windows;
    private Integer doors;
    private Integer fans;
    private Integer lamps;
    private Boolean board;
    private Boolean projector;
    private Boolean airconditioner;

    public Room() {
    }

    public Room(Integer id, String name, Integer windows, Integer doors, Integer fans, Integer lamps, Boolean board, Boolean projector, Boolean airconditioner) {
        this.id = id;
        this.name = name;
        this.windows = windows;
        this.doors = doors;
        this.fans = fans;
        this.lamps = lamps;
        this.board = board;
        this.projector = projector;
        this.airconditioner = airconditioner;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWindows() {
        return windows;
    }

    public void setWindows(Integer windows) {
        this.windows = windows;
    }

    public Integer getDoors() {
        return doors;
    }

    public void setDoors(Integer doors) {
        this.doors = doors;
    }

    public Integer getFans() {
        return fans;
    }

    public void setFans(Integer fans) {
        this.fans = fans;
    }

    public Integer getLamps() {
        return lamps;
    }

    public void setLamps(Integer lamps) {
        this.lamps = lamps;
    }

    public boolean isBoard() {
        return board;
    }

    public void setBoard(boolean board) {
        this.board = board;
    }

    public boolean isProjector() {
        return projector;
    }

    public void setProjector(boolean projector) {
        this.projector = projector;
    }

    public boolean isAirconditioner() {
        return airconditioner;
    }

    public void setAirconditioner(boolean airconditioner) {
        this.airconditioner = airconditioner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Room student = (Room) o;

        return id != null && id.equals(student.id);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return name;
    }
}
