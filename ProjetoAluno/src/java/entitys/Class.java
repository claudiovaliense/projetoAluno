package entitys;

import java.io.Serializable;

public class Class implements Serializable {

    private Integer id;
    private String codClass;
    private String codMatter;
    private String nameMatter;
    private String semester;
    private Integer idProfessor;

    public Class() {
    }

    public Class(Integer id, String codClass, String codMatter, String nameMatter, String semester, Integer idProfessor) {
        this.id = id;
        this.codClass = codClass;
        this.codMatter = codMatter;
        this.nameMatter = nameMatter;
        this.semester = semester;
        this.idProfessor = idProfessor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodClass() {
        return codClass;
    }

    public void setCodClass(String codClass) {
        this.codClass = codClass;
    }

    public String getCodMatter() {
        return codMatter;
    }

    public void setCodMatter(String codMatter) {
        this.codMatter = codMatter;
    }

    public String getNameMatter() {
        return nameMatter;
    }

    public void setNameMatter(String nameMatter) {
        this.nameMatter = nameMatter;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Integer getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(Integer idProfessor) {
        this.idProfessor = idProfessor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Class aClass = (Class) o;

        return id != null && id.equals(aClass.id);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return nameMatter + " - " + semester + " - " + codClass;
    }
}
