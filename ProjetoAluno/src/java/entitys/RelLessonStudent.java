package entitys;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class RelLessonStudent implements Serializable {

    private Integer idLesson;
    private Integer idStudent;
    private Integer participationNum;
    private String sitPlace;

    public RelLessonStudent() {
    }

    public RelLessonStudent(Integer idLesson, Integer idStudent, Integer participationNum, String sitPlace) {
        this.idLesson = idLesson;
        this.idStudent = idStudent;
        this.participationNum = participationNum;
        this.sitPlace = sitPlace;
    }

    public Integer getParticipationNum() {
        return participationNum;
    }

    public void setParticipationNum(Integer participationNum) {
        this.participationNum = participationNum;
    }

    public String getSitPlace() {
        return sitPlace;
    }

    public void setSitPlace(String sitPlace) {
        this.sitPlace = sitPlace;
    }

    public Integer getIdLesson() {
        return idLesson;
    }

    public void setIdLesson(Integer idLesson) {
        this.idLesson = idLesson;
    }

    public Integer getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Integer idStudent) {
        this.idStudent = idStudent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RelLessonStudent that = (RelLessonStudent) o;

        return idLesson != null && idStudent != null && idLesson.equals(that.idLesson) && idStudent.equals(that.idStudent);
    }
}
