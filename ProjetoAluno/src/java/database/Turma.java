package database;
// Generated 31/10/2017 12:25:13 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Turma generated by hbm2java
 */
public class Turma  implements java.io.Serializable {


     private Integer idturma;
     private Disciplina disciplina;
     private Professor professor;
     private Semestre semestre;
     private String codigoTurma;
     private Integer horaInicio;
     private Integer minutoInicio;
     private Integer horaFim;
     private Integer minutoFim;
     private Set turmaHasSalas = new HashSet(0);
     private Set aulas = new HashSet(0);
     private Set alunoHasTurmas = new HashSet(0);

    public Turma() {
    }

	
    public Turma(Disciplina disciplina, Professor professor, Semestre semestre) {
        this.disciplina = disciplina;
        this.professor = professor;
        this.semestre = semestre;
    }
    public Turma(Disciplina disciplina, Professor professor, Semestre semestre, String codigoTurma, Integer horaInicio, Integer minutoInicio, Integer horaFim, Integer minutoFim, Set turmaHasSalas, Set aulas, Set alunoHasTurmas) {
       this.disciplina = disciplina;
       this.professor = professor;
       this.semestre = semestre;
       this.codigoTurma = codigoTurma;
       this.horaInicio = horaInicio;
       this.minutoInicio = minutoInicio;
       this.horaFim = horaFim;
       this.minutoFim = minutoFim;
       this.turmaHasSalas = turmaHasSalas;
       this.aulas = aulas;
       this.alunoHasTurmas = alunoHasTurmas;
    }
   
    public Integer getIdturma() {
        return this.idturma;
    }
    
    public void setIdturma(Integer idturma) {
        this.idturma = idturma;
    }
    public Disciplina getDisciplina() {
        return this.disciplina;
    }
    
    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }
    public Professor getProfessor() {
        return this.professor;
    }
    
    public void setProfessor(Professor professor) {
        this.professor = professor;
    }
    public Semestre getSemestre() {
        return this.semestre;
    }
    
    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }
    public String getCodigoTurma() {
        return this.codigoTurma;
    }
    
    public void setCodigoTurma(String codigoTurma) {
        this.codigoTurma = codigoTurma;
    }
    public Integer getHoraInicio() {
        return this.horaInicio;
    }
    
    public void setHoraInicio(Integer horaInicio) {
        this.horaInicio = horaInicio;
    }
    public Integer getMinutoInicio() {
        return this.minutoInicio;
    }
    
    public void setMinutoInicio(Integer minutoInicio) {
        this.minutoInicio = minutoInicio;
    }
    public Integer getHoraFim() {
        return this.horaFim;
    }
    
    public void setHoraFim(Integer horaFim) {
        this.horaFim = horaFim;
    }
    public Integer getMinutoFim() {
        return this.minutoFim;
    }
    
    public void setMinutoFim(Integer minutoFim) {
        this.minutoFim = minutoFim;
    }
    public Set getTurmaHasSalas() {
        return this.turmaHasSalas;
    }
    
    public void setTurmaHasSalas(Set turmaHasSalas) {
        this.turmaHasSalas = turmaHasSalas;
    }
    public Set getAulas() {
        return this.aulas;
    }
    
    public void setAulas(Set aulas) {
        this.aulas = aulas;
    }
    public Set getAlunoHasTurmas() {
        return this.alunoHasTurmas;
    }
    
    public void setAlunoHasTurmas(Set alunoHasTurmas) {
        this.alunoHasTurmas = alunoHasTurmas;
    }




}


