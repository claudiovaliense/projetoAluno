package database;
// Generated 31/10/2017 12:25:13 by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Aluno generated by hbm2java
 */
public class Aluno  implements java.io.Serializable {


     private Integer idaluno;
     private Curso curso;
     private Semestre semestre;
     private String macId;
     private String nome;
     private String endereco;
     private Date dataNascimento;
     private String matricula;
     private String imei;
     private String imei2;
     private String imei3;
     private Set alunoHasTurmas = new HashSet(0);
     private Set aulaHasAlunos = new HashSet(0);
     private Set informacoesAlunos = new HashSet(0);

    public Aluno() {
    }

	
    public Aluno(Curso curso, Semestre semestre) {
        this.curso = curso;
        this.semestre = semestre;
    }
    public Aluno(Curso curso, Semestre semestre, String macId, String nome, String endereco, Date dataNascimento, String matricula, String imei, String imei2, String imei3, Set alunoHasTurmas, Set aulaHasAlunos, Set informacoesAlunos) {
       this.curso = curso;
       this.semestre = semestre;
       this.macId = macId;
       this.nome = nome;
       this.endereco = endereco;
       this.dataNascimento = dataNascimento;
       this.matricula = matricula;
       this.imei = imei;
       this.imei2 = imei2;
       this.imei3 = imei3;
       this.alunoHasTurmas = alunoHasTurmas;
       this.aulaHasAlunos = aulaHasAlunos;
       this.informacoesAlunos = informacoesAlunos;
    }
   
    public Integer getIdaluno() {
        return this.idaluno;
    }
    
    public void setIdaluno(Integer idaluno) {
        this.idaluno = idaluno;
    }
    public Curso getCurso() {
        return this.curso;
    }
    
    public void setCurso(Curso curso) {
        this.curso = curso;
    }
    public Semestre getSemestre() {
        return this.semestre;
    }
    
    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }
    public String getMacId() {
        return this.macId;
    }
    
    public void setMacId(String macId) {
        this.macId = macId;
    }
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getEndereco() {
        return this.endereco;
    }
    
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    public Date getDataNascimento() {
        return this.dataNascimento;
    }
    
    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
    public String getMatricula() {
        return this.matricula;
    }
    
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    public String getImei() {
        return this.imei;
    }
    
    public void setImei(String imei) {
        this.imei = imei;
    }
    public String getImei2() {
        return this.imei2;
    }
    
    public void setImei2(String imei2) {
        this.imei2 = imei2;
    }
    public String getImei3() {
        return this.imei3;
    }
    
    public void setImei3(String imei3) {
        this.imei3 = imei3;
    }
    public Set getAlunoHasTurmas() {
        return this.alunoHasTurmas;
    }
    
    public void setAlunoHasTurmas(Set alunoHasTurmas) {
        this.alunoHasTurmas = alunoHasTurmas;
    }
    public Set getAulaHasAlunos() {
        return this.aulaHasAlunos;
    }
    
    public void setAulaHasAlunos(Set aulaHasAlunos) {
        this.aulaHasAlunos = aulaHasAlunos;
    }
    public Set getInformacoesAlunos() {
        return this.informacoesAlunos;
    }
    
    public void setInformacoesAlunos(Set informacoesAlunos) {
        this.informacoesAlunos = informacoesAlunos;
    }




}


