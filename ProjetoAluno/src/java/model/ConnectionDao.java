/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 *
 * @author claudiomoises
 */
public class ConnectionDao {

    public Connection openConnection() {
        Connection conn = null;
        try { System.out.println("chegou aqui");
            Class.forName("com.mysql.jdbc.Driver");
            
            System.out.println("Conexao criada com sucesso2");
            
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "");
            System.out.println("Conexao criada com sucesso");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Nao encontrou class");
            e.printStackTrace();
        }
        return conn;
    } // fim do metodo

    /**
     * Fecha a conexao
     *
     * @param conn
     */
    public void fecharConexao(Connection conn) {
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
                System.out.println("Connexao fechada com successo");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    } // fim do metodo
}
