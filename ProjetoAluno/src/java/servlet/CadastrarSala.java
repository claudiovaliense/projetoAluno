
package servlet;

import database.Sala;
import hibernate.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Usuario
 */
public class CadastrarSala extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        
        try (PrintWriter out = response.getWriter()) {          

            Set<String> params = request.getParameterMap().keySet();

            //com array
            Object[] paramsString = params.toArray();
            System.out.println("tamanho " + paramsString[0]);

            //Cadastrar no banco
            /*Sala sala = new Sala(
                    Integer.parseInt((String) paramsString[0]),
                    Double.parseDouble((String)paramsString[1]),
                        Double.parseDouble((String)paramsString[1]),
                    paramsString[3],
                    paramsString[4],
                    paramsString[5],
                    paramsString[6],
                    paramsString[7],
                    paramsString[8],
                    paramsString[9],
                    paramsString[10],
                    paramsString[11],
                    paramsString[12]
            );*/
            Sala sala = new Sala();            
            sala.setLatitude(Double.parseDouble(request.getParameter((String) paramsString[0])));
            sala.setLongitude(Double.parseDouble(request.getParameter((String) paramsString[1])));
            sala.setArea(Double.parseDouble(request.getParameter((String) paramsString[2])));
            sala.setLocalizacaoTextual((request.getParameter((String) paramsString[3])));
            sala.setNumJanelas(Integer.parseInt(request.getParameter((String) paramsString[4])));
            sala.setNumProjetor(Integer.parseInt(request.getParameter((String) paramsString[5])));
            sala.setNumQuadro(Integer.parseInt(request.getParameter((String) paramsString[6])));
            sala.setNumArCondicionado(Integer.parseInt(request.getParameter((String) paramsString[7])));
            sala.setNumVentilador(Integer.parseInt(request.getParameter((String) paramsString[8])));
            sala.setNumPortas(Integer.parseInt(request.getParameter((String) paramsString[9])));
            sala.setNumLampadas(Integer.parseInt(request.getParameter((String) paramsString[10])));
            
            Session s = HibernateUtil.getSessionFactory().openSession();
            Transaction tr = s.beginTransaction();

            
            s.saveOrUpdate(sala);

            tr.commit();

            s.flush();
            s.close(); 
            
            
            out.println("<script>alert('Cadastro com sucesso!');document.location=('/ProjetoAluno/menu.jsp');</script>");


        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
