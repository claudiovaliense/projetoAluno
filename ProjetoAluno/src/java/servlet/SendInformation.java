/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import database.Curso;
import database.InformacoesAluno;
import hibernate.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Matheus
 */
public class SendInformation extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       // request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            Session s = HibernateUtil.getSessionFactory().openSession();

            String data = (String) request.getParameter("data");

            //out.println("Servidor: " + data);
            JSONObject sectionObj = new JSONObject(data);

            JSONArray jsonArrayInformacoes = sectionObj.getJSONArray("listaInformacoes");

            //Lista de cursos
            /* String sql = "select mac from informacoesAluno";
            SQLQuery query = s.createSQLQuery(sql);
            query.addEntity(Curso.class);
            List<Curso> listCursos = query.list();

            request.setAttribute("curso", listCursos);
             */
            for (int i = 0; i < jsonArrayInformacoes.length(); i++) {
                Transaction tr = s.beginTransaction();
                JSONObject jsonInformacao = jsonArrayInformacoes.getJSONObject(i);
                //Pega aula

                InformacoesAluno informacoesAluno = new InformacoesAluno();
                
                //String sql = "select * from aluno where imei = " +jsonInformacao.getString("IMEI");

                if (!jsonInformacao.isNull("dataHora")) {
                    informacoesAluno.setDataColeta(new Date(jsonInformacao.getLong("dataHora")));
                }

                if (!jsonInformacao.isNull("latitude")) {
                    informacoesAluno.setLatitude(jsonInformacao.getDouble("latitude"));
                }

                if (!jsonInformacao.isNull("longitude")) {
                    informacoesAluno.setLongitude(jsonInformacao.getDouble("longitude"));
                }

                if (!jsonInformacao.isNull("appAberto")) {
                    informacoesAluno.setAplicativoAberto(jsonInformacao.getString("appAberto"));
                }

                if (!jsonInformacao.isNull("estaParado")) {
                    informacoesAluno.setEstaParado((byte) jsonInformacao.getInt("estaParado"));
                }

                if (!jsonInformacao.isNull("MAC")) {
                    informacoesAluno.setMacId(jsonInformacao.getString("MAC"));
                }

                if (!jsonInformacao.isNull("luminosidade")) {
                    informacoesAluno.setLuminosidade(jsonInformacao.getDouble("luminosidade"));
                }
                
                if (!jsonInformacao.isNull("acuracia")) {
                    informacoesAluno.setPrecisaoGps(jsonInformacao.getDouble("acuracia"));
                }
                
                if (!jsonInformacao.isNull("bateria")) {
                    informacoesAluno.setCargaBateria(jsonInformacao.getInt("bateria"));
                }
                
                if (!jsonInformacao.isNull("IMEI")) {
                    informacoesAluno.setImei(jsonInformacao.getString("IMEI"));
                }
                
                

                s.saveOrUpdate(informacoesAluno);

                tr.commit();
                s.flush();
            } // fim for            
            s.close();

            out.print("sucesso");

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
