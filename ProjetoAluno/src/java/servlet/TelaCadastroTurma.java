/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import database.Disciplina;
import database.Professor;
import database.Semestre;
import hibernate.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

/**
 *
 * @author Usuario
 */
public class TelaCadastroTurma extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {      
            String sql;
            SQLQuery query;
            Session session = HibernateUtil.getSessionFactory().openSession();

             sql = "select * from disciplina";
            query = session.createSQLQuery(sql);
            query.addEntity(Disciplina.class);
            List<Disciplina> listDisciplina = query.list();

            request.setAttribute("disciplina", listDisciplina);
            
            sql = "select * from professor";
            query = session.createSQLQuery(sql);
            query.addEntity(Professor.class);
            List<Professor> listProfessor = query.list();

            request.setAttribute("professor", listProfessor);
             
            //Lista de semestres
            sql = "select * from semestre";
            query = session.createSQLQuery(sql);
            query.addEntity(Semestre.class);
            List<Semestre> listSemestre = query.list();
            //System.out.println("semestre: " +listSemestre.get(0).getNome());
             
            
            request.setAttribute("semestre", listSemestre);
session.close();
            request.getRequestDispatcher("/cadastrarTurma.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
