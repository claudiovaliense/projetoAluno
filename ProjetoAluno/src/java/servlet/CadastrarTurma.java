package servlet;

import database.Disciplina;
import database.Professor;
import database.Semestre;
import database.Turma;
import hibernate.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author claudiomoises
 */
public class CadastrarTurma extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {

            //Transformar os parametros em array
            Set<String> params = request.getParameterMap().keySet();
            Object[] paramsString = params.toArray();

            Turma turma = new Turma();
            turma.setCodigoTurma(request.getParameter((String) paramsString[0]));
            turma.setHoraInicio(Integer.parseInt(request.getParameter((String) paramsString[1])));
            turma.setMinutoInicio(Integer.parseInt(request.getParameter((String) paramsString[2])));
            turma.setHoraFim(Integer.parseInt(request.getParameter((String) paramsString[3])));
            turma.setMinutoFim(Integer.parseInt(request.getParameter((String) paramsString[4])));
            
            Disciplina disciplina = new Disciplina();
            disciplina.setIddisciplina(Integer.parseInt(request.getParameter("disciplinas")));
            turma.setDisciplina(disciplina);
            
            Professor professor = new Professor();
            professor.setIdprofessor(Integer.parseInt(request.getParameter("professors")));
            turma.setProfessor(professor);

            Semestre semestre = new Semestre();
            semestre.setIdSemestre(Integer.parseInt(request.getParameter("semestres")));
            turma.setSemestre(semestre);

            Session s = HibernateUtil.getSessionFactory().openSession();
            Transaction tr = s.beginTransaction();

            s.saveOrUpdate(turma);

            tr.commit();

            s.flush();
            s.close();

            out.println("<script>alert('Cadastro com sucesso!');document.location=('/ProjetoAluno/menu.jsp');</script>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
