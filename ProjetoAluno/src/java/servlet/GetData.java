/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import database.Aluno;
import database.AlunoHasTurma;
import database.Curso;
import database.Disciplina;
import database.Professor;
import database.Sala;
import database.Semestre;
import database.Turma;
import hibernate.HibernateUtil;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author claudiomoises
 */
public class GetData extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {

            //Conectar com servidor e criar JSON
            //Hibernate
            String sql;
            SQLQuery query;
            Session session = HibernateUtil.getSessionFactory().openSession();

            sql = "select * from aluno";
            query = session.createSQLQuery(sql);
            query.addEntity(Aluno.class);
            List<Aluno> listAluno = query.list();

            sql = "select * from professor";
            query = session.createSQLQuery(sql);
            query.addEntity(Professor.class);
            List<Professor> listProfessor = query.list();

            sql = "select * from disciplina";
            query = session.createSQLQuery(sql);
            query.addEntity(Disciplina.class);
            List<Disciplina> listDisciplina = query.list();

            sql = "select * from curso";
            query = session.createSQLQuery(sql);
            query.addEntity(Curso.class);
            List<Curso> listCurso = query.list();

            sql = "select * from semestre";
            query = session.createSQLQuery(sql);
            query.addEntity(Semestre.class);
            List<Semestre> listSemestre = query.list();

            sql = "select * from sala";
            query = session.createSQLQuery(sql);
            query.addEntity(Sala.class);
            List<Sala> listSala = query.list();

            sql = "select * from turma";
            query = session.createSQLQuery(sql);
            query.addEntity(Turma.class);
            List<Turma> listTurma = query.list();

            sql = "select * from aluno_has_turma";
            query = session.createSQLQuery(sql);
            query.addEntity(AlunoHasTurma.class);
            List<AlunoHasTurma> listAlunoHasTurma = query.list();

            //------
            //Criando o JSon para enviar ao app
            JSONObject jsonGeral = new JSONObject();

            //Adicionar todos os professores no JSON
            JSONArray professorArray = new JSONArray();
            for (Professor professor : listProfessor) {
                JSONObject professorJSON = new JSONObject();
                professorJSON.put("id", professor.getIdprofessor());
                professorJSON.put("nome", professor.getNome());
                professorJSON.put("matricula", professor.getMatricula());

                professorArray.put(professorJSON);
            }

            //Adicionar todos as disciplinas no JSON
            JSONArray disciplinaArray = new JSONArray();
            for (Disciplina disciplina : listDisciplina) {
                JSONObject disciplinaJSON = new JSONObject();
                disciplinaJSON.put("codigoDisciplina", disciplina.getCodigoDisciplina());
                disciplinaJSON.put("nome", disciplina.getNomeDisciplina());
                disciplinaJSON.put("cargaHoraria", disciplina.getCargaHoraria());

                disciplinaArray.put(disciplinaJSON);
            }

            //Adicionar todos os cursos no JSON
            JSONArray cursosArray = new JSONArray();
            for (Curso curso : listCurso) {
                JSONObject cursoJSON = new JSONObject();
                cursoJSON.put("idCurso", curso.getIdcurso());
                cursoJSON.put("nome", curso.getNome());

                cursosArray.put(cursoJSON);
            }

            //Adicionar todos as disciplinas no JSON
            JSONArray semestresArray = new JSONArray();
            for (Semestre semestre : listSemestre) {
                JSONObject semestreJSON = new JSONObject();
                semestreJSON.put("idCurso", semestre.getIdSemestre());
                semestreJSON.put("nome", semestre.getNome());

                semestresArray.put(semestreJSON);
            }

            //Adicionar todos as salas no JSON
            JSONArray salaArray = new JSONArray();
            for (Sala sala : listSala) {
                JSONObject salaJSON = new JSONObject();
                salaJSON.put("id", sala.getIdsala());
                salaJSON.put("nome", sala.getLocalizacaoTextual());
                salaJSON.put("njanelas", sala.getNumJanelas());
                salaJSON.put("nventiladores", sala.getNumVentilador());
                salaJSON.put("nlampadas", sala.getNumLampadas());
                salaJSON.put("temprojetor", sala.getNumProjetor());
                salaJSON.put("temquadro", sala.getNumQuadro());
                salaJSON.put("nportas", sala.getNumPortas());
                salaJSON.put("temarcondicionado", sala.getNumArCondicionado());

                /* salaJSON.put("latitude", sala.getLatitude());
                salaJSON.put("longitude", sala.getLongitude());
                salaJSON.put("area", sala.getArea());
                salaJSON.put("localizacaoTextual", sala.getLocalizacaoTextual());
                 */
                salaArray.put(salaJSON);
            }

            //Adicionar todos as turmas no JSON
            JSONArray turmaArray = new JSONArray();
            for (Turma turma : listTurma) {
                JSONObject turmaJSON = new JSONObject();
                turmaJSON.put("id", turma.getIdturma());
                turmaJSON.put("turma", turma.getCodigoTurma());
                turmaJSON.put("idProfessor", turma.getProfessor().getIdprofessor());
                turmaJSON.put("idSala", turma.getCodigoTurma());
                turmaJSON.put("codigoDisciplina", turma.getDisciplina().getCodigoDisciplina());
                turmaJSON.put("nomeDisciplina", turma.getDisciplina().getNomeDisciplina());
                turmaJSON.put("semestre", turma.getSemestre().getNome());

                //Adicionar todos os alunos no JSON da turma
                JSONArray alunoArray = new JSONArray();
                for (Aluno aluno : listAluno) {
                    for (AlunoHasTurma alunoHasTurma : listAlunoHasTurma) {
                        if (alunoHasTurma.getAluno().equals(aluno) && alunoHasTurma.getTurma().equals(turma)) {
                            JSONObject alunoJSON = new JSONObject();
                            alunoJSON.put("id", aluno.getIdaluno());
                            alunoJSON.put("nome", aluno.getNome());
                            alunoJSON.put("matricula", aluno.getMatricula());

                            alunoArray.put(alunoJSON);
                        }
                    }
                }
                turmaJSON.put("listaAlunos", alunoArray);

                turmaArray.put(turmaJSON);

            }

            //JSONArray salas = new JSONArray();
            //sala.put("");
            //Criar arquivo
            File file = new File("json.txt");
            System.out.println(file.getAbsolutePath());
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));

            // jsonGeral.put("listaAlunos", alunoArray);
            jsonGeral.put("listaProfessores", professorArray);
            jsonGeral.put("listaDisciplinas", disciplinaArray);
            jsonGeral.put("listaCursos", cursosArray);
            jsonGeral.put("listaSemestres", semestresArray);
            jsonGeral.put("listaSalas", salaArray);
            jsonGeral.put("listaTurmas", turmaArray);

            //escrever para quem solicitou
            out.print(jsonGeral.toString());
            //writer.write(jsonGeral.toString());
            writer.flush();// Criando o conteúdo do arquivo
            writer.close(); // Fechando conexão e escrita do arquivo.
            session.close();

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
