/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author claudiomoises
 */
@WebServlet(name = "Teste", urlPatterns = {"/Teste"})
public class Teste extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {

            Class.forName("com.mysql.jdbc.Driver");

            Connection conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "");

            /*            
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("criarBanco25.09.sql")));
            String line = br.readLine();
            String textoSQL = null;
            while (line != null) {
                line = br.readLine();
                textoSQL += line;

            }
            out.print(textoSQL);
             */
            
            //out.print(file.getAbsoluteFile());
           /* PreparedStatement stmt = conn.prepareStatement("source criarBanco25.09.sql;");
            stmt.execute();
            ResultSet rs = stmt.getResultSet();
            rs.next();
            out.print(rs.getString("nome"));
            out.print("aqui2");*/
            /* TODO output your page here. You may use following sample code. */
 /*out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Teste</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Teste at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");*/

 /*Pegar parametros*/
            for (String param : request.getParameterMap().keySet()) {
                out.print("<LI><b>Parâmetro '" + param + "'</b>="
                        + request.getParameter(param));
            }

            /*Redirecionar para o jsp*/
            //  request.getRequestDispatcher("testejsp.jsp").forward(request, response);
            /*
        System.out.println("aqui");
                    RequestDispatcher dispatcher = request.getRequestDispatcher("testejsp.jsp");
            dispatcher.forward(request, response);
             */
        } catch (SQLException ex) {
            Logger.getLogger(Teste.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Teste.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.setCharacterEncoding("UTF-8");

        //redirecionar
        // response.sendRedirect("/WebApp/HelloServlet"); 
        //response.sendRedirect("/WebApp/testejsp.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
