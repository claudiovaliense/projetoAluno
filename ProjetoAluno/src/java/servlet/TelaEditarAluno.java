/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import database.Aluno;
import database.Curso;
import database.Semestre;
import hibernate.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

/**
 *
 * @author claudiomoises
 */
public class TelaEditarAluno extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            //Lista de cursos
            String sql = "select * from curso";
            SQLQuery query = session.createSQLQuery(sql);
            query.addEntity(Curso.class);
            List<Curso> listCursos = query.list();

            request.setAttribute("curso", listCursos);
            
            
            //Lista de semestres
            sql = "select * from semestre";
            query = session.createSQLQuery(sql);
            query.addEntity(Semestre.class);
            List<Semestre> listSemestre = query.list();
                        
            request.setAttribute("semestre", listSemestre);

            sql = "select * from aluno where idaluno=" + request.getParameter("idAluno");
            query = session.createSQLQuery(sql);
            query.addEntity(Aluno.class);
            Aluno aluno = (Aluno) query.uniqueResult();

            request.setAttribute("listInformacoesAluno", aluno);
session.close();
            request.getRequestDispatcher("/editarAluno.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
