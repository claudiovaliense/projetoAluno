/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import database.Aluno;
import database.Aula;
import database.AulaHasAluno;
import database.Curso;
import database.EntradaSaida;
import database.Professor;
import database.Sala;
import database.Turma;
import entitys.InLessonTime;
import entitys.Lesson;
import entitys.RelLessonRoom;
import entitys.Student;
import hibernate.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author claudiomoises
 */
public class SendLesson extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String data = (String) request.getParameter("data");
            System.out.println("DATA -> " + data);
            degenarateJSONBD(data);
            out.print("sucesso");
        }
    }

    private void degenarateJSONBD(String data) throws JSONException {
        Session s = HibernateUtil.getSessionFactory().openSession();
        Transaction tr = s.beginTransaction();

        //JSONObject jsonObject = new JSONObject(data);
        // JSONArray sectionsArray = new JSONArray();
        JSONObject sectionObj = new JSONObject(data);

        /*Ler dados*/
        JSONArray jsonArraySecoes = sectionObj.getJSONArray("listaSecoes");

        //Receber o json para inserir no banco de dados
        //Pega secao
        for (int i = 0; i < jsonArraySecoes.length(); i++) {
            JSONObject jsonSecao = jsonArraySecoes.getJSONObject(i);
            //Pega aula
            JSONObject jsonAula = jsonSecao.getJSONObject("aula");

            Aula aula = new Aula();
            // aula.setIdaula(jsonAula.getInt("id"));
            aula.setNome(jsonAula.getString("nome"));

            if (!jsonAula.isNull("chegadaProfessor")) {
                aula.setChegadaProfessor(new Date(jsonAula.getLong("chegadaProfessor")));
            }

            if (!jsonAula.isNull("saidaProfessor")) {
                aula.setSaidaProfessor(new Date(jsonAula.getLong("saidaProfessor")));
            }

            if (!jsonAula.isNull("dataInicioAula")) {
                aula.setInicioAula(new Date(jsonAula.getLong("dataInicioAula")));
            }

            if (!jsonAula.isNull("dataFimAula")) {
                aula.setFimAula(new Date(jsonAula.getLong("dataFimAula")));
            }

            Turma turma = new Turma();
            turma.setIdturma(jsonAula.getInt("idTurma"));
            aula.setTurma(turma);

            Professor professor = new Professor();
            professor.setIdprofessor(jsonAula.getInt("idProfessor"));
            aula.setProfessor(professor);

            JSONObject jsonSala = jsonSecao.getJSONObject("sala");

            if (!jsonSala.isNull("numJanelasAbertas")) {
                aula.setNumJanelasAbertas(jsonSala.getInt("numJanelasAbertas"));
            }

            if (!jsonSala.isNull("numVentiladoresLigados")) {
                aula.setNumVentiladorLigados(jsonSala.getInt("numVentiladoresLigados"));
            }

            if (!jsonSala.isNull("usouArCondicionado")) {
                aula.setNumArCondicionadoLigado(jsonSala.getInt("usouArCondicionado"));
            }

            if (!jsonSala.isNull("temperatura")) {
                aula.setTemperatura(jsonSala.getInt("temperatura"));
            }

            if (!jsonSala.isNull("barulho")) {
                aula.setBarulho(jsonSala.getInt("barulho"));
            }

            if (!jsonSala.isNull("luminosidade")) {
                aula.setLuminosidade(jsonSala.getInt("luminosidade"));
            }

            if (!jsonSala.isNull("numLampadasLigadas")) {
                aula.setNumLampadasLigadas(jsonSala.getInt("numLampadasLigadas"));
            }

            if (!jsonSala.isNull("usouProjetor")) {
                aula.setUsouProjetor((byte) jsonSala.getInt("usouProjetor"));
            }

            if (!jsonSala.isNull("usouQuadro")) {
                aula.setUsouQuadro((byte) jsonSala.getInt("usouQuadro"));
            }

            //aula.setUsouQuadro((Byte) jsonSala.get("usouQuadro"));
            Sala sala = new Sala();
            sala.setIdsala(jsonAula.getInt("idRoom"));

            aula.setSala(sala);
            s.saveOrUpdate(aula);
            tr.commit();

            String sql = "select * from aula ORDER by idaula desc";
            SQLQuery query = s.createSQLQuery(sql);
            query.addEntity(Aula.class);
            List<Aula> listAulas = query.list();

            // System.out.println("aulaaaaaa: " +listAulas.get(0).getIdaula());
            JSONArray listaAlunos = jsonSecao.getJSONArray("listaAlunos");
            for (int j = 0; j < listaAlunos.length(); j++) {
                tr = s.beginTransaction();
                AulaHasAluno aulaHasAluno = new AulaHasAluno();

                JSONObject aluno = listaAlunos.getJSONObject(j);
                // aulaHasAluno.setIdAulaHasAluno(aluno.getInt("id"));
                aulaHasAluno.setLocalAlunoSentou(aluno.getString("localAluno"));
                aulaHasAluno.setNumParticipacoes(aluno.getInt("numParticipacoes"));
                aulaHasAluno.setDormiu((byte) aluno.getInt("dormiu"));

                sql = "select * from aluno where idaluno = " + aluno.getInt("id");
                query = s.createSQLQuery(sql);
                query.addEntity(Aluno.class);
                List<Aluno> listAluno = query.list();

                aulaHasAluno.setAluno(listAluno.get(0));
                aulaHasAluno.setAula(listAulas.get(0));

                System.out.println(aulaHasAluno.toString());

                s.saveOrUpdate(aulaHasAluno);
                tr.commit();

                JSONArray entradas = aluno.getJSONArray("listEntradaSaidas");
                for (int k = 0; k < entradas.length(); k++) {
                    tr = s.beginTransaction();

                    JSONObject entradaSaida = entradas.getJSONObject(k);
                    EntradaSaida entradasaida = new EntradaSaida();

                    entradasaida.setAulaHasAluno(aulaHasAluno);
                    entradasaida.setDataEntrada(new Date(entradaSaida.getLong("dataEntrada")));
                    entradasaida.setDataSaida(new Date(entradaSaida.getLong("dataSaida")));

                    s.saveOrUpdate(entradasaida);
                    tr.commit();

                }

            }

            System.out.println("Aula" + aula.toString());
            
            s.flush();

            /*Lesson lessonDao = new Lesson(
                    jsonAula.getInt("id"),
                    jsonAula.getInt("idTurma"),
                    jsonAula.getInt("idProfessor"),
                    jsonAula.getInt("idSala"),
                    jsonAula.getString("nome"),
                    new Date(jsonAula.getLong("dataInicioAula")),
                    new Date(jsonAula.getLong("dataFimAula")),
                    new Date(jsonAula.getLong("chegadaProfessor")),
                    new Date(jsonAula.getLong("saidaProfessor"))
            );

            //sala
            //JSONObject jsonSala = jsonSecao.getJSONObject("sala");
            RelLessonRoom relLessonRoom = new RelLessonRoom(
                    jsonAula.getInt("id"),
                    jsonSala.getInt("id"),
                    jsonSala.getInt("numJanelasAbertas"),
                    jsonSala.getInt("numPortasAbertas"),
                    jsonSala.getInt("numVentiladoresLigados"),
                    jsonSala.getInt("numLampadasLigadas"),
                    jsonSala.getInt("luminosidade"),
                    jsonSala.getInt("temperatura"),
                    jsonSala.getInt("barulho"),
                    jsonSala.getBoolean("usouQuadro"),
                    jsonSala.getBoolean("usouProjetor"),
                    jsonSala.getBoolean("usouArCondicionado")
            );*/
 /*
            //alunos
            JSONArray jsonArrayAlunos = jsonSecao.getJSONArray("listaAlunos");
            for (int j = 0; j < jsonArrayAlunos.length(); j++) {

                JSONObject jsonAluno = jsonArrayAlunos.getJSONObject(j);

                Student studentDao = new Student(
                        jsonAluno.getInt("id"),
                        jsonAluno.getString("nome"),
                        jsonAluno.getString("matricula")
                );

                JSONArray jsonArrayEntradaSaidas = jsonAluno.getJSONArray("listaEntradaSaidas");

                for (int k = 0; k < jsonArrayEntradaSaidas.length(); k++) {
                    JSONObject jsonAlunoEntradaSaida = jsonArrayEntradaSaidas.getJSONObject(j);

                    InLessonTime inLessonTimeDao = new InLessonTime(
                            jsonAlunoEntradaSaida.getInt("id"),
                            jsonAula.getInt("id"),
                            jsonAluno.getInt("id"),
                            new Date(jsonAlunoEntradaSaida.getInt("id")),
                            new Date(jsonAlunoEntradaSaida.getInt("id"))
                    );
                } // fim for

            } // fim for
             */
        } // fim for
        s.close();

        //return jsonObject.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
