<%-- 
    Document   : cadastrarAlunoTurma
    Created on : 12/09/2017, 12:00:25
    Author     : claudiomoises
--%>

<%@page import="database.Turma"%>
<%@page import="java.util.List"%>
<%@page import="database.Aluno"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    //Verifica se está logado
    if (!(session.getAttribute("login") != null
            && session.getAttribute("senha") != null)) {
        out.println("<script>alert('Não está logado!'); document.location=('/ProjetoAluno'); </script>");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro Aluno/Turma</title>
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
        <div id="campos">
            <form action="CadastrarAlunoTurma" method="POST" >
                <h1>Cadastro Aluno/Turma</h1>
                <label>Aluno:</label>
                <select name="alunos">
                    <%
                        List<Aluno> listAlunos = (List) request.getAttribute("alunos");
                        for (Aluno teste : listAlunos) {
                            out.println("<option value='" + teste.getIdaluno() + "'>" + teste.getNome() + "</option>");
                        }
                    %>                      
                </select>
                <label>Turma:</label>
                <select name="turmas">
                    <%
                        List<Turma> listTurmas = (List) request.getAttribute("turmas");
                        for (Turma teste : listTurmas) {
                            out.println("<option value='" + teste.getIdturma() + "'>" + teste.getCodigoTurma() + "</option>");
                        }
                    %>                      
                </select>
                <br/><br/><br/>
                <input type="submit" value="Cadastrar" />
            </form>
        </div>
        <br/><br/><br/>
        <a href="/ProjetoAluno/menu.jsp"> <input type="button" value="Voltar" />  </a> 

    </body>
</html>
