<%-- 
    Document   : cadastrarTurma
    Created on : 05/09/2017, 14:01:45
    Author     : claudiomoises
--%>

<%@page import="database.Semestre"%>
<%@page import="database.Professor"%>
<%@page import="java.util.List"%>
<%@page import="database.Disciplina"%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%   
    //Verifica se está logado
    if (!(session.getAttribute("login") != null 
            && session.getAttribute("senha")  != null )) {
        out.println("<script>alert('Não está logado!'); document.location=('/ProjetoAluno'); </script>");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastrar Turma</title>
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
        <div id="campos">
            <form action="CadastrarTurma" method="POST" >                                
                <h1>Cadastro Turma</h1>
                <label>Código:</label>
                <input type="text" name="nome" />
                <label>Hora início:</label>
                <input type="number" name="horaInico" />
                <label>Minuto início:</label>
                <input type="number" name="minutoInicio" />
                <label>Hora fim:</label>
                <input type="number" name="minutoInicio" />                  
                <label>Minuto fim:</label>
                <input type="number" name="minutoFim" />  
                 <label>Disciplina:</label>
                <select name="disciplinas">
                    <%
                       List<Disciplina> listDisciplina = (List) request.getAttribute("disciplina");
                        for (Disciplina teste : listDisciplina) {
                            out.println("<option value='" +teste.getIddisciplina()+"'>" + teste.getNomeDisciplina()+ "</option>");
                        }
                    %>                      
                </select>
                <label>Professor:</label>
                 <select name="professors">
                    <%
                        List<Professor> listProfessor = (List) request.getAttribute("professor");
                        for (Professor teste : listProfessor) {
                            out.println("<option value='" +teste.getIdprofessor()+"'>" + teste.getNome()+ "</option>");
                        }
                    %>                      
                </select>
                <label>Semestre:</label>
                <select name="semestres">
                    <% 
                        List<Semestre> listSemestre2 = (List) request.getAttribute("semestre");
                        for (Semestre teste : listSemestre2) {
                            out.println("<option value='" +teste.getIdSemestre()+"'>" + teste.getNome() + "</option>");
                        }
                    %>                      
                </select>

                <br/><br/><br/>
                <input type="submit" value="Cadastrar" />
            </form>
        </div>
        <br/><br/><br/>
        <a href="/ProjetoAluno/menu.jsp"> <input type="button" value="Voltar" />  </a> 
    </body>
</html>
