<%-- 
    Document   : cadastrarSala
    Created on : 30/08/2017, 21:26:12
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%   
    //Verifica se está logado
    if (!(session.getAttribute("login") != null 
            && session.getAttribute("senha")  != null )) {
        out.println("<script>alert('Não está logado!'); document.location=('/ProjetoAluno'); </script>");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastrar sala</title>
        <link rel="stylesheet" type="text/css" href="css/style.css"/>


    </head>
    <body>
        <div id="campos">
            <form action="CadastrarSala" method="POST" >
                <h1>Cadastro de Sala</h1>
                <label>Latitude:</label>
                <input type="text" name="latitude" required="true" />
                <label>Longitude:</label>
                <input type="text" name="longitude" required="true" />
                <label>Area:</label>
                <input type="text" name="area" required="true" />
                <label>Localização textual:</label>
                <input type="text" name="localizacao" required="true" />
                <label>Número de janelas:</label>
                <input type="text" name="numJanelas" required="true" />
                <label>Números de projetores:</label>
                <input type="text" name="numProjetores" required="true" />
                <label>Número de quadros:</label>
                <input type="text" name="numQuadros" required="true" />
                <label>Número de ar-condicionados:</label>
                <input type="text" name="numAr" required="true" />
                <label>Número ventiladores:</label>
                <input type="text" name="numVentiladores" required="true" />
                <label>Número portas:</label>
                <input type="text" name="numPortas" required="true" />
                <label>Número lampadas:</label>
                <input type="text" name="numLampadas" required="true" />                                
                <br/><br/>              
                <input type="submit" value="Cadastrar" />
            </form>
        </div>
        <br/><br/><br/>
        <a href="/ProjetoAluno/menu.jsp"> <input type="button" value="Voltar" />  </a> 
    </body>
</html>
