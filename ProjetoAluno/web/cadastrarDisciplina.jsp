<%-- 
    Document   : cadastrarDisciplina
    Created on : 05/09/2017, 14:01:13
    Author     : claudiomoises
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%   
    //Verifica se está logado
    if (!(session.getAttribute("login") != null 
            && session.getAttribute("senha")  != null )) {
        out.println("<script>alert('Não está logado!'); document.location=('/ProjetoAluno'); </script>");
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastrar Disciplina</title>
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
        <div id="campos">
            <form action="CadastrarDisciplina" method="POST" >
                <h1>Cadastro Disciplina</h1>
                <label>Código:</label>
                <input type="text" name="codigo" />
                <label>Nome:</label>
                <input type="text" name="nome" />
                <label>Carga horária:</label>
                <input type="number" name="cargaHoraria" />

                <br/><br/><br/>
                <input type="submit" value="Cadastrar" />
            </form>
        </div>
        <br/><br/><br/>
       <a href="/ProjetoAluno/menu.jsp"> <input type="button" value="Voltar" />  </a> 
    </body>
</html>
