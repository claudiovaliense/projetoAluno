<%-- 
    Document   : listaAlunos
    Created on : 17/10/2017, 15:00:28
    Author     : claudiomoises
--%>

<%@page import="database.Aluno"%>
<%@page import="database.InformacoesAluno"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    //Verifica se está logado
    if (!(session.getAttribute("login") != null
            && session.getAttribute("senha") != null)) {
        out.println("<script>alert('Não está logado!'); document.location=('/ProjetoAluno'); </script>");
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
        <title>Lista alunos</title>
    </head>
    <body>
        <h1>Aluno:</h1>

        <table>
            <th>ID</th>
            <th>MACID</th>
            <th>Nome</th>
            <th>Endereço</th>
            <th>Data nascimento</th>
            <th>Matricula</th>
            <th>Curso</th>
            <th>Semestre</th>
            <th>IMEI</th>
            <th>IMEI2</th>
            <th>IMEI3</th>
            <!-- <th></th> !-->       

            <%
                List<Aluno> listInformacoesAluno = (List) request.getAttribute("listInformacoesAluno");
                for (Aluno informacoesAluno : listInformacoesAluno) {
                    out.print("<tr onclick=\"location.href ='/ProjetoAluno/TelaEditarAluno?idAluno=" + informacoesAluno.getIdaluno() + "' \" style='cursor: pointer;'>");
                    //out.println("<tr>");
                    out.println("<td>" + informacoesAluno.getIdaluno() + "</td>");
                    out.println("<td>" + informacoesAluno.getMacId() + "</td>");
                    out.println("<td>" + informacoesAluno.getNome() + "</td>");
                    out.println("<td>" + informacoesAluno.getEndereco() + "</td>");
                    out.println("<td>" + informacoesAluno.getDataNascimento() + "</td>");
                    out.println("<td>" + informacoesAluno.getMatricula() + "</td>");
                    out.println("<td>" + informacoesAluno.getCurso().getNome() + "</td>");
                    out.println("<td>" + informacoesAluno.getSemestre().getNome() + "</td>");
                    out.println("<td>" + informacoesAluno.getImei() + "</td>");
                    out.println("<td>" + informacoesAluno.getImei2() + "</td>");
                    out.println("<td>" + informacoesAluno.getImei3() + "</td>");
                    //  out.println("<td><input type='button' onclick=\"location.href ='/ProjetoAluno/EditarAluno'\" value='Atualizar' /></td>");
                    out.println("</tr>");

                    ///out.println("<option value='" +teste.getIddisciplina()+"'>" + teste.getNomeDisciplina()+ "</option>");
                }
            %>  

        </table>
        <br/>
        <a href="/ProjetoAluno/menu.jsp"> <input type="button" value="Voltar" />  </a> 
    </body>
</html>
