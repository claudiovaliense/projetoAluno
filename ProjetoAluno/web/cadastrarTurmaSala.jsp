<%-- 
    Document   : cadastrarTurmaSala
    Created on : 12/09/2017, 12:27:40
    Author     : claudiomoises
--%>


<%@page import="database.Sala"%>
<%@page import="database.Turma"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%   
    //Verifica se está logado
    if (!(session.getAttribute("login") != null 
            && session.getAttribute("senha")  != null )) {
        out.println("<script>alert('Não está logado!'); document.location=('/ProjetoAluno'); </script>");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro Aluno/Turma</title>
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
        <div id="campos">
            <form action="CadastrarTurmaSala" method="POST" >
                <h1>Cadastro Turma/Sala</h1>
                <label>Turma:</label>
                <select name="turmas">
                    <%
                        List<Turma> listTurma= (List) request.getAttribute("turmas");
                        for (Turma teste : listTurma) {
                            out.println("<option value='" + teste.getIdturma()+ "'>" + teste.getCodigoTurma()+ "</option>");
                        }
                    %>                      
                </select>
                <label>Sala:</label>
                <select name="salas">
                    <%
                        List<Sala> listSala = (List) request.getAttribute("salas");
                        for (Sala teste : listSala) {
                            out.println("<option value='" + teste.getIdsala()+ "'>" + teste.getLocalizacaoTextual()+ "</option>");
                        }
                    %>                      
                </select>
                <br/><br/><br/>
                <input type="submit" value="Cadastrar" />
            </form>
        </div>
        <br/><br/><br/>
        <a href="/ProjetoAluno/menu.jsp"> <input type="button" value="Voltar" />  </a> 

    </body>
</html>
