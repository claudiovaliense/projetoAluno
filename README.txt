Configuração do git
https://tableless.com.br/tudo-que-voce-queria-saber-sobre-git-e-github-mas-tinha-vergonha-de-perguntar/

1. Baixe o projeto
2. Faça importação pelo netbeans
3. Instale o mysql pelo xampp e rode o scrpit criarBanco.sql através da importação do arquivo pelo phpmyadmin(pode executar o arquivo de outras maneiras também)
4. Teste pelo endereço http://localhost:8080/ProjetoAluno/

Comando git

// Baixar o projeto
git clone urlProjeto

// Criar branch para o usuario
git checkout -b nomeUsuario

// Enviar novo usuario para a web
git push --set-upstream origin nomeUsuario

// Atualizar projeto no branch padrão
git pull

// Adicionar todos os arquivos para enviar para web
git add .

// Realizar commit com comentário
git commit -m "comentarioCommit"

// Enviar arquivos
git push

---------------- Comandos mais especificos

// Atualizar com os arquivos de um determinado branch
git pull origin nomebranch

// Trocar para o branch
git checkout nomeBranch

// Deletar branch
git branch -D nomeBranch

// Mudar para um determinado branch
git branch origin nomeBranch

// Verificar o status do projetos, ver quantos submissões ou arquivos a serem adicionados
git status

// Verificar qual branch está
git branch

