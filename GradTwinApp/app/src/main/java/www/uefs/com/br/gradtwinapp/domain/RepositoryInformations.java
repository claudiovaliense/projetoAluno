package www.uefs.com.br.gradtwinapp.domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;

import www.uefs.com.br.gradtwinapp.database.DataBase;
import www.uefs.com.br.gradtwinapp.domain.entitys.Informations;

public class RepositoryInformations {
    private SQLiteDatabase dataBaseConnection;

    public RepositoryInformations(Context context) {
        DataBase dataBase = new DataBase(context);
        this.dataBaseConnection = dataBase.getWritableDatabase();
    }

    public void insertInformations(Informations informations) {
        ContentValues values = this.fillValues(informations);

        dataBaseConnection.insertOrThrow("INFORMATIONS", null, values);
    }

    public void deleteLocalization(int id) {
        dataBaseConnection.delete("INFORMATIONS", " _id = ? ", new String[]{Integer.toString(id)});
    }

    public void deleteLocalizations() {
        dataBaseConnection.delete("INFORMATIONS", null, null);
    }

    private ContentValues fillValues(Informations informations) {
        ContentValues values = new ContentValues();
        values.put("LATITUDE", informations.getLatitude());
        values.put("LONGITUDE", informations.getLongitude());
        values.put("OPENAPP", informations.getOpenApp());
        values.put("ISMOVING", informations.isMoving() == null ? null : (informations.isMoving() ? 1 : 0));
        values.put("DATEHOUR", this.getTime(informations.getDateHour()));

        return values;
    }

    public ArrayList<Informations> readLocalization() {
        ArrayList<Informations> informationses = new ArrayList<>();
        Cursor cursor = dataBaseConnection.query("INFORMATIONS", null, null, null, null, null, " _id DESC ");
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                informationses.add(new Informations(cursor.getInt(0), cursor.getDouble(1), cursor.getDouble(2), cursor.getString(3), cursor.getInt(4) == 1, new Date(cursor.getLong(5))));
            } while (cursor.moveToNext());
        }
        return informationses;
    }

    private Long getTime(Date date) {
        if (date != null) {
            return date.getTime();
        }
        return null;
    }
}