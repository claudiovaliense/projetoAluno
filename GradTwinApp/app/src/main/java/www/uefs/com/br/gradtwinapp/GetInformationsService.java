package www.uefs.com.br.gradtwinapp;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import www.uefs.com.br.gradtwinapp.domain.RepositoryInformations;
import www.uefs.com.br.gradtwinapp.domain.entitys.Informations;
import www.uefs.com.br.gradtwinapp.utils.HttpConnection;

public class GetInformationsService extends IntentService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    RepositoryInformations repositoryInformations;
    Intent intent;
    private Handler classesHandler = new Handler();
    private Date lastDate = new Date();


    public GetInformationsService() {
        super("CookieService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        this.intent = intent;

        this.mGoogleApiClient = new GoogleApiClient.Builder(this).addOnConnectionFailedListener(this).addConnectionCallbacks(this).addApi(LocationServices.API).addApi(ActivityRecognition.API).build();
        this.mGoogleApiClient.connect();
        this.mLocationRequest = new LocationRequest();
        this.mLocationRequest.setInterval(60000);
        this.mLocationRequest.setFastestInterval(59000);
        this.mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        this.repositoryInformations = new RepositoryInformations(this);

        /*new Thread(new Runnable() {
            @Override
            public void run() {
                Long time = new Date().getTime();
                while (true) {
                    Long timeTemp = new Date().getTime();
                    if (timeTemp - time > 60000) {
                        System.out.println("TO AQUII");
                        time = timeTemp;
                    }
                }
            }
        }).start();*/
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        //System.out.println("0000000000");
        if (ActivityRecognitionResult.hasResult(this.intent)) {
            //System.out.println("111111111");
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            for (DetectedActivity activity : result.getProbableActivities()) {
                if (activity.getType() == DetectedActivity.STILL) {
                    if (activity.getConfidence() >= 50) {
                        //this.isMoving = false;
                    } else {
                        //this.isMoving = true;
                    }
                }
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        Intent intent = new Intent(this, GetInformationsService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(mGoogleApiClient, 60000, pendingIntent);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        //Descobre o app que está aberto
            String foregroundTaskAppName = null;
            try {
                ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
                ActivityManager.RunningTaskInfo foregroundTaskInfo = activityManager.getRunningTasks(1).get(0);
                String foregroundTaskPackageName = foregroundTaskInfo.topActivity.getPackageName();
                foregroundTaskAppName = getPackageManager().getPackageInfo(foregroundTaskPackageName, 0).applicationInfo.loadLabel(getPackageManager()).toString();
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            this.saveInformations(location, foregroundTaskAppName, this.getIsMoving());
    }

    private void saveInformations(Location location, String openApp, Boolean isMoving) {
        if (new Date().getTime() - this.lastDate.getTime() >= 30000) {
            this.lastDate = new Date();
            Double latitude = null;
            Double longitude = null;
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
            Informations informations = new Informations(null, latitude, longitude, openApp, isMoving, new Date());
            this.repositoryInformations.insertInformations(informations);

            System.out.println("\n----------------------------------------------------------------\n");
            System.out.println("--> LOCATION: " + latitude + ", " + longitude);
            System.out.println("--> OPENAPP: " + openApp);
            System.out.println("--> ISMOVING: " + isMoving);
            System.out.println("--> DATEHOUR: " + informations.getDateHour());

            WifiManager manager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = manager.getConnectionInfo();
            if (info.getBSSID() != null) {
                this.sendJSON();
            }
        }
    }

    private String generateJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        JSONArray informationsArray = new JSONArray();
        for (Informations information : repositoryInformations.readLocalization()) {
            JSONObject informationObj = new JSONObject();

            informationObj.put("dataHora", information.getDateHour().getTime());
            informationObj.put("latitude", information.getLatitude());
            informationObj.put("longitude", information.getLongitude());
            informationObj.put("appAberto", information.getOpenApp());
            informationObj.put("estaParado", information.isMoving() == null ? null : information.isMoving() ? 0 : 1);
            informationObj.put("MAC", this.getMAC());
            System.out.println("Informações: " + informationObj);
            informationsArray.put(informationObj);
        }
        jsonObject.put("listaInformacoes", informationsArray);
        return jsonObject.toString();
    }

    private String getMAC() {
        WifiManager manager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        return info.getMacAddress();
    }

    @SuppressLint("NewApi")
    private void sendJSON() {
        new Thread() {
            public void run() {
                boolean sync = false;
                try {
                    //String answer = "sucesso";
                    //String answer = HttpConnection.sendJSON("http://172.16.209.120:8080/ProjetoAluno/SendInformation", generateJSON());
                    String answer = HttpConnection.sendJSON("http://adam.uefs.br/ProjetoAluno/SendInformation", generateJSON());
                    if (answer != null && answer.equals("sucesso")) {
                        repositoryInformations.deleteLocalizations();
                        sync = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (sync) {
                    classesHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            System.out.println("Informações enviadas com sucesso.");
                        }
                    });
                } else {
                    classesHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            System.out.println("Não foi possível enviar as informações para o servidor. Verifique sua conexão com a internet.");
                        }
                    });
                }
            }
        }.start();
    }

    public Boolean getIsMoving() {
        //System.out.println("0000000000");
        if (ActivityRecognitionResult.hasResult(this.intent)) {
            //System.out.println("111111111");
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(this.intent);
            for (DetectedActivity activity : result.getProbableActivities()) {
                if (activity.getType() == DetectedActivity.STILL) {
                    if (activity.getConfidence() >= 50) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        }
        return null;
    }
}
