package www.uefs.com.br.gradtwinapp.database;

public class ScriptsSQLite {

    public static String getCreateInformations() {
        StringBuilder sqlBilder = new StringBuilder();
        sqlBilder.append("CREATE TABLE IF NOT EXISTS INFORMATIONS ( ");
        sqlBilder.append("_id               INTEGER             NOT NULL    PRIMARY KEY AUTOINCREMENT, ");
        sqlBilder.append("LATITUDE          DOUBLE PRECISION, ");
        sqlBilder.append("LONGITUDE         DOUBLE PRECISION, ");
        sqlBilder.append("OPENAPP           VARCHAR(100), ");
        sqlBilder.append("ISMOVING          INTEGER, ");
        sqlBilder.append("DATEHOUR          LONG ");
        sqlBilder.append("); ");
        return sqlBilder.toString();
    }
}
