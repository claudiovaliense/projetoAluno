package www.uefs.com.br.gradtwinapp.domain.entitys;

import java.io.Serializable;
import java.util.Date;

public class Informations implements Serializable {

    private Integer id;
    private Double latitude;
    private Double longitude;
    private String openApp;
    private Boolean isMoving;
    private Date dateHour;

    public Informations(Integer id, Double latitude, Double longitude, String openApp, Boolean isMoving, Date dateHour) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.openApp = openApp;
        this.isMoving = isMoving;
        this.dateHour = dateHour;
    }

    public Informations() {

    }

    public String getOpenApp() {
        return openApp;
    }

    public void setOpenApp(String openApp) {
        this.openApp = openApp;
    }

    public Boolean isMoving() {
        return isMoving;
    }

    public void setMoving(boolean moving) {
        isMoving = moving;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getDateHour() {
        return dateHour;
    }

    public void setDateHour(Date dateHour) {
        this.dateHour = dateHour;
    }
}
