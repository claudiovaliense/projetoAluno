package br.com.uefs.gradtwin.gradtwinobserver;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Scanner;

import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryClass;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryInLessonTime;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryLesson;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryProfessor;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRelClassStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRelLessonRoom;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRelLessonStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRoom;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Class;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.InLessonTime;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Lesson;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Professor;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelClassStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelLessonRoom;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelLessonStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Room;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Student;
import br.com.uefs.gradtwin.gradtwinobserver.utils.HttpConnection;

public class ClassesActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private RepositoryLesson repositoryLesson;
    private RepositoryClass repositoryClass;
    private RepositoryStudent repositoryStudent;
    private RepositoryRoom repositoryRoom;
    private RepositoryProfessor repositoryProfessor;
    private RepositoryRelClassStudent repositoryRelClassStudent;
    private RepositoryRelLessonRoom repositoryRelLessonRoom;
    private RepositoryRelLessonStudent repositoryRelLessonStudent;
    private RepositoryInLessonTime repositoryInLessonTime;

    private ArrayAdapter<Class> adpClass;
    private ListView lstLesson;
    private ClassFilter filter;
    private Handler classesHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classes);

        EditText etFilter = (EditText) findViewById(R.id.edFilter);
        lstLesson = (ListView) findViewById(R.id.lblist);
        lstLesson.setOnItemClickListener(this);

        try {
            repositoryLesson = new RepositoryLesson(this);
            repositoryRelLessonRoom = new RepositoryRelLessonRoom(this);
            repositoryRelLessonStudent = new RepositoryRelLessonStudent(this);
            repositoryInLessonTime = new RepositoryInLessonTime(this);
            repositoryClass = new RepositoryClass(this);
            repositoryRoom = new RepositoryRoom(this);
            repositoryStudent = new RepositoryStudent(this);
            repositoryProfessor = new RepositoryProfessor(this);
            repositoryRelClassStudent = new RepositoryRelClassStudent(this);
        } catch (Exception ex) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setMessage("Erro na conexão com o banco de dados: " + ex.getMessage());
            dlg.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            dlg.show();
        }
        filter = new ClassFilter(adpClass);
        etFilter.addTextChangedListener(filter);
        this.onActivityResult(0, 0, null);
        this.setTitle("Turmas");
    }

    public void onClickSync(View v) {
        this.sendJSON();
        this.getJSON();
    }

    private String generateJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        JSONArray sectionsArray = new JSONArray();
        for (Lesson lesson : repositoryLesson.selectLessons(null, null, null)) {
            JSONObject sectionObj = new JSONObject();
            JSONObject lessonObj = new JSONObject();
            JSONObject roomObj = new JSONObject();
            JSONArray studentsArray = new JSONArray();

            //lessonObj.put("id", lesson.getId());
            lessonObj.put("idTurma", lesson.getIdClass());
            lessonObj.put("idProfessor", lesson.getIdProfessor());
            lessonObj.put("idRoom", lesson.getIdRoom());
            lessonObj.put("nome", lesson.getName());
            lessonObj.put("dataInicioAula", lesson.getInitTime() == null ? null : lesson.getInitTime().getTime());
            lessonObj.put("dataFimAula", lesson.getEndTime() == null ? null : lesson.getEndTime().getTime());
            lessonObj.put("chegadaProfessor", lesson.getProfessorInTime() == null ? null : lesson.getProfessorInTime().getTime());
            lessonObj.put("saidaProfessor", lesson.getProfessorOutTime() == null ? null : lesson.getProfessorOutTime().getTime());
            sectionObj.put("aula", lessonObj);
            System.out.println("Aula: " + lesson);
            if (!repositoryRelLessonRoom.selectRelLessonRooms(lesson.getId(), lesson.getIdRoom()).isEmpty()) {
                RelLessonRoom relLessonRoom = repositoryRelLessonRoom.selectRelLessonRooms(lesson.getId(), lesson.getIdRoom()).get(0);
                roomObj.put("id", relLessonRoom.getIdRoom());
                roomObj.put("numJanelasAbertas", relLessonRoom.getWindows());
                roomObj.put("numPortasAbertas", relLessonRoom.getDoors());
                roomObj.put("numVentiladoresLigados", relLessonRoom.getFans());  //relLessonRoom.getFans()
                roomObj.put("numLampadasLigadas", relLessonRoom.getLamps());
                roomObj.put("luminosidade", relLessonRoom.getLuminosity());
                roomObj.put("temperatura", relLessonRoom.getTemperature());
                roomObj.put("barulho", relLessonRoom.getNoise());
                roomObj.put("usouQuadro", relLessonRoom.isBoard() == null ? null : relLessonRoom.isBoard() ? 1 : 0);
                roomObj.put("usouProjetor", relLessonRoom.isProjector() == null ? null : relLessonRoom.isProjector() ? 1 : 0);
                roomObj.put("usouArCondicionado", relLessonRoom.isAirconditioner() == null ? null : relLessonRoom.isAirconditioner() ? 1 : 0);
                sectionObj.put("sala", roomObj);
                System.out.println("\tSala: " + repositoryRoom.selectRooms(relLessonRoom.getIdRoom(), null).get(0));
            }
            for (RelLessonStudent relLessonStudent : repositoryRelLessonStudent.selectRelLessonStudents(null, null)) {
                JSONObject studentObj = new JSONObject();
                studentObj.put("id", relLessonStudent.getIdStudent());
                studentObj.put("localAluno", relLessonStudent.getSitPlace());
                studentObj.put("numParticipacoes", relLessonStudent.getParticipationNum());
                studentObj.put("dormiu", relLessonStudent.isSlept() == null ? null : relLessonStudent.isSlept() ? 1 : 0);
                JSONArray inLessonTimeArray = new JSONArray();
                System.out.println("\tAluno: " + repositoryStudent.selectStudents(relLessonStudent.getIdStudent(), null).get(0));
                for (InLessonTime inLessonTime : repositoryInLessonTime.selectInLessonTimes(null, null, null)) {
                    JSONObject inLessonTimeObj = new JSONObject();
                    inLessonTimeObj.put("id", inLessonTime.getId());
                    inLessonTimeObj.put("dataEntrada", inLessonTime.getInitTime() == null ? null : inLessonTime.getInitTime().getTime());
                    inLessonTimeObj.put("dataSaida", inLessonTime.getEndTime() == null ? null : inLessonTime.getEndTime().getTime());
                    inLessonTimeArray.put(inLessonTimeObj);
                    System.out.println("\t\tEntrada/Saida: " + inLessonTime);
                }
                studentObj.put("listEntradaSaidas", inLessonTimeArray);
                studentsArray.put(studentObj);
            }
            sectionObj.put("listaAlunos", studentsArray);
            sectionsArray.put(sectionObj);
        }
        jsonObject.put("listaSecoes", sectionsArray);
        return jsonObject.toString();
    }

    private void degenerateJSON(String data) throws JSONException {
        JSONObject jsonObject = new JSONObject(data);
        JSONArray professorsArray = jsonObject.getJSONArray("listaProfessores");
        JSONArray roomsArray = jsonObject.getJSONArray("listaSalas");
        JSONArray classesArray = jsonObject.getJSONArray("listaTurmas");

        for (int i = 0; i < professorsArray.length(); i++) {
            JSONObject professor = professorsArray.getJSONObject(i);
            repositoryProfessor.insertProfessor(new Professor(professor.getInt("id"), professor.getString("nome"), professor.getString("matricula")));
            System.out.println("Professor: " + professor.getString("nome"));
        }
        for (int i = 0; i < roomsArray.length(); i++) {
            JSONObject room = roomsArray.getJSONObject(i);
            repositoryRoom.insertRoom(new Room(room.getInt("id"), room.getString("nome"), room.getInt("njanelas"), room.getInt("nportas"), room.getInt("nventiladores"), room.getInt("nlampadas"), room.getInt("temquadro") == 1, room.getInt("temprojetor") == 1, room.getInt("temarcondicionado") == 1));
            System.out.println("Sala: " + room.getString("nome"));
        }
        for (int i = 0; i < classesArray.length(); i++) {
            JSONObject _class = classesArray.getJSONObject(i);
            repositoryClass.insertClass(new Class(_class.getInt("id"), _class.getString("turma"), _class.getString("codigoDisciplina"), _class.getString("nomeDisciplina"), _class.getString("semestre"), _class.getInt("idProfessor")));
            System.out.println("Turma: " + _class.getString("turma"));
            JSONArray studentsInClass = _class.getJSONArray("listaAlunos");
            for (int j = 0; j < studentsInClass.length(); j++) {
                JSONObject studentInClass = studentsInClass.getJSONObject(j);
                if (repositoryStudent.selectStudents(studentInClass.getInt("id"), null).isEmpty()) {
                    repositoryStudent.insertStudent(new Student(studentInClass.getInt("id"), studentInClass.getString("nome"), studentInClass.getString("matricula")));
                }
                repositoryRelClassStudent.insertRelClassStudent(new RelClassStudent(_class.getInt("id"), studentInClass.getInt("id")));
                System.out.println("\tAluno: " + studentInClass.getString("nome"));
            }
        }
    }

    @SuppressLint("NewApi")
        private void sendJSON() {
        new Thread() {
            public void run() {
                boolean sync = false;
                try {
                    //String answer = "sucesso";
                    String answer = HttpConnection.sendJSON("http://adam.uefs.br/ProjetoAluno/SendLesson", generateJSON());
                    //String answer = HttpConnection.sendJSON("http://172.16.209.120:8080/ProjetoAluno/SendLesson", generateJSON());
                    if (answer != null && answer.equals("sucesso")) {
                        repositoryLesson.deleteLessons(null, null, null);
                        repositoryRelLessonStudent.deleteRelLessonStudents(null, null);
                        repositoryRelLessonRoom.deleteRelLessonRooms(null, null);
                        repositoryInLessonTime.deleteInLessonTimes(null, null, null);
                        sync = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (sync) {
                    classesHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            showMensage("Aulas enviadas com sucesso.");
                        }
                    });
                } else {
                    classesHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            showMensage("Não foi possível enviar as aulas para o servidor. Verifique sua conexão com a internet.");
                        }
                    });
                }
            }
        }.start();
    }

    @SuppressLint("NewApi")
    private void getJSON() {
        new Thread() {
            public void run() {
                boolean sync = false;
                try {
                    //String answer = new Scanner(getApplicationContext().getAssets().open("json.txt")).useDelimiter("\\Z").next();
                    String answer = HttpConnection.getJSON("http://adam.uefs.br/ProjetoAluno/GetData");
                    //String answer = HttpConnection.getJSON("http://172.16.209.120:8080/ProjetoAluno/GetData");
                    if (answer != null) {
                        repositoryClass.deleteClasses(null, null, null, null);
                        repositoryRoom.deleteRooms(null, null);
                        repositoryStudent.deleteStutents(null, null);
                        repositoryProfessor.deleteProfessors(null, null);
                        repositoryRelClassStudent.deleteRelClassStudents(null, null);

                        degenerateJSON(answer);
                        sync = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (sync) {
                    classesHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            adpClass.clear();
                            adpClass.addAll(repositoryClass.selectClasses(null, null, null, null));
                            showMensage("Dados recebidos com sucesso.");
                        }
                    });
                } else {
                    classesHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            showMensage("Não foi possível receber os dados atualizados do servidor. Verifique sua conexão com a internet.");
                        }
                    });
                }
            }
        }.start();
    }

    private void showMensage(String mensage) {
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        dlg.setMessage(mensage);
        dlg.setNeutralButton("OK", null);
        dlg.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        adpClass = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, repositoryClass.selectClasses(null, null, null, null));
        lstLesson.setAdapter(adpClass);
        filter.setAdapter(adpClass);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Class _class = this.adpClass.getItem(position);
        Intent it = new Intent(this, LessonsActivity.class);
        it.putExtra("IDCLASS", _class.getId());
        startActivityForResult(it, 1);
    }

    private class ClassFilter implements TextWatcher {
        ArrayAdapter<Class> adapter;

        void setAdapter(ArrayAdapter<Class> adapter) {
            this.adapter = adapter;
        }

        private ClassFilter(ArrayAdapter<Class> adapter) {
            this.adapter = adapter;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            adapter.getFilter().filter(s);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
