package br.com.uefs.gradtwin.gradtwinobserver;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.InLessonTime;

public class InLessonTimeActivity extends AppCompatActivity {
    private RepositoryStudent repositoryStudent;

    private EditText etInitDate, etEndDate;

    private Integer id, idLesson, idStudent;
    private Date initDate, endDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inoutstudent);

        etInitDate = (EditText) findViewById(R.id.edInitDate);
        etEndDate = (EditText) findViewById(R.id.edEndDate);
        etInitDate.setMaxWidth(etInitDate.getWidth());
        etEndDate.setMaxWidth(etEndDate.getWidth());

        ShowInitDateSelecterListener listenerDateInit = new ShowInitDateSelecterListener();
        this.etInitDate.setOnClickListener(listenerDateInit);
        this.etInitDate.setOnFocusChangeListener(listenerDateInit);
        this.etInitDate.setFocusable(false);

        ShowEndDateSelecterListener listenerDateEnd = new ShowEndDateSelecterListener();
        this.etEndDate.setOnClickListener(listenerDateEnd);
        this.etEndDate.setOnFocusChangeListener(listenerDateEnd);
        this.etEndDate.setFocusable(false);

        try {
            repositoryStudent = new RepositoryStudent(this);
        } catch (Exception ex) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setMessage("Erro na conecção com o banco de dados: " + ex.getMessage());
            dlg.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            dlg.show();
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("INLESSONTIME")) {
                InLessonTime inLessonTime = (InLessonTime) bundle.getSerializable("INLESSONTIME");
                this.id = inLessonTime.getId();
                this.idLesson = inLessonTime.getIdLesson();
                this.idStudent = inLessonTime.getIdStudent();

                Calendar cal = Calendar.getInstance();
                this.initDate = inLessonTime.getInitTime();
                cal.setTime(this.initDate);
                this.etInitDate.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(this.initDate) + ", " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
                this.endDate = inLessonTime.getEndTime();
                cal.setTime(this.endDate);
                this.etEndDate.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(this.endDate) + ", " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));

                this.setTitle("Adicionar Período em Sala - " + new RepositoryStudent(this).selectStudents(this.idStudent, null).get(0).getName());
                if (inLessonTime.getIdStudent() != null && inLessonTime.getIdLesson() != null) {
                    this.setTitle("Editar Entrada/Saída - " + repositoryStudent.selectStudents(this.idStudent, null));
                }
            }
        }
    }

    public void onClickSave(View v){
        if (this.etInitDate.getText().toString().isEmpty()) {
            this.etInitDate.setError("Preencha o campo 'Data/hora de início'");
            this.etInitDate.requestFocus();
        } else if (this.etEndDate.getText().toString().isEmpty()) {
            this.etEndDate.setError("Preencha o campo 'Data/hora de término'");
            this.etEndDate.requestFocus();
        } else {
            Intent it = new Intent();
            it.putExtra("INLESSONTIME", new InLessonTime(this.id, this.idLesson, this.idStudent, this.initDate, this.endDate));
            setResult(0, it);
            finish();
        }
    }

    public void onClickDelete(View v){
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        dlg.setMessage("Tem certeza que deseja excluir esta aula dos registros?");
        dlg.setNegativeButton("CANCELAR", null);
        dlg.setPositiveButton("CONFIRMAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent it = new Intent();
                it.putExtra("DELETE", true);
                setResult(0, it);
                finish();
            }
        });
        dlg.show();
    }

    public void onClickCancel(View v){
        finish();
    }

    private class ShowInitDateSelecterListener implements View.OnClickListener, View.OnFocusChangeListener {
        @Override
        public void onClick(View v) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(initDate);
            new DatePickerDialog(v.getContext(), new ReturnDateListener(true, cal), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(hasFocus) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(initDate);
                new DatePickerDialog(v.getContext(), new ReturnDateListener(true, cal), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        }
    }

    private class ShowEndDateSelecterListener implements View.OnClickListener, View.OnFocusChangeListener {
        @Override
        public void onClick(View v) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(endDate);
            new DatePickerDialog(v.getContext(), new ReturnDateListener(false, cal), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(hasFocus) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(endDate);
                new DatePickerDialog(v.getContext(), new ReturnDateListener(false, cal), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        }
    }

    private class ReturnDateListener implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
        Calendar cal;
        boolean init;
        DateFormat format;

        ReturnDateListener(boolean init, Calendar cal) {
            this.init = init;
            this.cal = cal;
            this.format = DateFormat.getDateInstance(DateFormat.SHORT);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            cal.set(year, monthOfYear, dayOfMonth);
            if (init) {
                new TimePickerDialog(view.getContext(), this, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show();
            } else {
                new TimePickerDialog(view.getContext(), this, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show();
            }
        }

        @Override
        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);
            if (init) {
                initDate = cal.getTime();
                etInitDate.setText(format.format(initDate) + " - " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
            } else {
                endDate = cal.getTime();
                etEndDate.setText(format.format(endDate) + " - " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
            }
        }
    }
}
