package br.com.uefs.gradtwin.gradtwinobserver.domain.entitys;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class InLessonTime implements Serializable {

    private Integer id;
    private Integer idLesson;
    private Integer idStudent;
    private Date initTime;
    private Date endTime;

    public InLessonTime() {
    }

    public InLessonTime(Integer id, Integer idLesson, Integer idStudent, Date initTime, Date endTime) {
        this.id = id;
        this.idLesson = idLesson;
        this.idStudent = idStudent;
        this.initTime = initTime;
        this.endTime = endTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getInitTime() {
        return initTime;
    }

    public void setInitTime(Date initTime) {
        this.initTime = initTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getIdLesson() {
        return idLesson;
    }

    public void setIdLesson(Integer idLesson) {
        this.idLesson = idLesson;
    }

    public Integer getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Integer idStudent) {
        this.idStudent = idStudent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InLessonTime that = (InLessonTime) o;

        return id != null && id.equals(that.id);
    }

    @Override
    public String toString() {
        Calendar calInit = Calendar.getInstance();
        Calendar calEnd = Calendar.getInstance();
        calInit.setTime(this.initTime);
        calEnd.setTime(this.endTime);
        return calInit.get(Calendar.HOUR_OF_DAY) + ":" + calInit.get(Calendar.MINUTE) + " - " + calEnd.get(Calendar.HOUR_OF_DAY) + ":" + calEnd.get(Calendar.MINUTE);
    }
}
