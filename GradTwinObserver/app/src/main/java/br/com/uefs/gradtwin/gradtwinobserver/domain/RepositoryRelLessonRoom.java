package br.com.uefs.gradtwin.gradtwinobserver.domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import br.com.uefs.gradtwin.gradtwinobserver.database.DataBase;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelLessonRoom;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Student;

public class RepositoryRelLessonRoom {
    private SQLiteDatabase dataBaseConnection;
    private Context context;

    public RepositoryRelLessonRoom(Context context) {
        this.context = context;
        DataBase dataBase = new DataBase(context);
        this.dataBaseConnection = dataBase.getWritableDatabase();
    }

    public void insertRelLessonRoom(RelLessonRoom relLessonRoom) {
        ContentValues values = this.fillValues(relLessonRoom);

        dataBaseConnection.insertOrThrow("RELLESSONROOM", null, values);
    }

    public void updateRelLessonRoom(RelLessonRoom relLessonRoom, int idLesson, int idRoom) {
        ContentValues values = this.fillValues(relLessonRoom);

        dataBaseConnection.update("RELLESSONROOM", values, " ID_LESSON = ? AND ID_ROOM = ? ", new String[]{Integer.toString(idLesson), Integer.toString(idRoom)});
    }

    public ArrayList<RelLessonRoom> selectRelLessonRooms(Integer idLesson, Integer idRoom) {
        ArrayList<RelLessonRoom> relLessonRooms = new ArrayList<>();
        String _idLesson = idLesson == null ? " 1 = 1 " : " ID_LESSON = " + idLesson.toString() + " ";
        String _idRoom = idRoom == null ? " 1 = 1 " : " ID_ROOM = " + idRoom.toString() + " ";
        String where = _idLesson + " AND " + _idRoom + " ";
        Cursor cursor = dataBaseConnection.query("RELLESSONROOM", null, where, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                relLessonRooms.add(new RelLessonRoom(cursor.getInt(0), cursor.getInt(1), cursor.isNull(2) ? null : cursor.getInt(2), cursor.isNull(3) ? null : cursor.getInt(3), cursor.isNull(4) ? null : cursor.getInt(4), cursor.isNull(5) ? null : cursor.getInt(5), cursor.isNull(6) ? null : cursor.getInt(6), cursor.isNull(7) ? null : cursor.getInt(7), cursor.isNull(8) ? null : cursor.getInt(8), cursor.isNull(9) ? null : cursor.getInt(9) == 1, cursor.isNull(10) ? null : cursor.getInt(10) == 1, cursor.isNull(11) ? null : cursor.getInt(11) == 1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return relLessonRooms;
    }

    public void deleteRelLessonRoom(int idLesson, int idRoom) {
        dataBaseConnection.delete("RELLESSONROOM", " ID_LESSON = ? AND ID_ROOM = ? ", new String[]{Integer.toString(idLesson), Integer.toString(idRoom)});
    }

    public void deleteRelLessonRooms(Integer idLesson, Integer idRoom) {
        for (RelLessonRoom relLessonRoom: this.selectRelLessonRooms(idLesson, idRoom)) {
            this.deleteRelLessonRoom(relLessonRoom.getIdLesson(), relLessonRoom.getIdRoom());
        }
    }

    private ContentValues fillValues(RelLessonRoom relLessonRoom) {
        ContentValues values = new ContentValues();
        values.put("ID_LESSON", relLessonRoom.getIdLesson());
        values.put("ID_ROOM", relLessonRoom.getIdRoom());
        values.put("WINDOWS", relLessonRoom.getWindows());
        values.put("DOORS", relLessonRoom.getDoors());
        values.put("FANS", relLessonRoom.getFans());
        values.put("LAMPS", relLessonRoom.getLamps());
        values.put("LUMINOSITY", relLessonRoom.getLuminosity());
        values.put("TEMPERATURE", relLessonRoom.getTemperature());
        values.put("NOISE", relLessonRoom.getNoise());
        values.put("BOARD", this.getValueOfBoolean(relLessonRoom.isBoard()));
        values.put("PROJECTOR", this.getValueOfBoolean(relLessonRoom.isProjector()));
        values.put("AIRCONDITIONER", this.getValueOfBoolean(relLessonRoom.isAirconditioner()));

        return values;
    }

    private Integer getValueOfBoolean (Boolean b) {
        return b == null ? null : (b ? 1 : 0);
    }
}
