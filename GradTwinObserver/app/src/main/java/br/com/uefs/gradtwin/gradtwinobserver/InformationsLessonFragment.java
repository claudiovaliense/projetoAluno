package br.com.uefs.gradtwin.gradtwinobserver;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryProfessor;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRelLessonRoom;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRoom;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Lesson;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Professor;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelLessonRoom;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Room;

public class InformationsLessonFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private RepositoryProfessor repositoryProfessor;
    private RepositoryRoom repositoryRoom;
    private RepositoryRelLessonRoom repositoryRelLessonRoom;

    private ArrayAdapter<Professor> adpProfessor;
    private ArrayAdapter<Room> adpRoom;

    private EditText etName, etInitDate, etEndDate, etProfessorInitDate, etProfessorEndDate, etLamps, etDoors, etWindows, etTemperature, etNoise, etFans, etLuminosity;
    private Button btStart, btFinish;
    private Spinner spProfessor, spRoom;
    private CheckBox chBoard, chProjector, chAirConditioner;

    private Integer id = null, idClass = null, idProfessor = null, idRoom = null;
    private Date initDate, endDate, professorInitDate = null, professorEndDate = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_informations_lesson, container, false);

        etName = view.findViewById(R.id.edName);
        btStart = view.findViewById(R.id.btStart);
        btFinish = view.findViewById(R.id.btFinish);
        spProfessor = view.findViewById(R.id.spProfessor);
        spRoom = view.findViewById(R.id.spRoom);
        etLamps = view.findViewById(R.id.etLamps);
        etDoors = view.findViewById(R.id.etDoors);
        etWindows = view.findViewById(R.id.etWindows);
        etTemperature = view.findViewById(R.id.etTemperature);
        etNoise = view.findViewById(R.id.etNoise);
        etFans = view.findViewById(R.id.etFans);
        etLuminosity = view.findViewById(R.id.etLuminosity);
        chBoard = view.findViewById(R.id.chBoard);
        chProjector = view.findViewById(R.id.chProjector);
        chAirConditioner = view.findViewById(R.id.chAirConditioner);
        etInitDate = view.findViewById(R.id.edInitDate);
        etEndDate = view.findViewById(R.id.edEndDate);
        etProfessorInitDate = view.findViewById(R.id.edProfessorInDate);
        etProfessorEndDate = view.findViewById(R.id.edProfessorOutDate);
        etLamps.setMaxWidth(etLamps.getWidth());
        etDoors.setMaxWidth(etDoors.getWidth());
        etWindows.setMaxWidth(etWindows.getWidth());
        etTemperature.setMaxWidth(etTemperature.getWidth());
        etNoise.setMaxWidth(etNoise.getWidth());
        etFans.setMaxWidth(etFans.getWidth());
        etLuminosity.setMaxWidth(etLuminosity.getWidth());
        etInitDate.setMaxWidth(etInitDate.getWidth());
        etEndDate.setMaxWidth(etEndDate.getWidth());
        etProfessorInitDate.setMaxWidth(etProfessorInitDate.getWidth());
        etProfessorEndDate.setMaxWidth(etProfessorEndDate.getWidth());
        spProfessor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spProfessor.getSelectedItemPosition() == 0) {
                    idProfessor = null;
                } else {
                    idProfessor = ((Professor) spProfessor.getSelectedItem()).getId();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
        spRoom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spRoom.getSelectedItemPosition() == 0) {
                    idRoom = null;
                } else {
                    idRoom = ((Room) spRoom.getSelectedItem()).getId();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        ShowInitDateSelecterListener listenerDateInit = new ShowInitDateSelecterListener();
        this.etInitDate.setOnClickListener(listenerDateInit);
        this.etInitDate.setOnFocusChangeListener(listenerDateInit);
        this.etInitDate.setFocusable(false);

        ShowEndDateSelecterListener listenerDateEnd = new ShowEndDateSelecterListener();
        this.etEndDate.setOnClickListener(listenerDateEnd);
        this.etEndDate.setOnFocusChangeListener(listenerDateEnd);
        this.etEndDate.setFocusable(false);

        ShowProfessorInitDateSelecterListener listenerProfessorDateInit = new ShowProfessorInitDateSelecterListener();
        this.etProfessorInitDate.setOnClickListener(listenerProfessorDateInit);
        this.etProfessorInitDate.setOnFocusChangeListener(listenerProfessorDateInit);
        this.etProfessorInitDate.setFocusable(false);

        ShowProfessorEndDateSelecterListener listenerProfessorDateEnd = new ShowProfessorEndDateSelecterListener();
        this.etProfessorEndDate.setOnClickListener(listenerProfessorDateEnd);
        this.etProfessorEndDate.setOnFocusChangeListener(listenerProfessorDateEnd);
        this.etProfessorEndDate.setFocusable(false);

        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                initDate = new Date();
                cal.setTime(initDate);
                etInitDate.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(initDate) + ", " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
            }
        });
        btFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                endDate = new Date();
                cal.setTime(endDate);
                etEndDate.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(endDate) + ", " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
            }
        });

        try {
            repositoryProfessor = new RepositoryProfessor(this.getContext());
            repositoryRoom = new RepositoryRoom(this.getContext());
            repositoryRelLessonRoom = new RepositoryRelLessonRoom(this.getContext());
        } catch (Exception ex) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this.getContext());
            dlg.setMessage("Erro na conecção com o banco de dados: " + ex.getMessage());
            dlg.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
                }
            });
            dlg.show();
        }

        adpProfessor = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_list_item_1, repositoryProfessor.selectProfessors(null, null));
        adpProfessor.insert(new Professor(), 0);
        adpRoom = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_list_item_1, repositoryRoom.selectRooms(null, null));
        adpRoom.insert(new Room(), 0);
        spProfessor.setAdapter(adpProfessor);
        spRoom.setAdapter(adpRoom);
        this.spRoom.setOnItemSelectedListener(this);

        Bundle bundle = getActivity().getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("LESSON")) {
                Lesson lesson = (Lesson) bundle.getSerializable("LESSON");
                RelLessonRoom relLessonRoom;
                if (lesson.getId() != null) {
                    relLessonRoom = repositoryRelLessonRoom.selectRelLessonRooms(lesson.getId(), null).get(0);
                } else {
                    relLessonRoom = new RelLessonRoom();
                }
                this.id = lesson.getId();
                this.idClass = lesson.getIdClass();
                this.idRoom = relLessonRoom.getIdRoom();
                this.idProfessor = lesson.getIdProfessor();
                this.etName.setText(lesson.getName());
                if (relLessonRoom.getDoors() != null) {
                    this.etDoors.setText(relLessonRoom.getDoors().toString());
                } if (relLessonRoom.getFans() != null) {
                    this.etFans.setText(relLessonRoom.getFans().toString());
                } if (relLessonRoom.getLamps() != null) {
                    this.etLamps.setText(relLessonRoom.getLamps().toString());
                } if (relLessonRoom.getLuminosity() != null) {
                    this.etLuminosity.setText(relLessonRoom.getLuminosity().toString());
                } if (relLessonRoom.getNoise() != null) {
                    this.etNoise.setText(relLessonRoom.getNoise().toString());
                } if (relLessonRoom.getTemperature() != null) {
                    this.etTemperature.setText(relLessonRoom.getTemperature().toString());
                } if (relLessonRoom.getWindows() != null) {
                    this.etWindows.setText(relLessonRoom.getWindows().toString());
                }
                this.chBoard.setChecked(this.getBoolValue(relLessonRoom.isBoard()));
                this.chProjector.setChecked(this.getBoolValue(relLessonRoom.isProjector()));
                this.chAirConditioner.setChecked(this.getBoolValue(relLessonRoom.isAirconditioner()));

                if (idProfessor != null && !repositoryProfessor.selectProfessors(this.idProfessor, null).isEmpty()) {
                    spProfessor.setSelection(adpProfessor.getPosition(repositoryProfessor.selectProfessors(this.idProfessor, null).get(0)));
                } if (idRoom != null && !repositoryRoom.selectRooms(this.idRoom, null).isEmpty()) {
                    spRoom.setSelection(adpRoom.getPosition(repositoryRoom.selectRooms(this.idRoom, null).get(0)));
                }

                Calendar cal = Calendar.getInstance();
                this.initDate = lesson.getInitTime();
                cal.setTime(this.initDate);
                this.etInitDate.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(this.initDate) + ", " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
                this.endDate = lesson.getEndTime();
                cal.setTime(this.endDate);
                this.etEndDate.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(this.endDate) + ", " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
                if (lesson.getProfessorInTime() != null) {
                    this.professorInitDate = lesson.getProfessorInTime();
                    cal.setTime(this.professorInitDate);
                    this.etProfessorInitDate.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(this.professorInitDate) + ", " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
                } if (lesson.getProfessorOutTime() != null) {
                    this.professorEndDate = lesson.getProfessorOutTime();
                    cal.setTime(this.professorEndDate);
                    this.etProfessorEndDate.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(this.professorEndDate) + ", " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
                }
            }
        }
        return view;
    }

    private boolean getBoolValue(Boolean b) {
        if (b == null) {
            return false;
        } else {
            return b;
        }
    }

    public boolean save() {
        if (this.etName.getText().toString().isEmpty()) {
            this.etName.setError("Preencha o campo nome");
            this.etName.requestFocus();
        } else if (this.etInitDate.getText().toString().isEmpty()) {
            this.etInitDate.setError("Preencha o campo 'Data/hora de início'");
            this.etInitDate.requestFocus();
        } else if (this.etEndDate.getText().toString().isEmpty()) {
            this.etEndDate.setError("Preencha o campo 'Data/hora de término'");
            this.etEndDate.requestFocus();
        } else if (this.adpRoom.isEmpty() || this.spRoom.getSelectedItemPosition() == 0) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this.getContext());
            dlg.setMessage("Escolha uma sala válida.");
            dlg.setNeutralButton("OK", null);
            dlg.show();
            this.spRoom.requestFocus();
        } else if (this.adpProfessor.isEmpty() || this.spProfessor.getSelectedItemPosition() == 0) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this.getContext());
            dlg.setMessage("Escolha um professor válido.");
            dlg.setNeutralButton("OK", null);
            dlg.show();
            this.spProfessor.requestFocus();
        } else {
            return true;
        }
        return false;
    }

    public Lesson getLesson() {
        return new Lesson(this.id, getActivity().getIntent().getExtras().getInt("IDCLASS"), this.idProfessor, this.idRoom, this.etName.getText().toString(), this.initDate, this.endDate, this.professorInitDate, this.professorEndDate);
    }

    public RelLessonRoom getRelLessonRoom() {
        RelLessonRoom relLessonRoom =  new RelLessonRoom(this.id, this.idRoom, null, null, null, null, null, null, null, null, null, null);
        if (!this.etWindows.getText().toString().isEmpty()) {
            relLessonRoom.setWindows(Integer.parseInt(this.etWindows.getText().toString()));
        } if (!this.etDoors.getText().toString().isEmpty()) {
            relLessonRoom.setDoors(Integer.parseInt(this.etDoors.getText().toString()));
        } if (!this.etFans.getText().toString().isEmpty()) {
            relLessonRoom.setFans(Integer.parseInt(this.etFans.getText().toString()));
        } if (!this.etLamps.getText().toString().isEmpty()) {
            relLessonRoom.setLamps(Integer.parseInt(this.etLamps.getText().toString()));
        } if (!this.etLuminosity.getText().toString().isEmpty()) {
            relLessonRoom.setLuminosity(Integer.parseInt(this.etLuminosity.getText().toString()));
        } if (!this.etNoise.getText().toString().isEmpty()) {
            relLessonRoom.setNoise(Integer.parseInt(this.etNoise.getText().toString()));
        } if (!this.etTemperature.getText().toString().isEmpty()) {
            relLessonRoom.setTemperature(Integer.parseInt(this.etTemperature.getText().toString()));
        } if (this.chBoard.getVisibility() == View.VISIBLE) {
            relLessonRoom.setBoard(chBoard.isChecked());
        } if (this.chProjector.getVisibility() == View.VISIBLE) {
            relLessonRoom.setProjector(chProjector.isChecked());
        } if (this.chAirConditioner.getVisibility() == View.VISIBLE) {
            relLessonRoom.setAirConditioner(chAirConditioner.isChecked());
        }
        return relLessonRoom;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i > 0) {
            this.idRoom = ((Room) spRoom.getSelectedItem()).getId();
            Room room = this.repositoryRoom.selectRooms(this.idRoom, null).get(0);
            if (!room.isProjector()) {
                this.chProjector.setVisibility(View.GONE);
            } else {
                this.chProjector.setVisibility(View.VISIBLE);
            }
            if (!room.isBoard()) {
                this.chBoard.setVisibility(View.GONE);
            } else {
                this.chBoard.setVisibility(View.VISIBLE);
            }
            if (!room.isAirconditioner()) {
                this.chAirConditioner.setVisibility(View.GONE);
            }  else {
                this.chAirConditioner.setVisibility(View.VISIBLE);
            }
            if (room.getWindows() < 1) {
                this.etWindows.setVisibility(View.GONE);
            } else {
                this.etWindows.setVisibility(View.VISIBLE);
            }
            if (room.getFans() < 1) {
                this.etFans.setVisibility(View.GONE);
            } else {
                this.etFans.setVisibility(View.VISIBLE);
            }
            if (room.getLamps() < 1) {
                this.etLamps.setVisibility(View.GONE);
            }  else {
                this.etLamps.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private class ShowInitDateSelecterListener implements View.OnClickListener, View.OnFocusChangeListener {
        @Override
        public void onClick(View v) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            if (initDate != null) {
                cal.setTime(initDate);
            }
            new DatePickerDialog(v.getContext(), new ReturnDateListener(true, false, cal), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                if (initDate != null) {
                    cal.setTime(initDate);
                }
                new DatePickerDialog(v.getContext(), new ReturnDateListener(true, false, cal), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        }
    }

    private class ShowEndDateSelecterListener implements View.OnClickListener, View.OnFocusChangeListener {
        @Override
        public void onClick(View v) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            if (endDate != null) {
                cal.setTime(endDate);
            }
            new DatePickerDialog(v.getContext(), new ReturnDateListener(false, false, cal), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                if (endDate != null) {
                    cal.setTime(endDate);
                }
                new DatePickerDialog(v.getContext(), new ReturnDateListener(false, false, cal), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        }
    }

    private class ShowProfessorInitDateSelecterListener implements View.OnClickListener, View.OnFocusChangeListener {
        @Override
        public void onClick(View v) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            if (professorInitDate != null) {
                cal.setTime(professorInitDate);
            }
            new DatePickerDialog(v.getContext(), new ReturnDateListener(true, true, cal), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                if (professorInitDate != null) {
                    cal.setTime(professorInitDate);
                }
                new DatePickerDialog(v.getContext(), new ReturnDateListener(true, true, cal), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        }
    }

    private class ShowProfessorEndDateSelecterListener implements View.OnClickListener, View.OnFocusChangeListener {
        @Override
        public void onClick(View v) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            if (professorEndDate != null) {
                cal.setTime(professorEndDate);
            }
            new DatePickerDialog(v.getContext(), new ReturnDateListener(false, true, cal), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                if (professorEndDate != null) {
                    cal.setTime(professorEndDate);
                }
                new DatePickerDialog(v.getContext(), new ReturnDateListener(false, true, cal), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        }
    }

    private class ReturnDateListener implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
        Calendar cal;
        boolean init;
        boolean professor;
        DateFormat format;

        ReturnDateListener(boolean init, boolean professor, Calendar cal) {
            this.init = init;
            this.professor = professor;
            this.cal = cal;
            this.format = DateFormat.getDateInstance(DateFormat.SHORT);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            cal.set(year, monthOfYear, dayOfMonth);
            new TimePickerDialog(view.getContext(), this, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show();
        }

        @Override
        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);
            if (init && !professor) {
                initDate = cal.getTime();
                etInitDate.setText(format.format(initDate) + ", " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
            } else if (!init && !professor) {
                endDate = cal.getTime();
                etEndDate.setText(format.format(endDate) + ", " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
            } else if (init && professor) {
                professorInitDate = cal.getTime();
                etProfessorInitDate.setText(format.format(professorInitDate) + ", " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
            } else if (!init && professor) {
                professorEndDate = cal.getTime();
                etProfessorEndDate.setText(format.format(professorEndDate) + ", " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
            }
        }
    }
}
