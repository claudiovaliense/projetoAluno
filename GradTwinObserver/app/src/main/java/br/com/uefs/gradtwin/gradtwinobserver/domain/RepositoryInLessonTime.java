package br.com.uefs.gradtwin.gradtwinobserver.domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;

import br.com.uefs.gradtwin.gradtwinobserver.database.DataBase;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.InLessonTime;

public class RepositoryInLessonTime {
    private SQLiteDatabase dataBaseConnection;
    private Context context;

    public RepositoryInLessonTime(Context context) {
        this.context = context;
        DataBase dataBase = new DataBase(context);
        this.dataBaseConnection = dataBase.getWritableDatabase();
    }

    public void insertInLessonTime(InLessonTime inLessonTime) {
        ContentValues values = this.fillValues(inLessonTime);

        dataBaseConnection.insertOrThrow("INLESSONTIME", null, values);
    }

    public void updateInLessonTime(InLessonTime inLessonTime, int id) {
        ContentValues values = this.fillValues(inLessonTime);

        dataBaseConnection.update("INLESSONTIME", values, " _id = ? ", new String[]{Integer.toString(id)});
    }

    public ArrayList<InLessonTime> selectInLessonTimes(Integer id, Integer idLesson, Integer idStudent) {
        ArrayList<InLessonTime> inLessonTimes = new ArrayList<>();
        String _id = id == null ? " 1 = 1 " : " _id = " + id.toString() + " ";
        String _idLesson = idLesson == null ? " 1 = 1 " : " ID_LESSON = " + idLesson.toString() + " ";
        String _idStudent = idStudent == null ? " 1 = 1 " : " ID_STUDENT = " + idStudent.toString() + " ";
        String where = _id + " AND " + _idLesson + " AND " + _idStudent + " ";
        Cursor cursor = dataBaseConnection.query("INLESSONTIME", null, where, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                inLessonTimes.add(new InLessonTime(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), new Date(cursor.getLong(3)), new Date(cursor.getLong(4))));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return inLessonTimes;
    }

    public void deleteInLessonTime(Integer id) {
        dataBaseConnection.delete("INLESSONTIME", " _id = ? ", new String[]{Integer.toString(id)});
    }

    public void deleteInLessonTimes(Integer id, Integer idLesson, Integer idStudent) {
        for (InLessonTime inLessonTime: this.selectInLessonTimes(id, idLesson, idStudent)) {
            this.deleteInLessonTime(inLessonTime.getId());
        }
    }

    private ContentValues fillValues(InLessonTime inLessonTime) {
        ContentValues values = new ContentValues();
        values.put("ID_LESSON", inLessonTime.getIdLesson());
        values.put("ID_STUDENT", inLessonTime.getIdStudent());
        values.put("INITTIME", this.getTime(inLessonTime.getInitTime()));
        values.put("ENDTIME", this.getTime(inLessonTime.getEndTime()));

        return values;
    }

    private Long getTime(Date date) {
        if (date != null) {
            return date.getTime();
        }
        return null;
    }
}
