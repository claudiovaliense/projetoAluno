package br.com.uefs.gradtwin.gradtwinobserver.domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import br.com.uefs.gradtwin.gradtwinobserver.database.DataBase;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Room;

public class RepositoryRoom {
    private final Context context;
    private SQLiteDatabase dataBaseConnection;

    public RepositoryRoom(Context context) {
        DataBase dataBase = new DataBase(context);
        this.dataBaseConnection = dataBase.getWritableDatabase();
        this.context = context;
    }

    public void insertRoom(Room room) {
        ContentValues values = this.fillValues(room);

        dataBaseConnection.insertOrThrow("ROOM", null, values);
    }

    public void updateRoom(Room room, int id) {
        ContentValues values = this.fillValues(room);

        dataBaseConnection.update("ROOM", values, " _id = ? ", new String[]{Integer.toString(id)});
    }

    public ArrayList<Room> selectRooms(Integer id, String name) {
        ArrayList<Room> rooms = new ArrayList<>();
        String _id = id == null ? " 1 = 1 " : " _id = " + id.toString() + " ";
        name = name == null ? " 1 = 1 " : " NAME = '" + name + "' ";
        String where = _id + " AND " + name + " ";
        Cursor cursor = dataBaseConnection.query("ROOM", null, where, null, null, null, " NAME ASC ");
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                rooms.add(new Room(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5), cursor.getInt(6) == 1, cursor.getInt(7) == 1, cursor.getInt(8) == 1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return rooms;
    }

    public void deleteRoom(int id) {
        dataBaseConnection.delete("ROOM", " _id = ? ", new String[]{Integer.toString(id)});
    }

    public void deleteRooms(Integer id, String name) {
        for (Room room: this.selectRooms(id, name)) {
            this.deleteRoom(room.getId());
        }
    }

    private ContentValues fillValues(Room room) {
        ContentValues values = new ContentValues();
        values.put("_id", room.getId());
        values.put("NAME", room.getName());
        values.put("WINDOWS", room.getWindows());
        values.put("DOORS", room.getDoors());
        values.put("FANS", room.getFans());
        values.put("LAMPS", room.getLamps());
        values.put("BOARD", room.isBoard() ? 1 : 0);
        values.put("PROJECTOR", room.isProjector() ? 1 : 0);
        values.put("AIRCONDITIONER", room.isAirconditioner() ? 1 : 0);

        return values;
    }
}
