package br.com.uefs.gradtwin.gradtwinobserver;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryInLessonTime;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRelClassStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Class;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.InLessonTime;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Lesson;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelLessonStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Student;

public class StudentsInLessonFragment extends Fragment implements AdapterView.OnItemClickListener {
    private RepositoryRelClassStudent repositoryRelClassStudent;
    private RepositoryInLessonTime repositoryInLessonTime;

    private ArrayAdapter<Student> adpStudent;
    private ArrayList<RelLessonStudent> relLessonStudents = new ArrayList<>();
    private ArrayList<InLessonTime> inlessontimes = new ArrayList<>();
    private ListView lstStudent;

    private Integer idLesson;
    private Integer idClass;
    private StudentFilter filter;
    private Long begin;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_students_lesson, container, false);

        EditText etFilter = view.findViewById(R.id.edFilter);
        lstStudent = view.findViewById(R.id.lblist);
        lstStudent.setOnItemClickListener(this);

        this.idLesson = 0;
        this.idClass = null;
        this.begin = null;

        try {
            repositoryRelClassStudent = new RepositoryRelClassStudent(this.getContext());
            repositoryInLessonTime = new RepositoryInLessonTime(this.getContext());
        } catch (Exception ex) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this.getContext());
            dlg.setMessage("Erro na conecção com o banco de dados: " + ex.getMessage());
            dlg.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
                }
            });
            dlg.show();
        }

        Bundle bundle = getActivity().getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("LESSON")) {
                Lesson lessonTemp = (Lesson) bundle.getSerializable("LESSON");
                if (lessonTemp != null) {
                    this.idLesson = lessonTemp.getId();
                }
            } if (bundle.containsKey("IDCLASS")) {
                this.idClass = bundle.getInt("IDCLASS");
            }
        }
        adpStudent = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_list_item_1, repositoryRelClassStudent.selectStudents(this.idClass));
        lstStudent.setAdapter(adpStudent);

        if (this.idLesson != null && idLesson != 0) {
            this.inlessontimes = new ArrayList<>(repositoryInLessonTime.selectInLessonTimes(null, this.idLesson, null));
        }

        filter = new StudentFilter(adpStudent);
        etFilter.addTextChangedListener(filter);

        return view;
    }

    public ArrayList<RelLessonStudent> getRelLessonStudents() {
        return relLessonStudents;
    }

    public ArrayList<InLessonTime> getInlessontimes() {
        return inlessontimes;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null && data.getExtras() != null) {
            Bundle bundle = data.getExtras();
            if (bundle.containsKey("INLESSONTIMES") && bundle.getSerializable("INLESSONTIMES") != null && !((ArrayList<InLessonTime>) bundle.getSerializable("INLESSONTIMES")).isEmpty()) {
                this.deleteAllInLessonTimes(((ArrayList<InLessonTime>) bundle.getSerializable("INLESSONTIMES")).get(0).getIdStudent());
                this.inlessontimes.addAll((ArrayList<InLessonTime>) bundle.getSerializable("INLESSONTIMES"));
            } if (bundle.containsKey("RELLESSONSTUDENT") && bundle.getSerializable("RELLESSONSTUDENT") != null) {
                RelLessonStudent relLessonStudent = (RelLessonStudent) bundle.getSerializable("RELLESSONSTUDENT");
                this.deleteRelLessonStudent(relLessonStudent.getIdStudent());
                this.relLessonStudents.add(relLessonStudent);
            } if (bundle.containsKey("BEGIN")) {
                this.begin = bundle.getLong("BEGIN");
            } else {
                this.begin = null;
            }
        }
    }

    private void deleteAllInLessonTimes(Integer idStudent) {
        for (int i = 0; i < this.inlessontimes.size(); i++) {
            if (this.inlessontimes.get(i).getIdStudent().equals(idStudent)) {
                this.inlessontimes.remove(i);
                i--;
            }
        }
    }

    private void deleteRelLessonStudent(Integer idStudent) {
        for (int i = 0; i < this.relLessonStudents.size(); i++) {
            if (this.relLessonStudents.get(i).getIdStudent() == idStudent) {
                this.relLessonStudents.remove(i);
                i--;
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Student student = this.adpStudent.getItem(position);
        Intent it = new Intent(this.getContext(), StudentInLessonActivity.class);
        it.putExtra("IDSTUDENT", student.getId());
        if (this.idLesson != null && this.idLesson != 0) {
            it.putExtra("IDLESSON", this.idLesson);
        }
        RelLessonStudent relLessonStudent = this.getRelLessonStudent(student.getId());
        if (relLessonStudent != null && relLessonStudent.getSitPlace() != null) {
            it.putExtra("SITPLACE", relLessonStudent.getSitPlace());
        }
        if (relLessonStudent != null && relLessonStudent.getParticipationNum() != null) {
            it.putExtra("PARTICIPATIONNUM", relLessonStudent.getParticipationNum().intValue());
        }
        if (relLessonStudent != null && relLessonStudent.isSlept() != null) {
            it.putExtra("SLEPT", relLessonStudent.isSlept());
        }
        if (this.inlessontimes != null && !this.inlessontimes.isEmpty()) {
            it.putExtra("INLESSONTIMES", this.getInLessonTimeOfStudent(student.getId()));
        } else if (this.idLesson != null) {
            it.putExtra("INLESSONTIMES", new ArrayList<>(repositoryInLessonTime.selectInLessonTimes(null, this.idLesson, student.getId())));
        }
        if (this.begin != null) {
            it.putExtra("BEGIN", this.begin.longValue());
        }
        startActivityForResult(it, 0);
    }

    private RelLessonStudent getRelLessonStudent(Integer idStudent) {
        for (RelLessonStudent relLessonStudent : this.relLessonStudents) {
            if (relLessonStudent.getIdStudent().equals(idStudent)) {
                return relLessonStudent;
            }
        }
        return null;
    }

    private ArrayList<InLessonTime> getInLessonTimeOfStudent(Integer idStudent) {
        ArrayList<InLessonTime> inLessonTimesTemp = new ArrayList<>();
        for (InLessonTime inLessonTime : this.inlessontimes) {
            if (inLessonTime.getIdStudent().equals(idStudent)) {
                inLessonTimesTemp.add(inLessonTime);
            }
        }
        return inLessonTimesTemp;
    }

    private class StudentFilter implements TextWatcher {
        ArrayAdapter<Student> adapter;

        void setAdapter(ArrayAdapter<Student> adapter) {
            this.adapter = adapter;
        }

        private StudentFilter(ArrayAdapter<Student> adapter) {
            this.adapter = adapter;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            adapter.getFilter().filter(s);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}