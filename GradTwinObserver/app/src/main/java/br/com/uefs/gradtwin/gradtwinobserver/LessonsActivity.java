package br.com.uefs.gradtwin.gradtwinobserver;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryClass;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryInLessonTime;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryProfessor;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRelClassStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRelLessonRoom;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRelLessonStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRoom;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Class;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.InLessonTime;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Lesson;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryLesson;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Professor;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelClassStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelLessonRoom;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelLessonStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Room;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Student;
import br.com.uefs.gradtwin.gradtwinobserver.utils.HttpConnection;

public class LessonsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private RepositoryLesson repositoryLesson;
    private RepositoryClass repositoryClass;

    private ArrayAdapter<Lesson> adpLesson;
    private ListView lstLesson;

    private LessonFilter filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lessons);

        EditText etFilter = (EditText) findViewById(R.id.edFilter);
        lstLesson = (ListView) findViewById(R.id.lblist);
        lstLesson.setOnItemClickListener(this);

        try {
            repositoryLesson = new RepositoryLesson(this);
            repositoryClass = new RepositoryClass(this);
        } catch (Exception ex) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setMessage("Erro na conecção com o banco de dados: " + ex.getMessage());
            dlg.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            dlg.show();
        }
        filter = new LessonFilter(adpLesson);
        etFilter.addTextChangedListener(filter);
        this.onActivityResult(0, 0, null);
        this.setTitle("Aulas: " + this.repositoryClass.selectClasses(getIntent().getExtras().getInt("IDCLASS"), null, null, null).get(0).toString());
    }

    public void onClickNewLesson(View v) {
        Intent it = new Intent(this, LessonActivity.class);
        it.putExtra("IDCLASS", getIntent().getExtras().getInt("IDCLASS"));
        startActivityForResult(it, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        adpLesson = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, repositoryLesson.selectLessons(null, this.getIntent().getExtras().getInt("IDCLASS"), null));
        lstLesson.setAdapter(adpLesson);
        filter.setAdapter(adpLesson);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Lesson lesson = this.adpLesson.getItem(position);
        Intent it = new Intent(this, LessonActivity.class);
        it.putExtra("LESSON", lesson);
        it.putExtra("IDCLASS", lesson.getIdClass());
        startActivityForResult(it, 1);
    }

    private class LessonFilter implements TextWatcher {
        ArrayAdapter<Lesson> adapter;

        void setAdapter(ArrayAdapter<Lesson> adapter) {
            this.adapter = adapter;
        }

        private LessonFilter(ArrayAdapter<Lesson> adapter) {
            this.adapter = adapter;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            adapter.getFilter().filter(s);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
