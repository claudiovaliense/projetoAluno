package br.com.uefs.gradtwin.gradtwinobserver.database;

class ScriptsSQLite {

    static String getCreateProfessorTable() {
        StringBuilder sqlBilder;
        sqlBilder = new StringBuilder();
        sqlBilder.append("CREATE TABLE IF NOT EXISTS PROFESSOR ( ");
        sqlBilder.append("_id               INTEGER             NOT NULL    PRIMARY KEY, ");
        sqlBilder.append("NAME              VARCHAR(100), ");
        sqlBilder.append("REGISTRATION      VARCHAR(20) ");
        sqlBilder.append("); ");
        return sqlBilder.toString();
    }

    static String getCreateRoomTable() {
        StringBuilder sqlBilder;
        sqlBilder = new StringBuilder();
        sqlBilder.append("CREATE TABLE IF NOT EXISTS ROOM ( ");
        sqlBilder.append("_id               INTEGER        NOT NULL    PRIMARY KEY, ");
        sqlBilder.append("NAME              VARCHAR(100), ");
        sqlBilder.append("WINDOWS           INTEGER, ");
        sqlBilder.append("DOORS             INTEGER, ");
        sqlBilder.append("FANS              INTEGER, ");
        sqlBilder.append("LAMPS             INTEGER, ");
        sqlBilder.append("BOARD             BOOLEAN, ");
        sqlBilder.append("PROJECTOR         BOOLEAN, ");
        sqlBilder.append("AIRCONDITIONER    BOOLEAN ");
        sqlBilder.append("); ");
        return sqlBilder.toString();
    }

    static String getCreateStudentTable() {
        StringBuilder sqlBilder;
        sqlBilder = new StringBuilder();
        sqlBilder.append("CREATE TABLE IF NOT EXISTS STUDENT ( ");
        sqlBilder.append("_id               INTEGER        NOT NULL    PRIMARY KEY, ");
        sqlBilder.append("NAME              VARCHAR(100), ");
        sqlBilder.append("REGISTRATION      VARCHAR(20) ");
        sqlBilder.append("); ");
        return sqlBilder.toString();
    }

    static String getCreateClassTable() {
        StringBuilder sqlBilder;
        sqlBilder = new StringBuilder();
        sqlBilder.append("CREATE TABLE IF NOT EXISTS CLASS ( ");
        sqlBilder.append("_id               INTEGER        NOT NULL    PRIMARY KEY, ");
        sqlBilder.append("CODCLASS          VARCHAR(10), ");
        sqlBilder.append("CODMATTER         VARCHAR(10), ");
        sqlBilder.append("NAMEMATTER        VARCHAR(10), ");
        sqlBilder.append("SEMESTER          VARCHAR(10), ");
        sqlBilder.append("ID_PROFESSOR      INTEGER ");
        sqlBilder.append("); ");
        return sqlBilder.toString();
    }

    static String getCreateRelClassStudentTable() {
        StringBuilder sqlBilder;
        sqlBilder = new StringBuilder();
        sqlBilder.append("CREATE TABLE IF NOT EXISTS RELCLASSSTUDENT ( ");
        sqlBilder.append("ID_CLASS          INTEGER        NOT NULL, ");
        sqlBilder.append("ID_STUDENT        INTEGER        NOT NULL ");
        sqlBilder.append("); ");
        return sqlBilder.toString();
    }

    static String getCreateLessonTable() {/////////////////
        StringBuilder sqlBilder;
        sqlBilder = new StringBuilder();
        sqlBilder.append("CREATE TABLE IF NOT EXISTS LESSON ( ");
        sqlBilder.append("_id               INTEGER        NOT NULL    PRIMARY KEY AUTOINCREMENT, ");
        sqlBilder.append("ID_CLASS          INTEGER, ");
        sqlBilder.append("ID_PROFESSOR      INTEGER, ");
        sqlBilder.append("ID_ROOM           INTEGER, ");
        sqlBilder.append("NAME              VARCHAR(100), ");
        sqlBilder.append("INITTIME          LONG, ");
        sqlBilder.append("ENDTIME           LONG, ");
        sqlBilder.append("PROFESSORINTIME   LONG, ");
        sqlBilder.append("PROFESSOROUTTIME  LONG ");
        sqlBilder.append("); ");
        return sqlBilder.toString();
    }

    static String getCreateRelLessonRoomTable() {
        StringBuilder sqlBilder;
        sqlBilder = new StringBuilder();
        sqlBilder.append("CREATE TABLE IF NOT EXISTS RELLESSONROOM ( ");
        sqlBilder.append("ID_LESSON         INTEGER        NOT NULL, ");
        sqlBilder.append("ID_ROOM           INTEGER        NOT NULL, ");
        sqlBilder.append("WINDOWS           INTEGER, ");
        sqlBilder.append("DOORS             INTEGER, ");
        sqlBilder.append("FANS              INTEGER, ");
        sqlBilder.append("LAMPS             INTEGER, ");
        sqlBilder.append("LUMINOSITY        INTEGER, ");
        sqlBilder.append("TEMPERATURE       INTEGER, ");
        sqlBilder.append("NOISE             INTEGER, ");
        sqlBilder.append("BOARD             BOOLEAN, ");
        sqlBilder.append("PROJECTOR         BOOLEAN, ");
        sqlBilder.append("AIRCONDITIONER    BOOLEAN ");
        sqlBilder.append("); ");
        return sqlBilder.toString();
    }

    static String getCreateRelLessonStudentTable() {
        StringBuilder sqlBilder;
        sqlBilder = new StringBuilder();
        sqlBilder.append("CREATE TABLE IF NOT EXISTS RELLESSONSTUDENT ( ");
        sqlBilder.append("ID_LESSON         INTEGER        NOT NULL, ");
        sqlBilder.append("ID_STUDENT        INTEGER        NOT NULL, ");
        sqlBilder.append("PARTICIPATIONSNUM INTEGER, ");
        sqlBilder.append("SITPLACE          VARCHAR(2), ");
        sqlBilder.append("SLEPT             INTEGER ");
        sqlBilder.append("); ");
        return sqlBilder.toString();
    }

    static String getCreateInLessonTimeTable() {
        StringBuilder sqlBilder;
        sqlBilder = new StringBuilder();
        sqlBilder.append("CREATE TABLE IF NOT EXISTS INLESSONTIME ( ");
        sqlBilder.append("_id               INTEGER        NOT NULL     PRIMARY KEY AUTOINCREMENT, ");
        sqlBilder.append("ID_LESSON         INTEGER        NOT NULL, ");
        sqlBilder.append("ID_STUDENT        INTEGER        NOT NULL, ");
        sqlBilder.append("INITTIME          LONG, ");
        sqlBilder.append("ENDTIME           LONG ");
        sqlBilder.append("); ");
        return sqlBilder.toString();
    }
}