package br.com.uefs.gradtwin.gradtwinobserver;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Date;

import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRelLessonStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.InLessonTime;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelLessonStudent;

public class StudentInLessonActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private RepositoryRelLessonStudent repositoryRelLessonStudent;
    private RepositoryStudent repositoryStudent;

    private EditText etParticipations;
    private Spinner spSitLocal;
    private ListView lstInLessonTime;
    private Button btIn, btOut;

    private ArrayAdapter<InLessonTime> adpInLessonTime;
    private ArrayAdapter<String> adpSitLocal;

    private Integer idLesson = null, idStudent = null, actualPosition;
    private InLessonTime inLessonTime = null;
    private ArrayList<InLessonTime> inLessonTimes = new ArrayList<>();
    private CheckBox chSlept;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_lesson);

        etParticipations = (EditText) findViewById(R.id.etParticipations);
        spSitLocal = (Spinner) findViewById(R.id.spSitLocal);
        btIn = (Button) findViewById(R.id.btIn);
        btOut = (Button) findViewById(R.id.btOut);
        chSlept = (CheckBox) findViewById(R.id.chSlept);
        lstInLessonTime = (ListView) findViewById(R.id.lblist);
        lstInLessonTime.setOnItemClickListener(this);

        try {
            repositoryRelLessonStudent = new RepositoryRelLessonStudent(this);
            repositoryStudent = new RepositoryStudent(this);
        } catch (Exception ex) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setMessage("Erro na conecção com o banco de dados: " + ex.getMessage());
            dlg.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            dlg.show();
        }
        this.adpSitLocal = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new String[]{"", "EF", "EC", "ET", "CF", "CC", "CT", "DF", "DC", "DT"});
        this.spSitLocal.setAdapter(this.adpSitLocal);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("INLESSONTIMES")) {
                this.inLessonTimes = (ArrayList<InLessonTime>) bundle.getSerializable("INLESSONTIMES");
            } if (bundle.containsKey("IDLESSON")) {
                this.idLesson = bundle.getInt("IDLESSON");
            } if (bundle.containsKey("IDSTUDENT")) {
                this.idStudent = bundle.getInt("IDSTUDENT");
            } if (this.idLesson != null && this.idStudent != null) {
                if (!repositoryRelLessonStudent.selectRelLessonStudents(this.idLesson, this.idStudent).isEmpty()) {
                    RelLessonStudent relLessonStudent = repositoryRelLessonStudent.selectRelLessonStudents(this.idLesson, this.idStudent).get(0);
                    this.etParticipations.setText("" + relLessonStudent.getParticipationNum());
                    this.chSlept.setChecked(this.getBoolValue(relLessonStudent.isSlept()));
                    this.spSitLocal.setSelection(adpSitLocal.getPosition(relLessonStudent.getSitPlace()));
                }
            } if (bundle.containsKey("SITPLACE")) {
                this.spSitLocal.setSelection(adpSitLocal.getPosition(bundle.getString("SITPLACE")));
            } if (bundle.containsKey("PARTICIPATIONNUM")) {
                this.etParticipations.setText("" + bundle.getInt("PARTICIPATIONNUM"));
            } if (bundle.containsKey("BEGIN")) {
                this.inLessonTime = new InLessonTime(null, this.idLesson, this.idStudent, new Date(bundle.getLong("BEGIN")), null);
                this.btIn.setVisibility(View.GONE);
                this.btOut.setVisibility(View.VISIBLE);
            } if (bundle.containsKey("SLEPT")) {
                this.chSlept.setChecked(bundle.getBoolean("SLEPT"));
            }
        }

        this.adpInLessonTime = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, this.inLessonTimes);
        this.lstInLessonTime.setAdapter(this.adpInLessonTime);
        this.setTitle(this.repositoryStudent.selectStudents(this.idStudent, null).get(0).getName());
    }

    private boolean getBoolValue(Boolean b) {
        if (b == null) {
            return false;
        } else {
            return b;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null && data.getExtras() != null) {
            Bundle bundle = data.getExtras();
            if (bundle.containsKey("INLESSONTIME") && bundle.getSerializable("INLESSONTIME") != null) {
                this.inLessonTimes.remove(actualPosition.intValue());
                this.inLessonTimes.add((InLessonTime) bundle.getSerializable("INLESSONTIME"));
            } else if (bundle.containsKey("DELETE") && bundle.getBoolean("DELETE")) {
                this.inLessonTimes.remove(actualPosition.intValue());
            }
        }
        this.adpInLessonTime = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, this.inLessonTimes);
        this.lstInLessonTime.setAdapter(this.adpInLessonTime);
    }

    public void onClickSave(View v){
        RelLessonStudent relLessonStudent = new RelLessonStudent(this.idLesson, this.idStudent, null, null, this.chSlept.isChecked());
        if (!this.etParticipations.getText().toString().isEmpty()) {
            relLessonStudent.setParticipationNum(Integer.parseInt(this.etParticipations.getText().toString()));
        } if (!this.adpSitLocal.isEmpty() && this.spSitLocal.getSelectedItemPosition() != 0) {
            relLessonStudent.setSitPlace((String) spSitLocal.getSelectedItem());
        }
        Intent it = new Intent();
        it.putExtra("RELLESSONSTUDENT", relLessonStudent);
        it.putExtra("INLESSONTIMES", this.inLessonTimes);
        if (this.btOut.getVisibility() == View.VISIBLE) {
            it.putExtra("BEGIN", this.inLessonTime.getInitTime().getTime());
        }
        setResult(0, it);
        finish();
    }

    public void onClickCancel(View v){
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long _id) {
        this.actualPosition = position;
        InLessonTime inLessonTime = this.adpInLessonTime.getItem(position);
        Intent it = new Intent(this, InLessonTimeActivity.class);
        it.putExtra("INLESSONTIME", inLessonTime);
        startActivityForResult(it, 1);
    }

    public void onClickIn(View view) {
        this.inLessonTime = new InLessonTime(null, this.idLesson, this.idStudent, new Date(), null);
        this.btIn.setVisibility(View.GONE);
        this.btOut.setVisibility(View.VISIBLE);
    }

    public void onClickOut(View view) {
        this.inLessonTime.setEndTime(new Date());
        this.inLessonTimes.add(this.inLessonTime);
        this.adpInLessonTime = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, this.inLessonTimes);
        this.lstInLessonTime.setAdapter(this.adpInLessonTime);
        this.btIn.setVisibility(View.VISIBLE);
        this.btOut.setVisibility(View.GONE);
    }
}
