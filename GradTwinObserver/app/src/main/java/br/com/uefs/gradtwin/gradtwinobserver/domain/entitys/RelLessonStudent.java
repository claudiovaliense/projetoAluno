package br.com.uefs.gradtwin.gradtwinobserver.domain.entitys;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class RelLessonStudent implements Serializable {

    private Integer idLesson;
    private Integer idStudent;
    private Integer participationNum;
    private String sitPlace;
    private Boolean slept;

    public RelLessonStudent() {
    }

    public RelLessonStudent(Integer idLesson, Integer idStudent, Integer participationNum, String sitPlace, Boolean slept) {
        this.idLesson = idLesson;
        this.idStudent = idStudent;
        this.participationNum = participationNum;
        this.sitPlace = sitPlace;
        this.slept = slept;
    }

    public Integer getParticipationNum() {
        return participationNum;
    }

    public void setParticipationNum(Integer participationNum) {
        this.participationNum = participationNum;
    }

    public String getSitPlace() {
        return sitPlace;
    }

    public void setSitPlace(String sitPlace) {
        this.sitPlace = sitPlace;
    }

    public Integer getIdLesson() {
        return idLesson;
    }

    public void setIdLesson(Integer idLesson) {
        this.idLesson = idLesson;
    }

    public Integer getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Integer idStudent) {
        this.idStudent = idStudent;
    }

    public Boolean isSlept() {
        return slept;
    }

    public void setSlept(Boolean slept) {
        this.slept = slept;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RelLessonStudent that = (RelLessonStudent) o;

        return idLesson != null && idStudent != null && idLesson.equals(that.idLesson) && idStudent.equals(that.idStudent);
    }
}
