package br.com.uefs.gradtwin.gradtwinobserver.domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;

import br.com.uefs.gradtwin.gradtwinobserver.database.DataBase;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Student;

public class RepositoryStudent {
    private final Context context;
    private SQLiteDatabase dataBaseConnection;

    public RepositoryStudent(Context context) {
        DataBase dataBase = new DataBase(context);
        this.dataBaseConnection = dataBase.getWritableDatabase();
        this.context = context;
    }

    public void insertStudent(Student student) {
        ContentValues values = this.fillValues(student);

        dataBaseConnection.insertOrThrow("STUDENT", null, values);
    }

    public void updateStudent(Student student, int id) {
        ContentValues values = this.fillValues(student);

        dataBaseConnection.update("STUDENT", values, " _id = ? ", new String[]{Integer.toString(id)});
    }

    public ArrayList<Student> selectStudents(Integer id, String name) {
        ArrayList<Student> students = new ArrayList<>();
        String _id = id == null ? " 1 = 1 " : " _id = " + id.toString() + " ";
        name = name == null ? " 1 = 1 " : " NAME = '" + name + "' ";
        String where = _id + " AND " + name + " ";
        Cursor cursor = dataBaseConnection.query("STUDENT", null, where, null, null, null, " NAME ASC ");
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                students.add(new Student(cursor.getInt(0), cursor.getString(1), cursor.getString(2)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return students;
    }

    public void deleteStudent(int id) {
        dataBaseConnection.delete("STUDENT", " _id = ? ", new String[]{Integer.toString(id)});
    }

    public void deleteStutents(Integer id, String name) {
        for (Student student: this.selectStudents(id, name)) {
            this.deleteStudent(student.getId());
        }
    }

    private ContentValues fillValues(Student student) {
        ContentValues values = new ContentValues();
        values.put("_id", student.getId());
        values.put("NAME", student.getName());
        values.put("REGISTRATION", student.getRegistration());

        return values;
    }
}
