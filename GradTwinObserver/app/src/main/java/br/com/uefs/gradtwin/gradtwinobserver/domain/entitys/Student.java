package br.com.uefs.gradtwin.gradtwinobserver.domain.entitys;

import java.io.Serializable;
import java.util.Date;

public class Student implements Serializable {

    private Integer id;
    private String name;
    private String registration;

    public Student() {
    }

    public Student(Integer id, String name, String registration) {
        this.id = id;
        this.name = name;
        this.registration = registration;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        return id != null && id.equals(student.id);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return name;
    }
}
