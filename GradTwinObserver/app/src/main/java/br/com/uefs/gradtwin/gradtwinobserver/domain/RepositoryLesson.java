package br.com.uefs.gradtwin.gradtwinobserver.domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import br.com.uefs.gradtwin.gradtwinobserver.database.DataBase;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Lesson;

public class RepositoryLesson {
    private SQLiteDatabase dataBaseConnection;
    private Context context;

    public RepositoryLesson(Context context) {
        DataBase dataBase = new DataBase(context);
        this.context = context;
        this.dataBaseConnection = dataBase.getWritableDatabase();
    }

    public int insertLesson(Lesson lesson) {
        ContentValues values = this.fillValues(lesson);

        Long l = dataBaseConnection.insertOrThrow("LESSON", null, values);
        return l.intValue();
    }

    public void updateLesson(Lesson lesson, int id) {
        ContentValues values = this.fillValues(lesson);

        dataBaseConnection.update("LESSON", values, " _id = ? ", new String[]{Integer.toString(id)});
    }

    public ArrayList<Lesson> selectLessons(Integer id, Integer idClass, String name) {
        ArrayList<Lesson> lessons = new ArrayList<>();
        String _id = id == null ? " 1 = 1 " : " _id = " + id.toString() + " ";
        String _idClass = idClass == null ? " 1 = 1 " : " ID_CLASS = " + idClass.toString() + " ";
        name = name == null ? " 1 = 1 " : " NAME = '" + name + "' ";
        String where = _id + " AND " + _idClass + " AND " + name + " ";
        Cursor cursor = dataBaseConnection.query("LESSON", null, where, null, null, null, " NAME ASC ");
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                lessons.add(new Lesson(cursor.getInt(0), cursor.getInt(1), cursor.isNull(2) ? null : cursor.getInt(2), cursor.getInt(3), cursor.getString(4), cursor.isNull(5) ? null : new Date(cursor.getLong(5)), cursor.isNull(6) ? null : new Date(cursor.getLong(6)), cursor.isNull(7) ? null : new Date(cursor.getLong(7)), cursor.isNull(8) ? null : new Date(cursor.getLong(8))));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return lessons;
    }

    public void deleteLesson(int id) {
        dataBaseConnection.delete("LESSON", " _id = ? ", new String[]{Integer.toString(id)});
        new RepositoryRelLessonRoom(this.context).deleteRelLessonRooms(id, null);
        new RepositoryRelLessonStudent(this.context).deleteRelLessonStudents(id, null);
    }

    public void deleteLessons(Integer id, Integer idClass, String name) {
        for (Lesson lesson: this.selectLessons(id, idClass, name)) {
            this.deleteLesson(lesson.getId());
        }
    }

    private ContentValues fillValues(Lesson lesson) {
        ContentValues values = new ContentValues();
        values.put("ID_CLASS", lesson.getIdClass());
        values.put("ID_PROFESSOR", lesson.getIdProfessor());
        values.put("ID_ROOM", lesson.getIdRoom());
        values.put("NAME", lesson.getName());
        values.put("INITTIME", this.getTime(lesson.getInitTime()));
        values.put("ENDTIME", this.getTime(lesson.getEndTime()));
        values.put("PROFESSORINTIME", this.getTime(lesson.getProfessorInTime()));
        values.put("PROFESSOROUTTIME", this.getTime(lesson.getProfessorOutTime()));

        return values;
    }

    private Long getTime(Date date) {
        if (date != null) {
            return date.getTime();
        }
        return null;
    }
}
