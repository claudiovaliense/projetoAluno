package br.com.uefs.gradtwin.gradtwinobserver.domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import br.com.uefs.gradtwin.gradtwinobserver.database.DataBase;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Class;

public class RepositoryClass {
    private SQLiteDatabase dataBaseConnection;
    private Context context;

    public RepositoryClass(Context context) {
        DataBase dataBase = new DataBase(context);
        this.dataBaseConnection = dataBase.getWritableDatabase();
        this.context = context;
    }

    public void insertClass(Class _class) {
        ContentValues values = this.fillValues(_class);

        dataBaseConnection.insertOrThrow("CLASS", null, values);
    }

    public void updateClass(Class _class, int id) {
        ContentValues values = this.fillValues(_class);

        dataBaseConnection.update("CLASS", values, " _id = ? ", new String[]{Integer.toString(id)});
    }

    public ArrayList<Class> selectClasses(Integer id, Integer idProfessor, Integer idMatter, Integer idSemester) {
        ArrayList<Class> classes = new ArrayList<>();
        String _id = id == null ? " 1 = 1 " : " _id = " + id.toString() + " ";
        String _idMatter = idMatter == null ? " 1 = 1 " : " ID_MATTER = " + idMatter.toString() + " ";
        String _idProfessor = idProfessor == null ? " 1 = 1 " : " ID_PROFESSOR = " + idProfessor.toString() + " ";
        String _idSemester = idSemester == null ? " 1 = 1 " : " ID_SEMESTER = " + idSemester.toString() + " ";
        String where = _id + " AND " + _idMatter + " AND " + _idProfessor + " AND " + _idSemester + " ";
        Cursor cursor = dataBaseConnection.query("CLASS", null, where, null, null, null, " _id DESC ");
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                classes.add(new Class(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(5)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return classes;
    }

    public void deleteClass(int id) {
        dataBaseConnection.delete("CLASS", " _id = ? ", new String[]{Integer.toString(id)});
    }

    public void deleteClasses(Integer id, Integer idProfessor, Integer idMatter, Integer idSemester) {
        for (Class _class: this.selectClasses(id, idProfessor, idMatter, idSemester)) {
            this.deleteClass(_class.getId());
        }
    }

    private ContentValues fillValues(Class _class) {
        ContentValues values = new ContentValues();
        values.put("_id", _class.getId());
        values.put("CODCLASS", _class.getCodClass());
        values.put("CODMATTER", _class.getCodMatter());
        values.put("NAMEMATTER", _class.getNameMatter());
        values.put("SEMESTER", _class.getSemester());
        values.put("ID_PROFESSOR", _class.getIdProfessor());

        return values;
    }
}
