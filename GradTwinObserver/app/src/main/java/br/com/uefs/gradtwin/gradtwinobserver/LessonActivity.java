package br.com.uefs.gradtwin.gradtwinobserver;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryInLessonTime;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryLesson;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRelLessonRoom;
import br.com.uefs.gradtwin.gradtwinobserver.domain.RepositoryRelLessonStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.InLessonTime;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Lesson;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelLessonRoom;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelLessonStudent;
import br.com.uefs.gradtwin.gradtwinobserver.utils.SectionsPageAdapter;

public class LessonActivity extends AppCompatActivity {
    private SectionsPageAdapter sectionsPageAdapter;

    private RepositoryLesson repositoryLesson;
    private RepositoryRelLessonRoom repositoryRelLessonRoom;
    private RepositoryRelLessonStudent repositoryRelLessonStudent;
    private RepositoryInLessonTime repositoryInLessonTime;

    private InformationsLessonFragment informationsFragment;
    private StudentsInLessonFragment studentsFragment;

    private Integer id = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson);

        Button btDelete = (Button) findViewById(R.id.btDelete);

        repositoryLesson = new RepositoryLesson(this);
        repositoryRelLessonRoom = new RepositoryRelLessonRoom(this);
        repositoryRelLessonStudent = new RepositoryRelLessonStudent(this);
        repositoryInLessonTime = new RepositoryInLessonTime(this);
        informationsFragment = new InformationsLessonFragment();
        studentsFragment = new StudentsInLessonFragment();

        ViewPager container = (ViewPager) findViewById(R.id.container);
        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(container);

        sectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());
        sectionsPageAdapter.addFragment(informationsFragment, "Informações");
        sectionsPageAdapter.addFragment(studentsFragment, "Alunos");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Lesson lesson = (Lesson) bundle.getSerializable("LESSON");
            if (lesson != null && lesson.getId() != null) {
                bundle.putInt("IDCLASS", lesson.getIdClass());
                this.id = lesson.getId();
                this.setTitle("Aula: " + lesson.getName());
                btDelete.setVisibility(View.VISIBLE);
            }
        }
        container.setAdapter(sectionsPageAdapter);
    }

    public void onClickSave(View v) {
        if (informationsFragment.save()) {
            Lesson lesson = informationsFragment.getLesson();
            RelLessonRoom relLessonRoom = informationsFragment.getRelLessonRoom();
            ArrayList<RelLessonStudent> relLessonStudents = this.studentsFragment.getRelLessonStudents();
            ArrayList<InLessonTime> inLessonTimes = this.studentsFragment.getInlessontimes();
            if (this.id == null) {
                this.id = this.repositoryLesson.insertLesson(lesson);
                relLessonRoom.setIdLesson(this.id);
                this.repositoryRelLessonRoom.insertRelLessonRoom(relLessonRoom);
            } else {
                this.repositoryLesson.updateLesson(lesson, this.id);
                this.repositoryRelLessonRoom.updateRelLessonRoom(relLessonRoom, this.id, relLessonRoom.getIdRoom());
            }
            for (RelLessonStudent relLessonStudent : relLessonStudents) {
                relLessonStudent.setIdLesson(this.id);
                if (this.repositoryRelLessonStudent.selectRelLessonStudents(relLessonStudent.getIdLesson(), relLessonStudent.getIdStudent()).isEmpty()) {
                    this.repositoryRelLessonStudent.insertRelLessonStudent(relLessonStudent);
                } else {
                    this.repositoryRelLessonStudent.updateRelLessonStudent(relLessonStudent, relLessonStudent.getIdLesson(), relLessonStudent.getIdStudent());
                }
            }
            this.repositoryInLessonTime.deleteInLessonTimes(null, this.id, null);
            for (InLessonTime inLessonTime : inLessonTimes) {
                inLessonTime.setIdLesson(this.id);
                this.repositoryInLessonTime.insertInLessonTime(inLessonTime);
            }
            finish();
        }
    }

    public void onClickDelete(View v) {
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        dlg.setMessage("Tem certeza que deseja excluir esta aula dos registros?");
        dlg.setNegativeButton("CANCELAR", null);
        dlg.setPositiveButton("CONFIRMAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                repositoryLesson.deleteLesson(id);
                finish();
            }
        });
        dlg.show();
    }

    public void onClickCancel(View v) {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (int i = 0; i < sectionsPageAdapter.getCount(); i++) {
            sectionsPageAdapter.getItem(i).onActivityResult(requestCode, resultCode, data);
        }
    }
}
