package br.com.uefs.gradtwin.gradtwinobserver.domain.entitys;

import java.io.Serializable;

public class RelClassStudent implements Serializable {

    private Integer idClass;
    private Integer idStudent;

    public RelClassStudent() {
    }

    public RelClassStudent(Integer idClass, Integer idStudent) {
        this.idClass = idClass;
        this.idStudent = idStudent;
    }

    public Integer getIdClass() {
        return idClass;
    }

    public void setIdClass(Integer idClass) {
        this.idClass = idClass;
    }

    public Integer getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Integer idStudent) {
        this.idStudent = idStudent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RelClassStudent that = (RelClassStudent) o;

        return idClass != null && idStudent != null && idClass.equals(that.idClass) && idStudent.equals(that.idStudent);
    }

    @Override
    public String toString() {
        return "RelClassStudent{" +
                "idClass=" + idClass +
                ", idStudent=" + idStudent +
                '}';
    }
}
