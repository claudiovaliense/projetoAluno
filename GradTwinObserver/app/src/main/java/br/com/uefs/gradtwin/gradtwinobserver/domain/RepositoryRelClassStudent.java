package br.com.uefs.gradtwin.gradtwinobserver.domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import br.com.uefs.gradtwin.gradtwinobserver.database.DataBase;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelClassStudent;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Student;

public class RepositoryRelClassStudent {
    private SQLiteDatabase dataBaseConnection;
    private Context context;

    public RepositoryRelClassStudent(Context context) {
        this.context = context;
        DataBase dataBase = new DataBase(context);
        this.dataBaseConnection = dataBase.getWritableDatabase();
    }

    public void insertRelClassStudent(RelClassStudent relClassStudent) {
        ContentValues values = this.fillValues(relClassStudent);

        dataBaseConnection.insertOrThrow("RELCLASSSTUDENT", null, values);
    }

    public void updateRelClassStudent(RelClassStudent relClassStudent, int idClass, int idStudent) {
        ContentValues values = this.fillValues(relClassStudent);

        dataBaseConnection.update("RELCLASSSTUDENT", values, " ID_CLASS = ? AND ID_STUDENT = ? ", new String[]{Integer.toString(idClass), Integer.toString(idStudent)});
    }

    public ArrayList<RelClassStudent> selectRelClassStudents(Integer idClass, Integer idStudent) {
        ArrayList<RelClassStudent> relClassStudents = new ArrayList<>();
        String _idClass = idClass == null ? " 1 = 1 " : " ID_CLASS = " + idClass.toString() + " ";
        String _idStudent = idStudent == null ? " 1 = 1 " : " ID_STUDENT = " + idStudent.toString() + " ";
        String where = _idClass + " AND " + _idStudent + " ";
        Cursor cursor = dataBaseConnection.query("RELCLASSSTUDENT", null, where, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                relClassStudents.add(new RelClassStudent(cursor.getInt(0), cursor.getInt(1)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return relClassStudents;
    }

    public ArrayList<Student> selectStudents(Integer idClass) {
        ArrayList<Student> students = new ArrayList<>();
        RepositoryStudent repositoryStudent = new RepositoryStudent(this.context);
        for (RelClassStudent relClassStudent : this.selectRelClassStudents(idClass, null)) {
            students.add(repositoryStudent.selectStudents(relClassStudent.getIdStudent(), null).get(0));
        }
        return students;
    }

    public void deleteRelClassStudent(int idClass, int idStudent) {
        dataBaseConnection.delete("RELCLASSSTUDENT", " ID_CLASS = ? AND ID_STUDENT = ? ", new String[]{Integer.toString(idClass), Integer.toString(idStudent)});
    }

    public void deleteRelClassStudents(Integer idClass, Integer idStudent) {
        for (RelClassStudent relClassStudent: this.selectRelClassStudents(idClass, idStudent)) {
            this.deleteRelClassStudent(relClassStudent.getIdClass(), relClassStudent.getIdStudent());
        }
    }

    private ContentValues fillValues(RelClassStudent relClassStudent) {
        ContentValues values = new ContentValues();
        values.put("ID_CLASS", relClassStudent.getIdClass());
        values.put("ID_STUDENT", relClassStudent.getIdStudent());

        return values;
    }
}
