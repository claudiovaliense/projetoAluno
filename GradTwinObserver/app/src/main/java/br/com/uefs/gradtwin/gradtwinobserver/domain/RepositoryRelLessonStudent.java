package br.com.uefs.gradtwin.gradtwinobserver.domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import br.com.uefs.gradtwin.gradtwinobserver.database.DataBase;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.RelLessonStudent;

public class RepositoryRelLessonStudent {
    private SQLiteDatabase dataBaseConnection;
    private Context context;

    public RepositoryRelLessonStudent(Context context) {
        this.context = context;
        DataBase dataBase = new DataBase(context);
        this.dataBaseConnection = dataBase.getWritableDatabase();
    }

    public void insertRelLessonStudent(RelLessonStudent relLessonStudent) {
        ContentValues values = this.fillValues(relLessonStudent);

        dataBaseConnection.insertOrThrow("RELLESSONSTUDENT", null, values);
    }

    public void updateRelLessonStudent(RelLessonStudent relLessonStudent, int idLesson, int idStudent) {
        ContentValues values = this.fillValues(relLessonStudent);

        dataBaseConnection.update("RELLESSONSTUDENT", values, " ID_LESSON = ? AND ID_STUDENT = ? ", new String[]{Integer.toString(idLesson), Integer.toString(idStudent)});
    }

    public ArrayList<RelLessonStudent> selectRelLessonStudents(Integer idLesson, Integer idStudent) {
        ArrayList<RelLessonStudent> relLessonStudents = new ArrayList<>();
        String _idLesson = idLesson == null ? " 1 = 1 " : " ID_LESSON = " + idLesson.toString() + " ";
        String _idStudent = idStudent == null ? " 1 = 1 " : " ID_STUDENT = " + idStudent.toString() + " ";
        String where = _idLesson + " AND " + _idStudent + " ";
        Cursor cursor = dataBaseConnection.query("RELLESSONSTUDENT", null, where, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                relLessonStudents.add(new RelLessonStudent(cursor.getInt(0), cursor.getInt(1), cursor.isNull(2) ? null : cursor.getInt(2), cursor.getString(3), cursor.getInt(4) == 1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return relLessonStudents;
    }

    public void deleteRelLessonStudent(int idLesson, int idStudent) {
        dataBaseConnection.delete("RELLESSONSTUDENT", " ID_LESSON = ? AND ID_STUDENT = ? ", new String[]{Integer.toString(idLesson), Integer.toString(idStudent)});
        new RepositoryInLessonTime(this.context).deleteInLessonTimes(null, idLesson, idStudent);
    }

    public void deleteRelLessonStudents(Integer idLesson, Integer idStudent) {
        for (RelLessonStudent relLessonStudent: this.selectRelLessonStudents(idLesson, idStudent)) {
            this.deleteRelLessonStudent(relLessonStudent.getIdLesson(), relLessonStudent.getIdStudent());
        }
    }

    private ContentValues fillValues(RelLessonStudent relLessonStudent) {
        ContentValues values = new ContentValues();
        values.put("ID_LESSON", relLessonStudent.getIdLesson());
        values.put("ID_STUDENT", relLessonStudent.getIdStudent());
        values.put("PARTICIPATIONSNUM", relLessonStudent.getParticipationNum());
        values.put("SITPLACE", relLessonStudent.getSitPlace());
        values.put("SLEPT", relLessonStudent.isSlept() ? 1 : 0);

        return values;
    }
}
