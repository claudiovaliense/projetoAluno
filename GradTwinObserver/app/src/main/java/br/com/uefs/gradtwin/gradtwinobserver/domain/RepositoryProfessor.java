package br.com.uefs.gradtwin.gradtwinobserver.domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;

import br.com.uefs.gradtwin.gradtwinobserver.database.DataBase;
import br.com.uefs.gradtwin.gradtwinobserver.domain.entitys.Professor;

public class RepositoryProfessor {
    private final Context context;
    private SQLiteDatabase dataBaseConnection;

    public RepositoryProfessor(Context context) {
        DataBase dataBase = new DataBase(context);
        this.context = context;
        this.dataBaseConnection = dataBase.getWritableDatabase();
    }

    public void insertProfessor(Professor professor) {
        ContentValues values = this.fillValues(professor);

        dataBaseConnection.insertOrThrow("PROFESSOR", null, values);
    }

    public void updateProfessor(Professor professor, int id) {
        ContentValues values = this.fillValues(professor);

        dataBaseConnection.update("PROFESSOR", values, " _id = ? ", new String[]{Integer.toString(id)});
    }

    public ArrayList<Professor> selectProfessors(Integer id, String name) {
        ArrayList<Professor> professors = new ArrayList<>();
        String _id = id == null ? " 1 = 1 " : " _id = " + id.toString() + " ";
        name = name == null ? " 1 = 1 " : " NAME = '" + name + "' ";
        String where = _id + " AND " + name + " ";
        Cursor cursor = dataBaseConnection.query("PROFESSOR", null, where, null, null, null, " NAME ASC ");
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                professors.add(new Professor(cursor.getInt(0), cursor.getString(1), cursor.getString(2)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return professors;
    }

    public void deleteProfessor(int id) {
        dataBaseConnection.delete("PROFESSOR", " _id = ? ", new String[]{Integer.toString(id)});
    }

    public void deleteProfessors(Integer id, String name) {
        for (Professor professor: this.selectProfessors(id, name)) {
            this.deleteProfessor(professor.getId());
        }
    }

    private ContentValues fillValues(Professor professor) {
        ContentValues values = new ContentValues();
        values.put("_id", professor.getId());
        values.put("NAME", professor.getName());
        values.put("REGISTRATION", professor.getRegistration());

        return values;
    }
}
