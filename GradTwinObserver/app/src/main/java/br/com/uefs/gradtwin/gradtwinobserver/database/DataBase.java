package br.com.uefs.gradtwin.gradtwinobserver.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBase extends SQLiteOpenHelper {

    public DataBase (Context context) {
        super(context, "GRADTWINDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ScriptsSQLite.getCreateProfessorTable());
        db.execSQL(ScriptsSQLite.getCreateRoomTable());
        db.execSQL(ScriptsSQLite.getCreateStudentTable());
        db.execSQL(ScriptsSQLite.getCreateClassTable());
        db.execSQL(ScriptsSQLite.getCreateRelClassStudentTable());
        db.execSQL(ScriptsSQLite.getCreateLessonTable());
        db.execSQL(ScriptsSQLite.getCreateInLessonTimeTable());
        db.execSQL(ScriptsSQLite.getCreateRelLessonRoomTable());
        db.execSQL(ScriptsSQLite.getCreateRelLessonStudentTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
